/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/python.hpp>
#include <fstream>

#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"

using namespace srs;

namespace {
  namespace py = boost::python;

  template <class T>
  py::list to_python_list_const(std::vector<T> vec) {
    py::list list;
    for (const auto& it : vec)
      list.append(it);
    return list;
  }
  template <class T>
  std::vector<T> to_cpp_vector_const(py::object iterable) {
    return std::vector<T>(py::stl_input_iterator<T>(iterable), py::stl_input_iterator<T>());
  }
  py::list parse_commands(const std::string& filename) {
    py::list out;
    port_t port;
    std::string addr;
    auto cmds = Messenger::parseCommands(filename, addr, port);
    out.append(to_python_list_const(cmds));
    out.append(addr);
    out.append(port);
    return out;
  }
  bool msg_send(srs::Messenger& msg, int port, py::list cmds) {
    return msg.send(port, to_cpp_vector_const<uint32_t>(cmds));
  }
  std::shared_ptr<Logger> getShared() {
    return std::shared_ptr<Logger>(&Logger::get(), [](Logger*) {});
  }
  template <typename T>
  std::string print_object(const T& obj) {
    std::ostringstream os;
    obj.print(os);
    return os.str();
  }
  template <typename T>
  bool save_config(
      const T& obj, const std::string& filename, const std::string& ipaddr, srs::port_t port, size_t subaddr = 0) {
    if (filename.empty())
      return false;
    std::ofstream file(filename);
    for (const auto& word : Messenger::writeCommands(ipaddr, port, subaddr, obj, true))
      file << word << "\n";
    return true;
  }
  BOOST_PYTHON_FUNCTION_OVERLOADS(save_config_sys_overloads, save_config<SystemRegister>, 4, 5);
  BOOST_PYTHON_FUNCTION_OVERLOADS(save_config_apvapp_overloads, save_config<ApvAppRegister>, 4, 5);
  BOOST_PYTHON_FUNCTION_OVERLOADS(save_config_apv_overloads, save_config<ApvRegister>, 4, 5);
  BOOST_PYTHON_FUNCTION_OVERLOADS(save_config_pll_overloads, save_config<PllRegister>, 4, 5);
  template <class E, class... Policies, class... Args>
  py::class_<E, Policies...> exception_(Args&&... args) {
    py::class_<E, Policies...> cls(std::forward<Args>(args)...);
    py::register_exception_translator<E>([ptr = cls.ptr()](E const& e) { PyErr_SetObject(ptr, py::object(e).ptr()); });
    return cls;
  }
  struct LoggedException : LoggedMessage, std::runtime_error {
    LoggedException(const char* from, const char* module = "", Type type = Type::undefined, short id = 0)
        : LoggedMessage(from, module, type, id), std::runtime_error(message()) {}
    const char* what() const noexcept override { return message().data(); }
  };
  void test_exception() { throw SRS_ERROR("test") << "A beautiful error object!"; }
}  // namespace

BOOST_PYTHON_MODULE(pysrsdriver) {
  namespace py = boost::python;

  py::def("test_exception", &test_exception);

  //-----------------------------------------------------------------------------------------------
  // Main SRS objects
  //-----------------------------------------------------------------------------------------------

  py::class_<SlowControl, boost::noncopyable>("SlowControl", py::init<std::string>())
      .def("addFec", &SlowControl::addFec, "Add a FEC receiver to this control")
      .def("clearFecs", &SlowControl::clearFecs, "Remove all FECs receivers from object")
      .def("numFec", &SlowControl::numFec, "Number of FECs objects registered to this object")
      .def("setDaqIP", py::make_function(&SlowControl::setDaqIP, py::return_internal_reference<>()))
      .def("read", &SlowControl::read, "Read the next frames from a given FEC id")
      .def("readSystemRegister", &SlowControl::readSystemRegister)
      .def("readApvAppRegister", &SlowControl::readApvAppRegister)
      .def("readApvRegister", &SlowControl::readApvRegister)
      .def("readPllRegister", &SlowControl::readPllRegister)
      .add_property("messenger",
                    py::make_function(&SlowControl::messenger, py::return_internal_reference<>()),
                    "Reference to the socket-messaging object")
      .add_property("readout",
                    &SlowControl::readoutEnabled,
                    py::make_function(&SlowControl::setReadoutEnable, py::return_internal_reference<>()))
      .def("rebootFec", &SlowControl::rebootFec, "Reboot the FPGA")
      .def("warmInit", &SlowControl::warmInit, "Initiate a warm initialisation of the card");

  py::class_<Messenger>("Messenger", py::init<std::string>())
      .def("parseCommands", parse_commands, "Parse a slow_control command file")
      .staticmethod("parseCommands")
      .def("send", &Messenger::send, "Send a message at a given port to the socket")
      .def("send", msg_send, "Send a message at a given port to the socket")
      .def("writePairs", &Messenger::writePairs, "Write pairs of (address, data) onto a socket-handled device")
      .def("writeBurst", &Messenger::writeBurst, "Write a collection of parameter words onto a socket-handled device")
      .def("readBurst", &Messenger::readBurst, "Read a collection of parameter words from a socket-handled device")
      .def("readList",
           &Messenger::readList,
           "Read a collection of register at a set of addresses from a socket-handled device");

  //-----------------------------------------------------------------------------------------------
  // Utilitaries
  //-----------------------------------------------------------------------------------------------

  py::enum_<Logger::Level>("LoggerLevel", "Logging threshold for the output stream")
      .value("nothing", Logger::Level::nothing)
      .value("error", Logger::Level::error)
      .value("warning", Logger::Level::warning)
      .value("information", Logger::Level::information)
      .value("debug", Logger::Level::debug);

  py::class_<Logger, std::shared_ptr<Logger>, boost::noncopyable>(
      "Logger", "General purposes logging singleton", py::no_init)
      .def("get", &getShared)
      .staticmethod("get")
      .add_property("level",
                    &Logger::level,
                    py::make_function(&Logger::setLevel, py::return_value_policy<py::reference_existing_object>()),
                    "Logging threshold")
      .def_readwrite("pretty", &Logger::pretty, "Use pretty-print routines");

  py::enum_<LogMessage::Type>("LoggedMessageType")
      .value("undefined", LogMessage::Type::undefined)
      .value("debug", LogMessage::Type::debug)
      .value("verbatim", LogMessage::Type::verbatim)
      .value("info", LogMessage::Type::info)
      .value("error", LogMessage::Type::error)
      .value("warning", LogMessage::Type::warning);

  exception_<LoggedException>("LoggedMessage", py::init<const char*, const char*, LogMessage::Type, short>())
      .add_property("type", &LoggedException::type)
      .add_property("message", &LoggedException::message);

  py::enum_<Port>("Port", "List of accessible SRS ports leading to control registers")
      .value("INVALID", Port::P_INVALID)
      .value("SYS", Port::P_SYS)
      .value("APVAPP", Port::P_APVAPP)
      .value("APV", Port::P_APV)
      .value("ADCCARD", Port::P_ADCCARD);

  //-----------------------------------------------------------------------------------------------
  // System register
  //-----------------------------------------------------------------------------------------------

  py::enum_<SystemRegister::ClockSource>("ClockSource")
      .value("UNKNOWN", SystemRegister::UNKNOWN)
      .value("LOCAL", SystemRegister::LOCAL)
      .value("DTC", SystemRegister::DTC)
      .value("ETH", SystemRegister::ETH);

  py::class_<SystemRegister::ClockSel>("ClockSel")
      .add_property("dtcClockInhibit",
                    &SystemRegister::ClockSel::dtcClockInhibit,
                    py::make_function(&SystemRegister::ClockSel::setDtcClockInhibit, py::return_internal_reference<>()),
                    "DTC clock inhibit")
      .add_property(
          "dtcTriggerInhibit",
          &SystemRegister::ClockSel::dtcTriggerInhibit,
          py::make_function(&SystemRegister::ClockSel::setDtcTriggerInhibit, py::return_internal_reference<>()),
          "DTC trigger inhibit")
      .add_property("dtcSwapPorts",
                    &SystemRegister::ClockSel::dtcSwapPorts,
                    py::make_function(&SystemRegister::ClockSel::setDtcSwapPorts, py::return_internal_reference<>()),
                    "Swap DTC ports (CTF connection from J1 instead of J2)")
      .add_property("dtcSwapLanes",
                    &SystemRegister::ClockSel::dtcSwapLanes,
                    py::make_function(&SystemRegister::ClockSel::setDtcSwapLanes, py::return_internal_reference<>()),
                    "Swap the clock and trigger inputs on the DTC connector")
      .add_property(
          "dtcTriggerPolInvert",
          &SystemRegister::ClockSel::dtcTriggerPolInvert,
          py::make_function(&SystemRegister::ClockSel::setDtcTriggerPolInvert, py::return_internal_reference<>()),
          "Invert the polarity of the trigger")
      .add_property(
          "ethernetClockSel",
          &SystemRegister::ClockSel::ethernetClockSel,
          py::make_function(&SystemRegister::ClockSel::setEthernetClockSel, py::return_internal_reference<>()),
          "Force the use of the ethernet clock as main application clock");

  py::enum_<SystemRegister::ClockStatus::ClockSelection>("ClockSelection")
      .value("LOCAL", SystemRegister::ClockStatus::LOCAL)
      .value("DTC", SystemRegister::ClockStatus::DTC)
      .value("ETH_RX", SystemRegister::ClockStatus::ETH_RX)
      .value("INVALID", SystemRegister::ClockStatus::INVALID);

  py::class_<SystemRegister::ClockStatus>("ClockStatus", "Read-only status word for the clock")
      .add_property("dtc0ClkLocked", &SystemRegister::ClockStatus::dtc0ClkLocked)
      .add_property("dtc1ClkLocked", &SystemRegister::ClockStatus::dtc1ClkLocked)
      .add_property("clockSelection", &SystemRegister::ClockStatus::clockSelection)
      .add_property("clockFrequency", &SystemRegister::ClockStatus::clockFrequency);

  py::enum_<SystemRegister::DtcConfig::PaddingType>("PaddingType")
      .value("NONE", SystemRegister::DtcConfig::NONE)
      .value("P_16BIT", SystemRegister::DtcConfig::P_16BIT)
      .value("P_32BIT", SystemRegister::DtcConfig::P_32BIT)
      .value("P_64BIT", SystemRegister::DtcConfig::P_64BIT);
  py::class_<SystemRegister::DtcConfig>("DtcConfig")
      .add_property("dataOverEth",
                    &SystemRegister::DtcConfig::dataOverEth,
                    py::make_function(&SystemRegister::DtcConfig::setDataOverEth, py::return_internal_reference<>()))
      .add_property("flowControl",
                    &SystemRegister::DtcConfig::flowControl,
                    py::make_function(&SystemRegister::DtcConfig::setFlowControl, py::return_internal_reference<>()))
      .add_property("paddingType",
                    &SystemRegister::DtcConfig::paddingType,
                    py::make_function(&SystemRegister::DtcConfig::setPaddingType, py::return_internal_reference<>()))
      .add_property(
          "dataChannelTrgId",
          &SystemRegister::DtcConfig::dataChannelTrgId,
          py::make_function(&SystemRegister::DtcConfig::setDataChannelTrgId, py::return_internal_reference<>()))
      .add_property("dataFrameTrgId",
                    &SystemRegister::DtcConfig::dataFrameTrgId,
                    py::make_function(&SystemRegister::DtcConfig::setDataFrameTrgId, py::return_internal_reference<>()))
      .add_property(
          "trailerWordCount",
          &SystemRegister::DtcConfig::trailerWordCount,
          py::make_function(&SystemRegister::DtcConfig::setTrailerWordCount, py::return_internal_reference<>()))
      .add_property("paddingByte",
                    &SystemRegister::DtcConfig::paddingByte,
                    py::make_function(&SystemRegister::DtcConfig::setPaddingByte, py::return_internal_reference<>()))
      .add_property("trailerByte",
                    &SystemRegister::DtcConfig::trailerByte,
                    py::make_function(&SystemRegister::DtcConfig::setTrailerByte, py::return_internal_reference<>()));

  SystemRegister::ClockSel (SystemRegister::*cst_clockSelection)() const = &SystemRegister::clockSelection;
  SystemRegister::DtcConfig (SystemRegister::*cst_dtcConfig)() const = &SystemRegister::dtcConfig;

  py::class_<SystemRegister>("SystemRegister")
      .def("save", save_config<SystemRegister>, save_config_sys_overloads())
      .add_property("fwVersion", &SystemRegister::fwVersion, "Firmware version identifier")
      .add_property("hwVersion", &SystemRegister::hwVersion, "Firmware version register")
      .add_property("vendorId", &SystemRegister::vendorId, "Local MAC address, vendor identifier part")
      .add_property("macId", &SystemRegister::macId, "Local MAC address, device identifier part")
      .add_property("fecIP",
                    &SystemRegister::fecIP,
                    py::make_function(&SystemRegister::setFecIP, py::return_internal_reference<>()),
                    "Local (FEC) IP address")
      .add_property("daqIP",
                    &SystemRegister::daqIP,
                    py::make_function(&SystemRegister::setDaqIP, py::return_internal_reference<>()),
                    "DAQ destination IP")
      .add_property("daqPort", &SystemRegister::daqPort, "UDP port for data transfer")
      .add_property("scPort", &SystemRegister::scPort, "UDP port for slow-control")
      .add_property("clockSource", &SystemRegister::clockSource)
      .add_property("dtcConfig",
                    cst_dtcConfig,
                    py::make_function(&SystemRegister::setDtcConfig, py::return_internal_reference<>()),
                    "Control register for the DTCC link")
      .add_property("clockSelection",
                    cst_clockSelection,
                    py::make_function(&SystemRegister::setClockSelection, py::return_internal_reference<>()),
                    "Main clock selection register")
      .add_property("clockStatus", &SystemRegister::clockStatus, "Main clock status register (R/O)");

  //-----------------------------------------------------------------------------------------------
  // APV application register
  //-----------------------------------------------------------------------------------------------

  py::enum_<ChannelEnable::Mode>("ChannelEnableMode")
      .value("DISABLED", ChannelEnable::Mode::DISABLED)
      .value("MASTER", ChannelEnable::Mode::MASTER)
      .value("SLAVE", ChannelEnable::Mode::SLAVE)
      .value("ALL_APVS", ChannelEnable::Mode::ALL_APVS);

  py::class_<ChannelEnable>("ChannelEnable", "Channels enable pattern")
      .def(py::init<>())
      .def(py::init<uint32_t>())
      .def("__str__", &print_object<ChannelEnable>)
      .def("__len__", &ChannelEnable::size, "Number of channel words contained")
      .def("mode", &ChannelEnable::mode)
      .def("setMode", py::make_function(&ChannelEnable::setMode, py::return_internal_reference<>()));

  py::class_<ApvAppRegister::TriggerMode>("TriggerMode")
      .add_property("apvReset",
                    &ApvAppRegister::TriggerMode::apvReset,
                    py::make_function(&ApvAppRegister::TriggerMode::setApvReset, py::return_internal_reference<>()))
      .add_property("apvTestPulse",
                    &ApvAppRegister::TriggerMode::apvTestPulse,
                    py::make_function(&ApvAppRegister::TriggerMode::setApvTestPulse, py::return_internal_reference<>()))
      .add_property(
          "externalTrigger",
          &ApvAppRegister::TriggerMode::externalTrigger,
          py::make_function(&ApvAppRegister::TriggerMode::setExternalTrigger, py::return_internal_reference<>()))
      .add_property(
          "nimTriggerPolarity",
          &ApvAppRegister::TriggerMode::nimTriggerPolarity,
          py::make_function(&ApvAppRegister::TriggerMode::setNimTriggerPolarity, py::return_internal_reference<>()));

  py::enum_<ApvAppRegister::EbEventInfoData::HeaderInfo>("HeaderInfo")
      .value("LABEL_AND_DATALENGTH", ApvAppRegister::EbEventInfoData::LABEL_AND_DATALENGTH)
      .value("TRIGGER_AND_DATALENGTH", ApvAppRegister::EbEventInfoData::TRIGGER_AND_DATALENGTH)
      .value("TRIGGER_ONLY", ApvAppRegister::EbEventInfoData::TRIGGER_ONLY);

  py::class_<ApvAppRegister::EbEventInfoData>("EbEventInfoData")
      .add_property(
          "headerInfo",
          &ApvAppRegister::EbEventInfoData::headerInfo,
          py::make_function(&ApvAppRegister::EbEventInfoData::setHeaderInfo, py::return_internal_reference<>()))
      .add_property("runLabel",
                    &ApvAppRegister::EbEventInfoData::runLabel,
                    py::make_function(&ApvAppRegister::EbEventInfoData::setRunLabel, py::return_internal_reference<>()),
                    "Run label copied in the MSBs of HEADER_INFO_FIELD of every event");

  py::class_<ApvAppRegister::ApzStatus>("ApzStatus")
      .add_property("phaseCalibBusy", &ApvAppRegister::ApzStatus::phaseCalibBusy)
      .add_property("pedestalCalibBusy", &ApvAppRegister::ApzStatus::pedestalCalibBusy)
      .add_property("phaseAligned", &ApvAppRegister::ApzStatus::phaseAligned)
      .add_property("watchdogFlag", &ApvAppRegister::ApzStatus::watchdogFlag)
      .add_property("calibAllDone", &ApvAppRegister::ApzStatus::calibAllDone)
      .add_property("commandDone", &ApvAppRegister::ApzStatus::commandDone)
      .add_property("apzBypassN", &ApvAppRegister::ApzStatus::apzBypassN)
      .add_property("apzEnabled", &ApvAppRegister::ApzStatus::apzEnabled)
      .add_property("calibAllCurrent", &ApvAppRegister::ApzStatus::calibAllCurrent)
      .add_property("apzChannelStatus", &ApvAppRegister::ApzStatus::apzChannelStatus);

  py::class_<ApvAppRegister::ApzZeroSuppThreshold>("ApzZeroSuppThreshold",
                                                   "Threshold register for the zero-suppression operation")
      .add_property("fractionalThreshold",
                    &ApvAppRegister::ApzZeroSuppThreshold::fractionalThreshold,
                    py::make_function(&ApvAppRegister::ApzZeroSuppThreshold::setFractionalThreshold,
                                      py::return_internal_reference<>()))
      .add_property("integerThreshold",
                    &ApvAppRegister::ApzZeroSuppThreshold::integerThreshold,
                    py::make_function(&ApvAppRegister::ApzZeroSuppThreshold::setIntegerThreshold,
                                      py::return_internal_reference<>()));

  py::class_<ApvAppRegister::ApzZeroSuppParameters>("ApzZeroSuppParameters",
                                                    "Configuration register for the zero-suppression unit")
      .add_property(
          "peakFindMode",
          &ApvAppRegister::ApzZeroSuppParameters::peakFindMode,
          py::make_function(&ApvAppRegister::ApzZeroSuppParameters::setPeakFindMode, py::return_internal_reference<>()))
      .add_property("disablePedestalCorrection",
                    &ApvAppRegister::ApzZeroSuppParameters::disablePedestalCorrection,
                    py::make_function(&ApvAppRegister::ApzZeroSuppParameters::setDisablePedestalCorrection,
                                      py::return_internal_reference<>()))
      .add_property(
          "forceSignal",
          &ApvAppRegister::ApzZeroSuppParameters::forceSignal,
          py::make_function(&ApvAppRegister::ApzZeroSuppParameters::setForceSignal, py::return_internal_reference<>()))
      .add_property("thresholdMode",
                    &ApvAppRegister::ApzZeroSuppParameters::thresholdMode,
                    py::make_function(&ApvAppRegister::ApzZeroSuppParameters::setThresholdMode,
                                      py::return_internal_reference<>()));

  py::enum_<ApvAppRegister::EventBuilderMode>("EventBuilderMode")
      .value("FRAME_EVENT_CNT", ApvAppRegister::FRAME_EVENT_CNT)
      .value("FRAME_RUN_CNT", ApvAppRegister::FRAME_RUN_CNT)
      .value("TIMESTAMP", ApvAppRegister::TIMESTAMP);

  ApvAppRegister::TriggerMode (ApvAppRegister::*cst_triggerMode)() const = &ApvAppRegister::triggerMode;
  ChannelEnable (ApvAppRegister::*cst_ebChannelEnableMask)() const = &ApvAppRegister::ebChannelEnableMask;
  ApvAppRegister::EbEventInfoData (ApvAppRegister::*cst_ebEventInfoData)() const = &ApvAppRegister::ebEventInfoData;
  ApvAppRegister::ApzZeroSuppThreshold (ApvAppRegister::*cst_apzZeroSuppThreshold)() const =
      &ApvAppRegister::apzZeroSuppThreshold;
  ApvAppRegister::ApzZeroSuppParameters (ApvAppRegister::*cst_apzZeroSuppParameters)() const =
      &ApvAppRegister::apzZeroSuppParameters;

  py::class_<ApvAppRegister>("ApvAppRegister")
      .def("__str__", &print_object<ApvAppRegister>)
      .def("save", save_config<ApvAppRegister>, save_config_apvapp_overloads())
      .add_property("triggerMode",
                    cst_triggerMode,
                    py::make_function(&ApvAppRegister::setTriggerMode, py::return_internal_reference<>()))
      .add_property("triggerBurst",
                    &ApvAppRegister::triggerBurst,
                    py::make_function(&ApvAppRegister::setTriggerBurst, py::return_internal_reference<>()))
      .add_property("triggerSeqPeriod",
                    &ApvAppRegister::triggerSeqPeriod,
                    py::make_function(&ApvAppRegister::setTriggerSeqPeriod, py::return_internal_reference<>()))
      .add_property("triggerDelay",
                    &ApvAppRegister::triggerDelay,
                    py::make_function(&ApvAppRegister::setTriggerDelay, py::return_internal_reference<>()))
      .add_property("testPulseDelay",
                    &ApvAppRegister::testPulseDelay,
                    py::make_function(&ApvAppRegister::setTestPulseDelay, py::return_internal_reference<>()))
      .add_property("roSync",
                    &ApvAppRegister::roSync,
                    py::make_function(&ApvAppRegister::setRoSync, py::return_internal_reference<>()))
      .add_property("adcStatus", &ApvAppRegister::adcStatus)
      .add_property(
          "ebChannelEnableMask",
          cst_ebChannelEnableMask,
          py::make_function(&ApvAppRegister::setEbChannelEnableMask, py::return_value_policy<py::return_by_value>()))
      .add_property("ebCaptureWindow",
                    &ApvAppRegister::ebCaptureWindow,
                    py::make_function(&ApvAppRegister::setEbCaptureWindow, py::return_internal_reference<>()))
      .add_property("ebMode",
                    &ApvAppRegister::ebMode,
                    py::make_function(&ApvAppRegister::setEbMode, py::return_internal_reference<>()))
      .add_property("ebDataFormat",
                    &ApvAppRegister::ebDataFormat,
                    py::make_function(&ApvAppRegister::setEbDataFormat, py::return_internal_reference<>()))
      .add_property("ebEventInfoData",
                    cst_ebEventInfoData,
                    py::make_function(&ApvAppRegister::setEbEventInfoData, py::return_internal_reference<>()))
      .add_property("readoutEnabled",
                    &ApvAppRegister::readoutEnabled,
                    py::make_function(&ApvAppRegister::setReadoutEnabled, py::return_internal_reference<>()))
      .add_property("apzSyncDet", &ApvAppRegister::apzSyncDet)
      .add_property("apzStatus", &ApvAppRegister::apzStatus)
      .add_property("apzStatus",
                    &ApvAppRegister::apzStatus,
                    py::make_function(&ApvAppRegister::setApzApvSelect, py::return_internal_reference<>()))
      .add_property("apzNumSamples",
                    &ApvAppRegister::apzNumSamples,
                    py::make_function(&ApvAppRegister::setApzNumSamples, py::return_internal_reference<>()),
                    "How many time-samples to process for each trigger")
      .add_property("apzZeroSuppThreshold",
                    cst_apzZeroSuppThreshold,
                    py::make_function(&ApvAppRegister::setApzZeroSuppThreshold, py::return_internal_reference<>()))
      .add_property("apzZeroSuppParameters",
                    cst_apzZeroSuppParameters,
                    py::make_function(&ApvAppRegister::setApzZeroSuppParameters, py::return_internal_reference<>()))
      .add_property("apvSyncLowThreshold", &ApvAppRegister::apvSyncLowThreshold)
      .add_property("apvSyncHighThreshold", &ApvAppRegister::apvSyncHighThreshold)
      .add_property("apzCommand", &ApvAppRegister::apzCommand);

  //-----------------------------------------------------------------------------------------------
  // APV register
  //-----------------------------------------------------------------------------------------------

  py::enum_<ApvChannel::Mode>("ApvChannelMode")
      .value("MASTER", ApvChannel::Mode::MASTER)
      .value("SLAVE", ApvChannel::Mode::SLAVE)
      .value("ALL_APVS", ApvChannel::Mode::ALL_APVS);

  py::class_<ApvChannel>("ApvChannel",
                         "Specialised channels pattern of inhibit for the APV register",
                         py::init<uint32_t, ApvChannel::Mode>())
      .def("__str__", &print_object<ApvChannel>)
      .def("__len__", &ApvChannel::size, "Number of channel words contained")
      .def("all", &ApvChannel::all)
      .staticmethod("all");

  py::enum_<ApvRegister::ApvMode::TriggerMode>("ApvTriggerMode")
      .value("T_3SAMPLE", ApvRegister::ApvMode::T_3SAMPLE)
      .value("T_1SAMPLE", ApvRegister::ApvMode::T_1SAMPLE);

  py::enum_<ApvRegister::ApvMode::ReadoutMode>("ReadoutMode")
      .value("RO_DECONVOLUTION", ApvRegister::ApvMode::RO_DECONVOLUTION)
      .value("RO_PEAK", ApvRegister::ApvMode::RO_PEAK);

  py::enum_<ApvRegister::ApvMode::ReadoutFrequency>("ReadoutFrequency")
      .value("F_20MHZ", ApvRegister::ApvMode::F_20MHZ)
      .value("F_40MHZ", ApvRegister::ApvMode::F_40MHZ);

  py::enum_<ApvRegister::ApvMode::PreampPolarity>("PreampPolarity")
      .value("POL_NONINV", ApvRegister::ApvMode::POL_NONINV)
      .value("POL_INV", ApvRegister::ApvMode::POL_INV);

  py::class_<ApvRegister::ApvMode>("ApvMode", py::init<uint32_t>())
      .add_property("analogueBias",
                    &ApvRegister::ApvMode::analogueBias,
                    py::make_function(&ApvRegister::ApvMode::setAnalogueBias, py::return_internal_reference<>()))
      .add_property("triggerMode",
                    &ApvRegister::ApvMode::triggerMode,
                    py::make_function(&ApvRegister::ApvMode::setTriggerMode, py::return_internal_reference<>()))
      .add_property("calibrationInhibit",
                    &ApvRegister::ApvMode::calibrationInhibit,
                    py::make_function(&ApvRegister::ApvMode::setCalibrationInhibit, py::return_internal_reference<>()))
      .add_property("readoutMode",
                    &ApvRegister::ApvMode::readoutMode,
                    py::make_function(&ApvRegister::ApvMode::setReadoutMode, py::return_internal_reference<>()))
      .add_property("readoutFrequency",
                    &ApvRegister::ApvMode::readoutFrequency,
                    py::make_function(&ApvRegister::ApvMode::setReadoutFrequency, py::return_internal_reference<>()))
      .add_property("preampPolarity",
                    &ApvRegister::ApvMode::preampPolarity,
                    py::make_function(&ApvRegister::ApvMode::setPreampPolarity, py::return_internal_reference<>()));

  ApvRegister::ApvMode (ApvRegister::*cst_mode)() const = &ApvRegister::mode;

  py::class_<ApvRegister>("ApvRegister")
      .def("__str__", &print_object<ApvRegister>)
      .def("save", save_config<ApvRegister>, save_config_apv_overloads())
      .add_property("error", &ApvRegister::error, "SEU or Sync error")
      .add_property("mode", cst_mode, py::make_function(&ApvRegister::setMode, py::return_internal_reference<>()))
      .add_property("latency",
                    &ApvRegister::latency,
                    py::make_function(&ApvRegister::setLatency, py::return_internal_reference<>()),
                    "Trigger latency")
      .add_property("muxGain",
                    &ApvRegister::muxGain,
                    py::make_function(&ApvRegister::setMuxGain, py::return_internal_reference<>()),
                    "Gain of the output buffer")
      .add_property("iPre",
                    &ApvRegister::iPre,
                    py::make_function(&ApvRegister::setIPre, py::return_internal_reference<>()),
                    "Preamplifier current")
      .add_property(
          "iPcasc", &ApvRegister::iPcasc, py::make_function(&ApvRegister::setIPcasc, py::return_internal_reference<>()))
      .add_property(
          "iPsf", &ApvRegister::iPsf, py::make_function(&ApvRegister::setIPsf, py::return_internal_reference<>()))
      .add_property(
          "iSha", &ApvRegister::iSha, py::make_function(&ApvRegister::setISha, py::return_internal_reference<>()))
      .add_property(
          "iSsf", &ApvRegister::iSsf, py::make_function(&ApvRegister::setISsf, py::return_internal_reference<>()))
      .add_property(
          "iPsp", &ApvRegister::iPsp, py::make_function(&ApvRegister::setIPsp, py::return_internal_reference<>()))
      .add_property("iMuxin",
                    &ApvRegister::iMuxin,
                    py::make_function(&ApvRegister::setIMuxin, py::return_internal_reference<>()),
                    "Output buffer pedestal control")
      .add_property("iCal",
                    &ApvRegister::iCal,
                    py::make_function(&ApvRegister::setICal, py::return_internal_reference<>()),
                    "Calibration pulse strength")
      .add_property(
          "vPsp", &ApvRegister::vPsp, py::make_function(&ApvRegister::setVPsp, py::return_internal_reference<>()))
      .add_property(
          "vFs", &ApvRegister::vFs, py::make_function(&ApvRegister::setVFs, py::return_internal_reference<>()))
      .add_property(
          "vFp", &ApvRegister::vFp, py::make_function(&ApvRegister::setVFp, py::return_internal_reference<>()))
      .add_property("cDrv",
                    &ApvRegister::cDrv,
                    py::make_function(&ApvRegister::setCDrv, py::return_internal_reference<>()),
                    "Calibration channel mask")
      .add_property("cSel",
                    &ApvRegister::cSel,
                    py::make_function(&ApvRegister::setCSel, py::return_internal_reference<>()),
                    "Calibraton fine phase (3.125 ns)");

  //-----------------------------------------------------------------------------------------------
  // PLL register
  //-----------------------------------------------------------------------------------------------

  py::class_<PllChannel>(
      "PllChannel", "Specialised channels pattern of inhibit for the PLL register", py::init<uint32_t>())
      .def("__str__", &print_object<PllChannel>)
      .def("__len__", &PllChannel::size, "Number of channel words contained")
      .def("all", &PllChannel::all)
      .staticmethod("all");

  py::class_<PllRegister::Csr1FineDelay>("Csr1FineDelay", py::init<uint32_t>())
      .add_property("phaseAdjust",
                    &PllRegister::Csr1FineDelay::phaseAdjust,
                    py::make_function(&PllRegister::Csr1FineDelay::setPhaseAdjust, py::return_internal_reference<>()),
                    "CLK fine-phase adjustment (value <= 11)")
      .add_property("phaseFlip",
                    &PllRegister::Csr1FineDelay::phaseFlip,
                    py::make_function(&PllRegister::Csr1FineDelay::setPhaseFlip, py::return_internal_reference<>()),
                    "CLK phase flip")
      .add_property(
          "triggerDelayEnabled",
          &PllRegister::Csr1FineDelay::triggerDelayEnabled,
          py::make_function(&PllRegister::Csr1FineDelay::setTriggerDelayEnabled, py::return_internal_reference<>()),
          "Enables access to TRG_DELAY register");

  PllRegister::Csr1FineDelay (PllRegister::*cst_csr1FineDelay)() const = &PllRegister::csr1FineDelay;

  py::class_<PllRegister>("PllRegister")
      .def("save", save_config<PllRegister>, save_config_pll_overloads())
      .add_property("csr1FineDelay",
                    cst_csr1FineDelay,
                    py::make_function(&PllRegister::setCsr1FineDelay, py::return_internal_reference<>()))
      .add_property("triggerDelay",
                    &PllRegister::triggerDelay,
                    py::make_function(&PllRegister::setTriggerDelay, py::return_internal_reference<>()),
                    "Trigger delay (in clock cycles)");

  //-----------------------------------------------------------------------------------------------
}
