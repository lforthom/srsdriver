#  srsdriver: a C++ driver for the SRS readout system
#  Copyright (C) 2021-2023  Laurent Forthomme
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pysrsdriver as srs
import argparse


def main():
    parser = argparse.ArgumentParser(description='Readout test')
    parser.add_argument('ipaddr', nargs='?', default='10.0.0.2')
    parser.add_argument('-d', '--debug', action='store_true', help='Debugging mode')
    arg = parser.parse_args()

    if arg.debug:
        srs.Logger.get().level = srs.LoggerLevel.debug

    sc = srs.SlowControl(arg.ipaddr)
    apvapp = sc.readApvAppRegister()
    ch_en = apvapp.ebChannelEnableMask()
    print(ch_en)

    sc.addFec(6006)
    # sc.readout = True
    if sc.readout:
        print('<<< readout enabled >>>')
        frames = sc.read(0)
        print('>>> frames:', len(frames))
        for frm in frames:
            print(frm)

    # sc.readout = False


if __name__ == '__main__':
    main()
