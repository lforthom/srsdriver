#  srsdriver: a C++ driver for the SRS readout system
#  Copyright (C) 2021-2023  Laurent Forthomme
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pysrsdriver as srs
import argparse


def main():
    parser = argparse.ArgumentParser(description='Readout test')
    parser.add_argument('ipaddr', nargs='?', default='10.0.0.2')
    parser.add_argument('-d', '--debug', action='store_true', help='Debugging mode')
    arg = parser.parse_args()

    if arg.debug:
        srs.Logger.get().level = srs.LoggerLevel.debug

    sc = srs.SlowControl(arg.ipaddr)

    # APV application register
    apvapp = sc.readApvAppRegister()

    trg = srs.TriggerMode()
    trg.apvReset = True
    trg.apvTestPulse = True
    apvapp.triggerMode = trg  # BCLK_MODE

    apvapp.triggerBurst = 8  # BCKL_TRGBURST
    apvapp.triggerSeqPeriod = 40000  # BCKL_FREQ
    apvapp.triggerDelay = 0x100  # BCKL_TRGDELAY
    apvapp.testPulseDelay = 0x84  # BCKL_TPDELAY
    apvapp.roSync = 300  # BCKL_ROSYNC

    poi = srs.ChannelEnable()
    poi.setMode(0, srs.ChannelEnableMode.ALL_APVS)
    poi.setMode(1, srs.ChannelEnableMode.ALL_APVS)
    apvapp.ebChannelEnableMask = poi  # EVBLD_CHMASK

    apvapp.ebCaptureWindow = 4000  # EVBLD_DATALENGTH
    apvapp.ebMode = srs.EventBuilderMode.TIMESTAMP  # EVBLD_MODE
    apvapp.ebDataFormat = 0  # EVBLD_EVENTINFOTYPE

    info = srs.EbEventInfoData()
    info.headerInfo = srs.HeaderInfo(0xb)
    info.runLabel = 0xaabb
    apvapp.ebEventInfoData = info  # EVBLD_EVENTINFODATA
    print(apvapp)

    # APV hybrid register
    apv = sc.readApvRegister(srs.ApvChannel.all(srs.ApvChannelMode.ALL_APVS))
    apv.mode = srs.ApvMode(0x19)
    apv.latency = 0x80
    apv.muxGain = 0x2
    apv.iPre = 0x62
    apv.iPcasc = 0x34
    apv.iPsf = 0x22
    apv.iSha = 0x22
    apv.iSsf = 0x22
    apv.iPsp = 0x37
    apv.iMuxin = 0x10
    apv.iCal = 0x64
    apv.vPsp = 0x28
    apv.vFs = 0x3c
    apv.vFp = 0x1e
    apv.cDrv = 0x17f
    apv.cSel = 0xf7
    print(apv)

    # PLL register
    pll = sc.readPllRegister(srs.PllChannel.all())
    pll.csr1FineDelay = srs.Csr1FineDelay(0x4)
    pll.triggerDelay = 0
    print(pll)


if __name__ == '__main__':
    main()
