set(CPACK_PACKAGE_VERSION ${VERSION})
set(CPACK_SET_DESTDIR ON)
set(CPACK_PACKAGE_NAME "srsdriver")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A slow control and readout library for SRS")
set(CPACK_PACKAGE_RELOCATABLE FALSE)
set(CPACK_PACKAGE_RELEASE 1)
set(CPACK_PACKAGE_CONTACT "Laurent Forthomme <laurent.forthomme@cern.ch>")
set(CPACK_PACKAGE_LICENSE GPL3+)
set(CPACK_PACKAGE_VENDOR "Helsinki Institute of Physics")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-${CPACK_PACKAGE_RELEASE}.${CMAKE_SYSTEM_PROCESSOR}")
set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md")
find_program(RPMBUILD rpmbuild QUIET)
if(RPMBUILD)
    #--- RPM information
    message(STATUS "RPM packaging utilitaries detected")
    set(CPACK_GENERATOR "RPM")
    set(CPACK_RPM_MAIN_COMPONENT lib)
    set(CPACK_RPM_FILE_NAME RPM-DEFAULT)
    #set(CPACK_RPM_CHANGELOG_FILE "${CMAKE_CURRENT_SOURCE_DIR}/CHANGELOG")
    #set(CPACK_RPM_PACKAGE_URL https://cepgen.hepforge.org/)
    #set(CPACK_RPM_PACKAGE_SOURCE ON)
    set(CPACK_RPM_ENABLE_COMPONENT_DEPENDS ON)
    #--- packages interdependencies
    set(CPACK_RPM_DEVEL_PACKAGE_REQUIRES "srsdriver == ${VERSION}")
    set(CPACK_RPM_DEVEL_PACKAGE_ARCHITECTURE noarch)
    set(CPACK_RPM_PYTHON_PACKAGE_REQUIRES "srsdriver == ${VERSION},python >= 2.7")
    set(CPACK_RPM_CONTROL_PACKAGE_REQUIRES "srsdriver == ${VERSION}")
    set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION
      /usr /usr/share /usr/bin /usr/lib /usr/lib64 /usr/include)
endif()
include(CPack)
#--- register packages
cpack_add_component(lib
    DISPLAY_NAME "srsdriver core library"
    DESCRIPTION "The full set of core libraries"
    REQUIRED)
cpack_add_component(devel
    DISPLAY_NAME "srsdriver development headers"
    DESCRIPTION "Collection of C++ includes for the development of dependent libraries"
    DEPENDS lib)
cpack_add_component(python
    DISPLAY_NAME "pysrsdriver Python bindings"
    DESCRIPTION "Boost-powered Python binding to the SRS library"
    DEPENDS lib)
cpack_add_component(control
    DISPLAY_NAME "GUI control software for srsdriver"
    DESCRIPTION "User-friendly Qt-based GUI control software for SRS"
    DEPENDS lib)
set(CPACK_COMPONENTS_ALL lib devel python control)

