#! /bin/bash

echo "======== CONFIGURATION OF SRS CARD 1"
echo ""
echo "set 10.0.0.2 -> 10.0.0.4"
./srsSlowControl config/scripts/set_ip0.txt $1
sleep 0.1
echo "ADC_0 config"
./srsSlowControl config/scripts/adc_card0.txt $1
sleep 0.1
echo "FEC_0 config"
./srsSlowControl config/scripts/fec0PhysicsRun.txt $1
sleep 0.1
echo "APV_0 config"
./srsSlowControl config/scripts/apv0.txt $1
sleep 0.1
echo "APV_0 reset"
./srsSlowControl config/scripts/fec0apvreset.txt $1
sleep 0.1
echo "PLL_0 config"
./srsSlowControl config/scripts/pll0.txt $1
echo ""
