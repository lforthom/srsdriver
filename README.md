# SRS C++ driver

These library and executables are tools to interact with a UDP socket handled by the SRS readout component through simple objects.

## Usage

The usage of this library can be found [in the project wiki](https://gitlab.cern.ch/lforthom/srsdriver/-/wikis/home).

## Dependencies

For the Python bindings, the following packages are required:

- `boost-python` and `boost-devel`
- `python3` (obviously) and `python3-devel`
