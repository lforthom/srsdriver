/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srsdriver/Messenger.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"

int main(int argc, char* argv[]) {
  std::string input_config;
  bool debug, dry_run;

  srs::utils::ArgumentsParser(argc, argv)
      .addArgument("config,i", "path to the configuration file", &input_config)
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .addOptionalArgument("dry-run,x", "do not send the configuration", &dry_run, false)
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);

  std::string addr;
  srs::port_t port{};
  const auto config = srs::Messenger::parseCommands(input_config, addr, port);
  if (!dry_run) {
    srs::Messenger msg(addr);
    if (msg.send(port, config))
      SRS_INFO("main") << config.size() << "-word command sent "
                       << "to port " << port << " at address " << addr << "!";
    else
      SRS_ERROR("main") << "failed to send command "
                        << "to port " << port << " at address " << addr << "!";
  }

  return 0;
}
