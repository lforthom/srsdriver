/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srsdriver/AdcCardRegister.h"
#include "srsdriver/ApvAppRegister.h"
#include "srsdriver/ApvRegister.h"
#include "srsdriver/PllRegister.h"
#include "srsdriver/SystemRegister.h"
#include "srsutils/JsonConverter.h"

using json = nlohmann::json;

namespace srs {
  void to_json(json& js, const AdcCardRegister::ChannelPattern& reg) {
    js = json{};
    for (size_t i = 0; i < 8; ++i)
      js[i] = reg.enabled(i);
  }

  void from_json(const json& js, AdcCardRegister::ChannelPattern& reg) {
    for (size_t i = 0; i < 8; ++i)
      reg.setEnabled(i, js[i].template get<bool>());
  }

  void to_json(json& js, const AdcCardRegister& reg) {
    js = json{{"hybridResetPin", reg.hybridResetPin()},
              {"powerDownCh0", reg.powerDownCh0()},
              {"powerDownCh1", reg.powerDownCh1()},
              {"eqLevel0", reg.eqLevel0()},
              {"eqLevel1", reg.eqLevel1()},
              {"triggerOut", reg.triggerOut()},
              {"bclkBuffer", reg.bclkBuffer()}};
  }

  void from_json(const json& js, AdcCardRegister& reg) {
    reg.setHybridResetPin(js["hybridResetPin"].template get<AdcCardRegister::ChannelPattern>())
        .setPowerDownCh0(js["powerDownCh0"].template get<AdcCardRegister::ChannelPattern>())
        .setPowerDownCh1(js["powerDownCh1"].template get<AdcCardRegister::ChannelPattern>())
        .setEqLevel0(js["eqLevel0"].template get<AdcCardRegister::ChannelPattern>())
        .setEqLevel1(js["eqLevel1"].template get<AdcCardRegister::ChannelPattern>())
        .setTriggerOut(js["triggerOut"].template get<AdcCardRegister::ChannelPattern>())
        .setBclkBuffer(js["bclkBuffer"].template get<AdcCardRegister::ChannelPattern>());
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(ApvRegister::ApvMode::TriggerMode,
                               {{ApvRegister::ApvMode::TriggerMode::T_3SAMPLE, 3},
                                {ApvRegister::ApvMode::TriggerMode::T_1SAMPLE, 1}})

  NLOHMANN_JSON_SERIALIZE_ENUM(ApvRegister::ApvMode::ReadoutMode,
                               {{ApvRegister::ApvMode::ReadoutMode::RO_DECONVOLUTION, "deconvolution"},
                                {ApvRegister::ApvMode::ReadoutMode::RO_PEAK, "peak"}})

  NLOHMANN_JSON_SERIALIZE_ENUM(ApvRegister::ApvMode::ReadoutFrequency,
                               {{ApvRegister::ApvMode::ReadoutFrequency::F_20MHZ, "20MHz"},
                                {ApvRegister::ApvMode::ReadoutFrequency::F_40MHZ, "40MHz"}})

  NLOHMANN_JSON_SERIALIZE_ENUM(ApvRegister::ApvMode::PreampPolarity,
                               {{ApvRegister::ApvMode::PreampPolarity::POL_NONINV, "inverted"},
                                {ApvRegister::ApvMode::PreampPolarity::POL_INV, "direct"}})

  void to_json(json& js, const ApvRegister::ApvMode& reg) {
    js = json{{"analogueBias", reg.analogueBias()},
              {"triggerMode", reg.triggerMode()},
              {"calibrationInhibit", reg.calibrationInhibit()},
              {"readoutMode", reg.readoutMode()},
              {"readoutFrequency", reg.readoutFrequency()},
              {"preampPolarity", reg.preampPolarity()}};
  }

  void from_json(const json& js, ApvRegister::ApvMode& reg) {
    reg.setAnalogueBias(js["analogueBias"].template get<bool>())
        .setTriggerMode(js["triggerMode"].template get<ApvRegister::ApvMode::TriggerMode>())
        .setCalibrationInhibit(js["calibrationInhibit"].template get<bool>())
        .setReadoutMode(js["readoutMode"].template get<ApvRegister::ApvMode::ReadoutMode>())
        .setReadoutFrequency(js["readoutFrequency"].template get<ApvRegister::ApvMode::ReadoutFrequency>())
        .setPreampPolarity(js["preampPolarity"].template get<ApvRegister::ApvMode::PreampPolarity>());
  }

  void to_json(json& js, const ApvRegister& reg) {
    js = json{{"error", reg.error()},
              {"mode", reg.mode()},
              {"latency", reg.latency()},
              {"muxGain", reg.muxGain()},
              {"iPre", reg.iPre()},
              {"iPcasc", reg.iPcasc()},
              {"iPsf", reg.iPsf()},
              {"iSha", reg.iSha()},
              {"iSsf", reg.iSsf()},
              {"iPsp", reg.iPsp()},
              {"iMuxin", reg.iMuxin()},
              {"iCal", reg.iCal()},
              {"vPsp", reg.vPsp()},
              {"vFs", reg.vFs()},
              {"vFp", reg.vFp()},
              {"cDrv", reg.cDrv()},
              {"cSel", reg.cSel()}};
  }

  void from_json(const json& js, ApvRegister& reg) {
    reg.setMode(js["mode"].template get<ApvRegister::ApvMode>())
        .setLatency(js["latency"].template get<int>())
        .setMuxGain(js["muxGain"].template get<int>())
        .setIPre(js["iPre"].template get<int>())
        .setIPcasc(js["iPcasc"].template get<int>())
        .setIPsf(js["iPsf"].template get<int>())
        .setISha(js["iSha"].template get<int>())
        .setISsf(js["iSsf"].template get<int>())
        .setIPsp(js["iPsp"].template get<int>())
        .setIMuxin(js["iMuxin"].template get<int>())
        .setICal(js["iCal"].template get<int>())
        .setVPsp(js["vPsp"].template get<int>())
        .setVFs(js["vFs"].template get<int>())
        .setVFp(js["vFp"].template get<int>())
        .setCDrv(js["cDrv"].template get<int>())
        .setCSel(js["cSel"].template get<int>());
  }

  void to_json(json& js, const ApvAppRegister::TriggerMode& reg) {
    js = json{{"apvReset", reg.apvReset()},
              {"apvTestPulse", reg.apvTestPulse()},
              {"externalTrigger", reg.externalTrigger()},
              {"nimTriggerPolarity", reg.nimTriggerPolarity()}};
  }

  void from_json(const json& js, ApvAppRegister::TriggerMode& reg) {
    reg.setApvReset(js["apvReset"].template get<bool>())
        .setApvTestPulse(js["apvTestPulse"].template get<bool>())
        .setExternalTrigger(js["externalTrigger"].template get<bool>())
        .setNimTriggerPolarity(js["nimTriggerPolarity"].template get<bool>());
  }

  void to_json(json& js, const ApvAppRegister::EbEventInfoData& reg) {
    js = json{{"headerInfo", reg.headerInfo()}, {"runLabel", reg.runLabel()}};
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(
      ApvAppRegister::EbEventInfoData::HeaderInfo,
      {{ApvAppRegister::EbEventInfoData::HeaderInfo::LABEL_AND_DATALENGTH, "labelAndDataLength"},
       {ApvAppRegister::EbEventInfoData::HeaderInfo::TRIGGER_AND_DATALENGTH, "triggerAndDataLength"},
       {ApvAppRegister::EbEventInfoData::HeaderInfo::TRIGGER_ONLY, "triggerOnly"}})

  void from_json(const json& js, ApvAppRegister::EbEventInfoData& reg) {
    reg.setHeaderInfo(js["headerInfo"].template get<ApvAppRegister::EbEventInfoData::HeaderInfo>())
        .setRunLabel(js["runLabel"].template get<int>());
  }

  void to_json(json& js, const ApvAppRegister::ApzZeroSuppThreshold& reg) {
    js = json{{"fractionalThreshold", reg.fractionalThreshold()}, {"integerThreshold", reg.integerThreshold()}};
  }

  void from_json(const json& js, ApvAppRegister::ApzZeroSuppThreshold& reg) {
    reg.setFractionalThreshold(js["fractionalThreshold"].template get<int>())
        .setIntegerThreshold(js["integerThreshold"].template get<int>());
  }

  void to_json(json& js, const ApvAppRegister::ApzZeroSuppParameters& reg) {
    js = json{{"peakFindMode", reg.peakFindMode()},
              {"disablePedestalCorrection", reg.disablePedestalCorrection()},
              {"forceSignal", reg.forceSignal()},
              {"thresholdMode", reg.thresholdMode()}};
  }

  void from_json(const json& js, ApvAppRegister::ApzZeroSuppParameters& reg) {
    reg.setPeakFindMode(js["peakFindMode"].template get<bool>())
        .setDisablePedestalCorrection(js["disablePedestalCorrection"].template get<bool>())
        .setForceSignal(js["forceSignal"].template get<bool>())
        .setThresholdMode(js["thresholdMode"].template get<bool>());
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(ApvAppRegister::EventBuilderMode,
                               {{ApvAppRegister::EventBuilderMode::FRAME_EVENT_CNT, "frameEventCounter"},
                                {ApvAppRegister::EventBuilderMode::FRAME_RUN_CNT, "frameRunCounter"},
                                {ApvAppRegister::EventBuilderMode::TIMESTAMP, "timestamp"}})

  void to_json(json& js, const ApvAppRegister& reg) {
    js = json{{"triggerMode", reg.triggerMode()},
              {"triggerBurst", reg.triggerBurst()},
              {"triggerSeqPeriod", reg.triggerSeqPeriod()},
              {"triggerDelay", reg.triggerDelay()},
              {"testPulseDelay", reg.testPulseDelay()},
              {"roSync", reg.roSync()},
              {"ebCaptureWindow", reg.ebCaptureWindow()},
              {"ebMode", reg.ebMode()},
              {"ebDataFormat", reg.ebDataFormat()},
              {"ebEventInfoData", reg.ebEventInfoData()},
              {"readoutEnabled", reg.readoutEnabled()},
              {"apzApvSelect", reg.apzApvSelect()},
              {"apzNumSamples", reg.apzNumSamples()},
              {"apzZeroSuppThreshold", reg.apzZeroSuppThreshold()},
              {"apzZeroSuppParameters", reg.apzZeroSuppParameters()},
              {"apvSyncLowThreshold", reg.apvSyncLowThreshold()},
              {"apvSyncHighThreshold", reg.apvSyncHighThreshold()},
              {"apzCommand", reg.apzCommand()}};
  }

  void from_json(const json& js, ApvAppRegister& reg) {
    reg.setTriggerMode(js["triggerMode"].template get<ApvAppRegister::TriggerMode>())
        .setTriggerBurst(js["triggerBurst"].template get<int>())
        .setTriggerSeqPeriod(js["triggerSeqPeriod"].template get<int>())
        .setTriggerDelay(js["triggerDelay"].template get<int>())
        .setTestPulseDelay(js["testPulseDelay"].template get<int>())
        .setRoSync(js["roSync"].template get<int>())
        .setEbCaptureWindow(js["ebCaptureWindow"].template get<int>())
        .setEbMode(js["ebMode"].template get<ApvAppRegister::EventBuilderMode>())
        .setEbDataFormat(js["ebDataFormat"].template get<int>())
        .setEbEventInfoData(js["ebEventInfoData"].template get<ApvAppRegister::EbEventInfoData>())
        .setReadoutEnabled(js["readoutEnabled"].template get<bool>())
        .setApzApvSelect(js["apzApvSelect"].template get<int>())
        .setApzNumSamples(js["apzNumSamples"].template get<int>())
        .setApzZeroSuppThreshold(js["apzZeroSuppThreshold"].template get<ApvAppRegister::ApzZeroSuppThreshold>())
        .setApzZeroSuppParameters(js["apzZeroSuppParameters"].template get<ApvAppRegister::ApzZeroSuppParameters>())
        .setApvSyncLowThreshold(js["apvSyncLowThreshold"].template get<int>())
        .setApvSyncHighThreshold(js["apvSyncHighThreshold"].template get<int>())
        .setApzCommand(js["apzCommand"].template get<int>());
  }

  void to_json(json& js, const PllRegister::Csr1FineDelay& reg) {
    js = json{{"phaseAdjust", reg.phaseAdjust()},
              {"phaseFlip", reg.phaseFlip()},
              {"triggerDelayEnabled", reg.triggerDelayEnabled()}};
  }

  void from_json(const json& js, PllRegister::Csr1FineDelay& reg) {
    reg.setPhaseAdjust(js["phaseAdjust"].template get<int>())
        .setPhaseFlip(js["phaseFlip"].template get<bool>())
        .setTriggerDelayEnabled(js["triggerDelayEnabled"].template get<bool>());
  }

  void to_json(json& js, const PllRegister& reg) {
    js = json{{"csr1FineDelay", reg.csr1FineDelay()}, {"triggerDelay", reg.triggerDelay()}};
  }

  void from_json(const json& js, PllRegister& reg) {
    reg.setCsr1FineDelay(js["csr1FineDelay"].template get<PllRegister::Csr1FineDelay>())
        .setTriggerDelay(js["triggerDelay"].template get<int>());
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(SystemRegister::DtcConfig::PaddingType,
                               {{SystemRegister::DtcConfig::PaddingType::NONE, "none"},
                                {SystemRegister::DtcConfig::PaddingType::P_16BIT, "16bit"},
                                {SystemRegister::DtcConfig::PaddingType::P_32BIT, "32bit"},
                                {SystemRegister::DtcConfig::PaddingType::P_64BIT, "64bit"}})

  void to_json(json& js, const SystemRegister::DtcConfig& reg) {
    js = json{{"dataOverEth", reg.dataOverEth()},
              {"flowControl", reg.flowControl()},
              {"paddingType", reg.paddingType()},
              {"dataChannelTrgId", reg.dataChannelTrgId()},
              {"dataFrameTrgId", reg.dataFrameTrgId()},
              {"trailerWordCount", reg.trailerWordCount()},
              {"paddingByte", reg.paddingByte()},
              {"trailerByte", reg.trailerByte()}};
  }

  void from_json(const json& js, SystemRegister::DtcConfig& reg) {
    reg.setDataOverEth(js["dataOverEth"].template get<bool>())
        .setFlowControl(js["flowControl"].template get<bool>())
        .setPaddingType(js["paddingType"].template get<SystemRegister::DtcConfig::PaddingType>())
        .setDataChannelTrgId(js["dataChannelTrgId"].template get<bool>())
        .setDataFrameTrgId(js["dataFrameTrgId"].template get<bool>())
        .setTrailerWordCount(js["trailerWordCount"].template get<int>())
        .setPaddingByte(js["paddingByte"].template get<int>())
        .setTrailerByte(js["trailerByte"].template get<int>());
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(SystemRegister::ClockSource,
                               {{SystemRegister::ClockSource::UNKNOWN, nullptr},
                                {SystemRegister::ClockSource::LOCAL, "local"},
                                {SystemRegister::ClockSource::DTC, "dtc"},
                                {SystemRegister::ClockSource::ETH, "eth"}})

  void to_json(json& js, const SystemRegister::ClockSel& reg) {
    js = json{{"dtcClockInhibit", reg.dtcClockInhibit()},
              {"dtcTriggerInhibit", reg.dtcTriggerInhibit()},
              {"dtcSwapPorts", reg.dtcSwapPorts()},
              {"dtcSwapLanes", reg.dtcSwapLanes()},
              {"dtcTriggerPolInvert", reg.dtcTriggerPolInvert()},
              {"ethernetClockSel", reg.ethernetClockSel()}};
  }

  void from_json(const json& js, SystemRegister::ClockSel& reg) {
    reg.setDtcClockInhibit(js["dtcClockInhibit"].template get<bool>())
        .setDtcTriggerInhibit(js["dtcTriggerInhibit"].template get<bool>())
        .setDtcSwapPorts(js["dtcSwapPorts"].template get<bool>())
        .setDtcSwapLanes(js["dtcSwapLanes"].template get<bool>())
        .setDtcTriggerPolInvert(js["dtcTriggerPolInvert"].template get<bool>())
        .setEthernetClockSel(js["ethernetClockSel"].template get<bool>());
  }

  void to_json(json& js, const SystemRegister& reg) {
    js = json{{"fwVersion", reg.fwVersion()},
              {"vendorId", reg.vendorId()},
              {"macId", reg.macId()},
              {"fecIP", reg.fecIP()},
              {"daqIP", reg.daqIP()},
              {"daqPort", reg.daqPort()},
              {"scPort", reg.scPort()},
              {"hwVersion", reg.hwVersion()},
              {"dtcConfig", reg.dtcConfig()},
              {"clockSource", reg.clockSource()},
              {"clockSelection", reg.clockSelection()}};
  }

  void from_json(const json& js, SystemRegister& reg) {
    reg.setFecIP(js["fecIP"].template get<std::string>())
        .setDaqIP(js["daqIP"].template get<std::string>())
        .setDtcConfig(js["dtcConfig"].template get<SystemRegister::DtcConfig>())
        .setClockSelection(js["clockSelection"].template get<SystemRegister::ClockSel>());
  }
}  // namespace srs
