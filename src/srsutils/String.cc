/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>  // For va_start, etc.

#include <array>
#include <iterator>

#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  namespace utils {
    std::string format(const std::string& fmt, ...) {
      int size = ((int)fmt.size()) * 2 + 50;
      std::string str;
      va_list ap;
      while (true) {
        //--- maximum two passes on a POSIX system...
        str.resize(size);
        va_start(ap, fmt);
        int n = vsnprintf(static_cast<char*>(str.data()), size, fmt.c_str(), ap);
        va_end(ap);
        //--- check if everything worked
        if (n > -1 && n < size) {
          str.resize(n);
          return str;
        }
        size = (n > -1) ? n + 1 : size * 2;
      }
      return str;
    }

    std::string colourise(const std::string& str, const Colour& col, const Modifier& mod) {
      if (!Logger::get().pretty())
        return str;
      if (mod == Modifier::reset)
        return format("\033[%dm%s\033[0m", (int)col, str.c_str());
      if (col == Colour::reset)
        return format("\033[%dm%s\033[0m", (int)mod, str.c_str());
      return format("\033[%d;%dm%s\033[0m", (int)col, (int)mod, str.c_str());
    }

    std::string now() {
      static constexpr size_t max_len = 25;
      std::array<char, max_len> buffer{};
      time_t raw_time{};
      struct tm buf {};
      time(&raw_time);
      const struct tm* time_info = localtime_r(&raw_time, &buf);
      strftime(buffer.data(), max_len, "%Y-%d-%m %H:%M:%S", time_info);
      return buffer.data();
    }

    std::vector<std::string> split(const std::string& str, char delim) {
      std::vector<std::string> out;
      std::string token;
      std::istringstream iss(str);
      while (std::getline(iss, token, delim))
        out.emplace_back(token);
      return out;
    }

    std::string merge(const std::vector<std::string>& vec, const std::string& delim) {
      if (vec.empty())
        return std::string();
      if (vec.size() == 1)
        return vec.at(0);
      std::ostringstream oss;
      std::copy(vec.begin(), std::prev(vec.end()), std::ostream_iterator<std::string>(oss, delim.c_str()));
      return oss.str() + *vec.rbegin();
    }

    std::string tolower(const std::string& str) {
      std::string out;
      out.resize(str.size());
      std::transform(str.begin(), str.end(), out.begin(), ::tolower);
      return out;
    }

    std::string s(const std::string& str, int num, bool print_num) {
      std::ostringstream oss;
      if (print_num)
        oss << num << " ";
      oss << str;
      if (abs(num) > 1)
        oss << "s";
      return oss.str();
    }
  }  // namespace utils
}  // namespace srs
