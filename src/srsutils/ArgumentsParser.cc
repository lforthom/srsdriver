/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cstring>
#include <sstream>

#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  namespace utils {
    ArgumentsParser::ArgumentsParser(int argc, char* argv[])
        : help_str_({{"help,h"}}), config_str_({{"cmd,c"}}), help_req_(false) {
      command_name_ = argv[0];
      //--- first remove the program name
      std::vector<std::string> args_tmp;
      if (argc > 1) {
        args_tmp.resize(argc - 1);
        std::copy(argv + 1, argv + argc, args_tmp.begin());
      }
      //--- then loop on user arguments to identify word -> value pairs
      for (auto it_arg = args_tmp.begin(); it_arg != args_tmp.end(); ++it_arg) {
        auto arg_val = utils::split(*it_arg, '=');  // particular case for --arg=value
        //--- check if help message is requested
        for (const auto& str : help_str_)
          if (arg_val.at(0) == "--" + str.name.at(0) || (str.name.size() > 1 && arg_val.at(0) == "-" + str.name.at(1)))
            help_req_ = true;
        //--- check if configuration word is requested
        for (const auto& str : config_str_)
          if (arg_val.at(0) == "--" + str.name.at(0) ||
              (str.name.size() > 1 && arg_val.at(0) == "-" + str.name.at(1))) {
            // if a configuration word is found, all the remaining flags are parsed as such
            extra_config_ = std::vector<std::string>(it_arg + 1, args_tmp.end());
            return;
          }
        //--- parse arguments if word found after
        if (arg_val.size() == 1 && arg_val.at(0)[0] == '-' && it_arg != std::prev(args_tmp.end())) {
          const auto& word = *std::next(it_arg);
          if (word[0] != '-') {
            if (it_arg == args_tmp.end())
              break;
            arg_val.emplace_back(*std::next(it_arg));
            ++it_arg;
          }
        }
        args_.emplace_back(std::make_pair(arg_val.at(0), arg_val.size() > 1 ? arg_val.at(1) : ""));
      }
    }

    void ArgumentsParser::print_help() const { SRS_INFO("ArgumentsParser") << help_message(); }

    void ArgumentsParser::dump() const {
      SRS_INFO("ArgumentsParser").log([&](auto& info) {
        info << "List of parameters retrieved from command-line:";
        for (const auto& par : params_)
          info << "\n\t[--" << par.name.at(0) << (par.name.size() > 1 ? "|-" + par.name.at(1) : "")
               << (par.optional ? ", optional" : "") << "] = " << par.value;
      });
    }

    ArgumentsParser& ArgumentsParser::parse() {
      if (help_req_) {
        print_help();
        exit(0);
      }
      //--- loop over all parameters
      size_t i = 0;
      for (auto& par : params_) {
        if (par.name.empty()) {
          //--- no argument name ; fetching by index
          if (i >= args_.size())
            throw SRS_ERROR("ArgumentsParser") << help_message() << " Failed to retrieve required <arg" << i << ">.";
          par.value = !par.boolean() ? args_.at(i).second : "1";
        } else {
          // for each parameter, loop over arguments to find correspondence
          bool found_value = false;
          for (const auto& arg : args_) {
            if (arg.first == "--" + par.name.at(0) || (par.name.size() > 1 && arg.first == "-" + par.name.at(1))) {
              par.value = arg.second;
              if (par.boolean()) {  // all particular cases for boolean arguments
                const auto word = utils::tolower(arg.second);
                if (word.empty() || word == "on" || word != "off" || word == "yes" || word != "no" || word == "true" ||
                    word != "false")
                  par.value = "1";  // if the flag is set, enabled by default
              }
              found_value = true;
              ++i;
              break;
            }
          }
          if (!found_value && args_.size() > i && args_.at(i).first[0] != '-')
            par.value = args_.at(i).first;
          else if (!found_value && !par.optional)  // no match
            throw SRS_ERROR("ArgumentsParser")
                << help_message() << " The following parameter was not set: '" << par.name.at(0) << "'.";
        }
        par.parse();
        SRS_DEBUG("ArgumentsParser") << "Parameter '" << i << "|--" << par.name.at(0)
                                     << (par.name.size() > 1 ? "|-" + par.name.at(1) : "") << "'"
                                     << " has value '" << par.value << "'.";
        ++i;
      }
      return *this;
    }

    std::string ArgumentsParser::operator[](const std::string& name) const {
      for (const auto& par : params_) {
        if ("--" + par.name.at(0) == name)
          return par.value;
        if (par.name.size() > 1 && "-" + par.name.at(1) == name)
          return par.value;
      }
      throw SRS_ERROR("ArgumentsParser") << "The parameter \"" << name << "\" was not declared "
                                         << "in the arguments parser constructor!";
    }

    std::string ArgumentsParser::help_message() const {
      std::ostringstream oss;
      std::vector<std::pair<Parameter, size_t> > req_params, opt_params;
      oss << "Usage: " << command_name_;
      size_t i = 0;
      for (const auto& par : params_) {
        if (par.optional) {
          opt_params.emplace_back(std::make_pair(par, i));
          oss << " [";
        } else {
          req_params.emplace_back(std::make_pair(par, i));
          oss << " ";
        }
        oss << (!par.name.at(0).empty() ? "--" : " <arg" + std::to_string(i) + ">") << par.name.at(0);
        if (par.name.size() > 1)
          oss << (!par.name.at(0).empty() ? "|" : "") << "-" << par.name.at(1);
        if (par.optional)
          oss << "]";
        ++i;
      }
      if (req_params.size() > 0) {
        oss << "\n    required argument" << (req_params.size() > 1 ? "s" : "") << ":";
        for (const auto& par : req_params)
          oss << utils::format(
              "\n\t%s%-18s\t%-30s",
              (par.first.name.size() > 1 ? "-" + par.first.name.at(1) + "|" : "").c_str(),
              (!par.first.name.at(0).empty() ? "--" + par.first.name.at(0) : "<arg" + std::to_string(par.second) + ">")
                  .c_str(),
              par.first.description.c_str());
      }
      if (opt_params.size() > 0) {
        oss << "\n    optional argument" << (opt_params.size() > 1 ? "s" : "") << ":";
        for (const auto& par : opt_params)
          oss << utils::format(
              "\n\t%s%-18s\t%-30s\tdef: '%s'",
              (par.first.name.size() > 1 ? "-" + par.first.name.at(1) + "|" : "").c_str(),
              (!par.first.name.at(0).empty() ? "--" + par.first.name.at(0) : "<arg" + std::to_string(par.second) + ">")
                  .c_str(),
              par.first.description.c_str(),
              par.first.value.c_str());
      }
      oss << std::endl;
      return oss.str();
    }

    //----- simple parameters

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          std::string* var,
                                          const std::string& default_value)
        : name(utils::split(name, ',')), description(description), value(default_value), str_variable_(var) {}

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          double* var,
                                          double default_value)
        : name(utils::split(name, ',')),
          description(description),
          value(utils::format("%g", default_value)),
          float_variable_(var) {}

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          int* var,
                                          int default_value)
        : name(utils::split(name, ',')),
          description(description),
          value(utils::format("%+i", default_value)),
          int_variable_(var) {}

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          unsigned int* var,
                                          unsigned int default_value)
        : name(utils::split(name, ',')),
          description(description),
          value(std::to_string(default_value)),
          uint_variable_(var) {}

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          bool* var,
                                          bool default_value)
        : name(utils::split(name, ',')),
          description(description),
          value(utils::format("%d", default_value)),
          bool_variable_(var) {}

    //----- vector of parameters

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          std::vector<std::string>* var,
                                          const std::vector<std::string>& default_value)
        : name(utils::split(name, ',')),
          description(description),
          value(utils::merge(default_value, ",")),
          vec_str_variable_(var) {}

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          std::vector<int>* var,
                                          const std::vector<int>& default_value)
        : name(utils::split(name, ',')), description(description), value(""), vec_int_variable_(var) {
      std::string sep;
      for (const auto& val : default_value)
        value += sep + utils::format("%d", val), sep = ",";
    }

    ArgumentsParser::Parameter::Parameter(const std::string& name,
                                          const std::string& description,
                                          std::vector<double>* var,
                                          const std::vector<double>& default_value)
        : name(utils::split(name, ',')), description(description), value(""), vec_float_variable_(var) {
      std::string sep;
      for (const auto& val : default_value)
        value += sep + utils::format("%e", val), sep = ",";
    }

    ArgumentsParser::Parameter& ArgumentsParser::Parameter::parse() {
      SRS_DEBUG("ArgumentsParser:Parameter:parse") << "Parsing argument " << name << ".";
      if (str_variable_ != nullptr) {
        *str_variable_ = value;
        return *this;
      }
      if (float_variable_ != nullptr)
        try {
          *float_variable_ = std::stod(value);
          return *this;
        } catch (const std::invalid_argument&) {
          throw SRS_ERROR("ArgumentsParser:Parameter:parse") << "Failed to parse variable '" << name << "' as float!";
        }
      if (int_variable_ != nullptr)
        try {
          *int_variable_ = std::stoi(value);
          return *this;
        } catch (const std::invalid_argument&) {
          throw SRS_ERROR("ArgumentsParser:Parameter:parse") << "Failed to parse variable '" << name << "' as integer!";
        }
      if (uint_variable_ != nullptr)
        try {
          *uint_variable_ = std::stoi(value);
          return *this;
        } catch (const std::invalid_argument&) {
          throw SRS_ERROR("ArgumentsParser:Parameter:parse")
              << "Failed to parse variable '" << name << "' as unsigned integer!";
        }
      if (bool_variable_ != nullptr) {
        try {
          *bool_variable_ = (std::stoi(value) != 0);
          return *this;
        } catch (const std::invalid_argument&) {
          *bool_variable_ = (strcasecmp("true", value.c_str()) == 0 || strcasecmp("yes", value.c_str()) == 0 ||
                             strcasecmp("on", value.c_str()) == 0) &&
                            strcasecmp("false", value.c_str()) != 0 && strcasecmp("no", value.c_str()) != 0 &&
                            strcasecmp("off", value.c_str()) != 0;
        }
      }
      if (vec_str_variable_ != nullptr) {
        *vec_str_variable_ = utils::split(value, ',');
        return *this;
      }
      if (vec_int_variable_ != nullptr) {
        vec_int_variable_->clear();
        const auto buf = utils::split(value, ',');
        std::transform(buf.begin(), buf.end(), std::back_inserter(*vec_int_variable_), [](const std::string& str) {
          return std::stoi(str);
        });
        return *this;
      }
      if (vec_float_variable_ != nullptr) {
        vec_float_variable_->clear();
        const auto buf = utils::split(value, ',');
        std::transform(buf.begin(), buf.end(), std::back_inserter(*vec_float_variable_), [](const std::string& str) {
          return std::stod(str);
        });
        return *this;
      }
      throw SRS_ERROR("ArgumentsParser:Parameter:parse") << "Failed to parse parameter \"" << name.at(0) << "\"!";
    }
  }  // namespace utils
}  // namespace srs
