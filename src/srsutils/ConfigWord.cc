/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsutils/ConfigWord.h"
#include "srsutils/Logging.h"

namespace srs {
  ConfigWord::ConfigWord(word_t word) : Word(word), orig_word_(word) {}

  ConfigWord::ConfigWord(const ConfigWord& oth) : Word(oth), orig_word_(oth.orig_word_) {}

  ConfigWord::~ConfigWord() {}

  ConfigWord& ConfigWord::operator=(const ConfigWord& oth) {
    word_ = oth.word_;
    orig_word_ = oth.orig_word_;
    return *this;
  }

  ConfigWord& ConfigWord::set(uint16_t lsb, uint32_t seq, uint16_t size) {
    if (size == 0 || size >= WORD_LENGTH)
      throw SRS_ERROR("ConfigWord:set") << "Invalid size for bit to be set: " << size << ".";
    bitset_t bs(word_);
    for (size_t i = 0; i < size; ++i)
      bs[i + lsb] = ((seq >> i) & 0x1);
    word_ = bs.to_ulong();
    return *this;
  }
}  // namespace srs
