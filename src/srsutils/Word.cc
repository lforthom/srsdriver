/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsutils/Logging.h"
#include "srsutils/String.h"
#include "srsutils/Word.h"

namespace srs {
  Word::Word(word_t word) : word_(word) {}

  Word::Word(const Word& oth) : word_(oth.word_) {}

  Word::~Word() {}

  Word& Word::operator=(const Word& oth) {
    word_ = oth.word_;
    return *this;
  }

  void Word::print(std::ostream& os) const {
    os << __PRETTY_FUNCTION__ << "\t" << utils::format("0x%08x", word_) << ": " << std::bitset<WORD_LENGTH>(word_)
       << std::endl;
  }
}  // namespace srs
