/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <numeric>  // accumulate

#include "srsdriver/ApvAddress.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsreadout/AdcData.h"
#include "srsutils/ApvAdcCalibration.h"
#include "srsutils/Logging.h"

namespace srs {
  namespace utils {
    ApvAdcCalibration::ApvAdcCalibration(SlowControl& sc)
        : running_({false}), sc_(sc), num_samples_baseline_(20), num_printout_(10) {
      if (sc_.numFec() == 0)
        throw SRS_ERROR("ApvAdcCalibration") << "Failed to retrieve at least one FEC to proceed to calibration!";
    }

    ApvAdcCalibration::chanvalues_t ApvAdcCalibration::run(uint16_t ical, size_t num_events) {
      chanvalues_t max_ampl_vs_chan;
      const uint32_t poi = 0xf;
      auto apv_chan = srs::ApvAddress(poi, srs::ApvAddress::ALL_APVS);
      running_ = {false};
      sc_.setReadoutEnable(false);

      auto apvreg = sc_.readApvRegister(apv_chan);

      SRS_INFO("ApvAdcCalibration:run") << "ICAL = " << ical << ".";

      apvreg.setICal(ical);
      sc_.messenger().writeConfig(srs::P_APV, apv_chan, apvreg);
      sc_.setReadoutEnable(true);
      running_ = {true};
      for (size_t i = 0; i < num_events; ++i) {
        if ((i + 1) % num_printout_ == 0)
          SRS_INFO("ApvAdcCalibration:run") << "Event " << i + 1 << " / " << num_events;
        for (size_t j = 0; j < sc_.numFec(); ++j)
          collectFrames(max_ampl_vs_chan, j);
      }
      for (auto& ch_max : max_ampl_vs_chan)
        ch_max.second /= num_events;
      sc_.setReadoutEnable(false);
      return max_ampl_vs_chan;
    }

    void ApvAdcCalibration::collectFrames(chanvalues_t& ave, size_t fec_id) {
      auto frames = sc_.read(fec_id, running_);
      for (const auto& frm : frames) {
        auto data = frm.next<srs::AdcData>();
        size_t data_size = 0;
        float max_ampl = 0.;
        while (true) {
          try {
            const auto frame = data.next();
            auto vals = frame.data;
            // use the first N values of this time bin to compute baseline
            const float baseline =
                (float)std::accumulate(frame.data.begin(), frame.data.begin() + num_samples_baseline_, 0.) /
                (float)num_samples_baseline_;
            for (const auto& val : vals) {
              const float val_bl = (float)val - baseline;
              if (val_bl > max_ampl)
                max_ampl = val_bl;
            }
            data_size++;
          } catch (const srs::SrsFrame::NoMoreFramesException&) {
            break;
          }
        }
        auto ch_id = frm.daqChannel();  //srs::AdcData::physChannel(frame.address);
        ave[ch_id] += max_ampl;

        SRS_DEBUG("ApvAdcCalibration:collectFrames") << data_size << " frames in window";
      }
    }
  }  // namespace utils
}  // namespace srs
