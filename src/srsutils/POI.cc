/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srsutils/Logging.h"
#include "srsutils/POI.h"

namespace srs {
  template <size_t N>
  void POI<N>::print(std::ostream& os) const {
    os << "{";
    std::string sep;
    for (size_t i = 0; i < size(); ++i)
      os << sep << std::dec << i << ": "
         << "0x" << std::hex << enabled(i) << std::dec,
          sep = ", ";
    os << "}";
  }
}  // namespace srs
