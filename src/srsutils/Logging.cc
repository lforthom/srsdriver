/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  Logger::Logger() : level_(Level::information), output_(&std::cout), pretty_(true) {}

  Logger& Logger::get() {
    static Logger log;
    return log;
  }

  void Logger::addLoggingRule(const std::string& rule) {
    try {
      allowed_exc_.emplace_back(rule, std::regex_constants::extended);
    } catch (const std::regex_error& err) {
      throw std::runtime_error("Failed to build regex for logging tool.\n" + std::string(err.what()));
    }
  }

  bool Logger::passLoggingRule(const std::string& tmpl, const Level& lev) const {
    if (level_ >= lev)
      return true;
    if (allowed_exc_.empty())
      return false;
    for (const auto& rule : allowed_exc_)
      try {
        if (std::regex_match(tmpl, rule))
          return true;
      } catch (const std::regex_error& err) {
        throw std::runtime_error("Failed to evaluate regex for logging tool.\n" + std::string(err.what()));
      }
    return false;
  }

  std::ostream& Logger::operator<<(const std::string& msg) {
    if (!output_)
      return std::cerr << "No output stream defined for logger!" << std::endl;
    return (*output_) << msg;
  }

  Logger& Logger::setLevel(const Level& lev) {
    if (output_)
      (*output_) << "Logging level set to " << lev << std::endl;

    level_ = lev;
    return *this;
  }

  std::ostream& operator<<(std::ostream& os, const Logger::Level& lvl) {
    switch (lvl) {
      case Logger::Level::nothing:
        return os << "None";
      case Logger::Level::error:
        return os << "Errors";
      case Logger::Level::warning:
        return os << "Warnings";
      case Logger::Level::information:
        return os << "Infos";
      case Logger::Level::debug:
        return os << "Debug";
    }
    return os;
  }

  //-----------------------------------------------------------------------------------------------

  LoggedMessage::LoggedMessage(const char* module, Type type, short id)
      : module_(module), type_(type), error_num_(id) {}

  LoggedMessage::LoggedMessage(const char* from, const char* module, Type type, short id)
      : from_(from), module_(module), type_(type), error_num_(id) {}

  LoggedMessage::LoggedMessage(const LoggedMessage& oth) noexcept
      : from_(oth.from_),
        module_(oth.module_),
        message_(oth.message_.str()),
        type_(oth.type_),
        error_num_(oth.error_num_) {}

  LoggedMessage::~LoggedMessage() noexcept { LoggedMessage::print(); }

  std::ostream& LoggedMessage::print(std::ostream* os) const {
    if (!os)
      os = Logger::get().outputPtr();
    switch (type_) {
      case Type::info:
        (*os) << utils::colourise("Info:", utils::Colour::green, utils::Modifier::bold) << "\t" << message_.str()
              << "\n";
        break;
      case Type::debug:
        (*os) << utils::colourise("Debug:", utils::Colour::yellow, utils::Modifier::reverse) << " "
              << utils::colourise(from_, utils::Colour::reset, utils::Modifier::underline) << "\n\t" << message_.str()
              << "\n";
        break;
      case Type::warning:
        (*os) << utils::colourise("Warning:", utils::Colour::blue, utils::Modifier::bold) << " "
              << utils::colourise(from_, utils::Colour::reset, utils::Modifier::underline) << "\n\t" << message_.str()
              << "\n";
        break;
      case Type::verbatim:
        (*os) << message_.str() << "\n";
        break;
      case Type::undefined:
      case Type::error: {
        const std::string sep(80, '-');
        if (Logger::get().pretty())
          (*os) << sep << "\n";
        if (type_ == Type::error)
          (*os) << utils::colourise("Error", utils::Colour::red, utils::Modifier::bold);
        else if (type_ == Type::undefined)
          (*os) << utils::colourise("Undefined exception", utils::Colour::reset, utils::Modifier::reverse);
        (*os) << " occurred";
        if (Logger::get().pretty())
          (*os) << " at " << utils::now() << "\n  ";
        else
          (*os) << " and ";
        if (!from_.empty())
          (*os) << "raised by: " << utils::colourise(from_, utils::Colour::reset, utils::Modifier::underline);
        if (errorNumber() != 0)
          (*os) << " (error #" << error_num_ << ")";
        if (Logger::get().pretty())
          (*os) << "\n\n";
        else
          (*os) << " → ";
        (*os) << message_.str() << "\n";
        if (Logger::get().pretty())
          (*os) << sep << "\n";
      } break;
    }
    return (*os) << std::flush;
  }
}  // namespace srs
