/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsdriver/SlowControlError.h"

namespace srs {
  void SlowControlError::print(std::ostream& os) const {
    if (!frameDecoderErrors() && !frameReceiverErrors()) {
      os << "I2C acknowledge: " << std::bitset<16>(word_) << "\n";
      return;
    }
    os << "SlowControlError(" << std::bitset<16>(word_) << ")";
    if (frameDecoderErrors())
      os << "\n--- Frame receiver errors:";
    if (checksumError())
      os << "\nChecksum error";
    if (illFormedCommand())
      os << "\nIll-formed command";
    if (unrecognisedCommand())
      os << "\nCommand unrecognised";
    if (frameReceiverErrors())
      os << "\n--- Frame receiver errors:";
    if (replyIdError())
      os << "\nReply Id error";
    if (illegalLength())
      os << "\nIllegal length (< 4 words)";
    if (incompleteWord())
      os << "\nIllegal length (incomplete 32-bit word)";
    if (bufferFull())
      os << "\nBuffer full";
    if (illegalSourcePort())
      os << "\nIllegal source port";
    if (destinationPortUnavailable())
      os << "\nDestination port unavailable";
    os << "\n";
  }
}  // namespace srs
