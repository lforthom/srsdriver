/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bitset>
#include <iostream>

#include "srsdriver/ApvAddress.h"

namespace srs {
  ApvAddress::ApvAddress(uint32_t word) : POI(0, {11, 10, 9, 8, 15, 14, 13, 12}) { operator uint32_t&() = word; }

  ApvAddress::ApvAddress(uint8_t poi, Mode mode) : POI(poi, {11, 10, 9, 8, 15, 14, 13, 12}) { set(0, mode, 2); }

  ApvAddress ApvAddress::all(Mode mode) {
    ApvAddress addr(0, mode);
    for (size_t i = 0; i < 8; ++i)
      addr.setEnabled(i, 0x1);
    return addr;
  }

  ApvAddress ApvAddress::channel(size_t ch_id, Mode mode) {
    ApvAddress addr(0, mode);
    addr.setEnabled(ch_id, 0x1);
    return addr;
  }

  ApvAddress::Mode ApvAddress::mode() const { return static_cast<Mode>(uint32_t(*this) & 0x3); }

  void ApvAddress::print(std::ostream& os) const {
    os << "APV address [mode=" << mode() << "]: " << std::bitset<16>(*this) << "\n";
    for (size_t i = 0; i < pattern_.size(); ++i)
      os << "* HDMI channel " << i << " enabled: " << std::boolalpha << (bool)enabled(i) << "\n";
  }
}  // namespace srs
