/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bitset>
#include <iomanip>
#include <iostream>
#include <unordered_map>

#include "srsdriver/AdcCardRegister.h"

namespace srs {
  AdcCardRegister::AdcCardRegister() {
    // replace generic words objects with custom words objects
    for (const auto& fname : fieldNames)
      (*this)[fname.first].reset(new ChannelPattern(*operator[](fname.first)));
  }

  AdcCardRegister::AdcCardRegister(const words_t& words) : Config(words) {
    // replace generic words objects with custom words objects
    for (const auto& fname : fieldNames)
      (*this)[fname.first].reset(new ChannelPattern(*operator[](fname.first)));
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::hybridResetPin() {
    return *dynamic_cast<ChannelPattern*>(operator[](HYBRID_RST_N).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::hybridResetPin() const {
    return ChannelPattern(value(HYBRID_RST_N) & 0xff);
  }

  AdcCardRegister& AdcCardRegister::setHybridResetPin(const AdcCardRegister::ChannelPattern& pattern) {
    hybridResetPin() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::powerDownCh0() {
    return *dynamic_cast<ChannelPattern*>(operator[](PWRDOWN_CH0).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::powerDownCh0() const {
    return ChannelPattern(value(PWRDOWN_CH0) & 0xff);
  }

  AdcCardRegister& AdcCardRegister::setPowerDownCh0(const AdcCardRegister::ChannelPattern& pattern) {
    powerDownCh0() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::powerDownCh1() {
    return *dynamic_cast<ChannelPattern*>(operator[](PWRDOWN_CH1).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::powerDownCh1() const {
    return ChannelPattern(value(PWRDOWN_CH1) & 0xff);
  }

  AdcCardRegister& AdcCardRegister::setPowerDownCh1(const AdcCardRegister::ChannelPattern& pattern) {
    powerDownCh1() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::eqLevel0() {
    return *dynamic_cast<ChannelPattern*>(operator[](EQ_LEVEL_0).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::eqLevel0() const { return ChannelPattern(value(EQ_LEVEL_0) & 0xff); }

  AdcCardRegister& AdcCardRegister::setEqLevel0(const AdcCardRegister::ChannelPattern& pattern) {
    eqLevel0() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::eqLevel1() {
    return *dynamic_cast<ChannelPattern*>(operator[](EQ_LEVEL_1).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::eqLevel1() const { return ChannelPattern(value(EQ_LEVEL_1) & 0xff); }

  AdcCardRegister& AdcCardRegister::setEqLevel1(const AdcCardRegister::ChannelPattern& pattern) {
    eqLevel1() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::triggerOut() {
    return *dynamic_cast<ChannelPattern*>(operator[](TRGOUT_ENABLE).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::triggerOut() const {
    return ChannelPattern(value(TRGOUT_ENABLE) & 0xff);
  }

  AdcCardRegister& AdcCardRegister::setTriggerOut(const AdcCardRegister::ChannelPattern& pattern) {
    triggerOut() = pattern;
    return *this;
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::bclkBuffer() {
    return *dynamic_cast<ChannelPattern*>(operator[](BCLK_ENABLE).get());
  }

  AdcCardRegister::ChannelPattern AdcCardRegister::bclkBuffer() const {
    return ChannelPattern(value(BCLK_ENABLE) & 0xff);
  }

  AdcCardRegister& AdcCardRegister::setBclkBuffer(const AdcCardRegister::ChannelPattern& pattern) {
    bclkBuffer() = pattern;
    return *this;
  }

  void AdcCardRegister::print(std::ostream& os) const {
    os << "================== ADC card register ====================\n"
       << "Hybrid reset pin: " << std::bitset<8>(hybridResetPin()) << "\n"
       << "Power down of analog circuitry (master path): " << std::bitset<8>(powerDownCh0()) << "\n"
       << "Power down of analog circuitry  (slave path): " << std::bitset<8>(powerDownCh1()) << "\n"
       << "Equalisation control (bit 0): " << std::bitset<8>(eqLevel0()) << "\n"
       << "Equalisation control (bit 1): " << std::bitset<8>(eqLevel1()) << "\n"
       << "TRGOUT buffer: " << std::bitset<8>(triggerOut()) << "\n"
       << "  BCLK buffer: " << std::bitset<8>(bclkBuffer()) << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  const fnames_t AdcCardRegister::fieldNames = {{HYBRID_RST_N, "HYBRID_RST_N"},
                                                {PWRDOWN_CH0, "PWRDOWN_CH0"},
                                                {PWRDOWN_CH1, "PWRDOWN_CH1"},
                                                {EQ_LEVEL_0, "EQ_LEVEL_0"},
                                                {EQ_LEVEL_1, "EQ_LEVEL_1"},
                                                {TRGOUT_ENABLE, "TRGOUT_ENABLE"},
                                                {BCLK_ENABLE, "BCLK_ENABLE"}};

  std::ostream& operator<<(std::ostream& os, const AdcCardRegister::Field& field) {
    return os << AdcCardRegister::fieldNames.at(field);
  }

  //-----------------------------------------------------------------------------------------------

  AdcCardRegister::ChannelPattern::ChannelPattern(uint32_t word) : POI(word, {3, 2, 1, 0, 7, 6, 5, 4}) {}

  AdcCardRegister::ChannelPattern::ChannelPattern(const AdcCardRegister::ChannelPattern& oth) : POI(0, oth.pattern_) {
    for (size_t i = 0; i < 8; ++i)
      setEnabled(i, oth.enabled(i));
  }

  AdcCardRegister::ChannelPattern& AdcCardRegister::ChannelPattern::operator=(
      const AdcCardRegister::ChannelPattern& oth) {
    pattern_ = oth.pattern_;
    for (size_t i = 0; i < 8; ++i)
      setEnabled(i, oth.enabled(i));
    return *this;
  }

  void AdcCardRegister::ChannelPattern::print(std::ostream& os) const {
    os << "Word: " << std::bitset<16>(*this) << "\n";
    for (size_t i = 0; i < pattern_.size(); ++i)
      os << "* HDMI channel " << i << " enabled: " << std::boolalpha << (bool)enabled(i) << "\n";
  }
}  // namespace srs
