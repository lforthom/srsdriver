/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>

#include "srsdriver/SystemRegister.h"
#include "srsutils/Logging.h"

namespace srs {
  SystemRegister::SystemRegister() {
    // replace generic words objects with custom words objects
    (*this)[MCLK_SEL].reset(new ClockSel(*operator[](MCLK_SEL)));
    (*this)[DTCC_CTRL].reset(new DtcConfig(*operator[](DTCC_CTRL)));
  }

  SystemRegister::SystemRegister(const words_t& words) : Config(words) {
    // replace generic words objects with custom words objects
    (*this)[MCLK_SEL].reset(new ClockSel(*operator[](MCLK_SEL)));
    (*this)[DTCC_CTRL].reset(new DtcConfig(*operator[](DTCC_CTRL)));
  }

  uint16_t SystemRegister::fwVersion() const { return value(VERSION) & 0xffff; }

  std::string SystemRegister::vendorId() const {
    std::ostringstream oss;
    const auto& id = value(FPGAMAC_VENDORID);
    static const uint8_t mask = 0xff;
    oss << std::hex << ((id >> 16) & mask) << ":" << ((id >> 8) & mask) << ":" << (id & mask) << std::dec;
    return oss.str();
  }

  std::string SystemRegister::macId() const {
    std::ostringstream oss;
    const auto& id = value(FPGAMAC_VENDORID);
    static const uint8_t mask = 0xff;
    oss << std::hex << ((id >> 16) & mask) << ":" << ((id >> 8) & mask) << ":" << (id & mask) << std::dec;
    return oss.str();
  }

  std::string SystemRegister::fecIP() const {
    std::ostringstream oss;
    const auto& ip = value(FPGA_IP);
    static const uint8_t mask = 0xff;
    oss << ((ip >> 24) & mask) << "." << ((ip >> 16) & mask) << "." << ((ip >> 8) & mask) << "." << (ip & mask);
    return oss.str();
  }

  SystemRegister& SystemRegister::setFecIP(const std::string& ip) {
    std::stringstream ss(ip);
    std::vector<int> ip_subaddr;
    std::string substr;
    while (ss.good()) {
      getline(ss, substr, '.');
      ip_subaddr.emplace_back(std::stoi(substr));
    }
    if (ip_subaddr.size() != 4)
      throw SRS_ERROR("SystemRegister:setFecIP") << "Failed to parse FEC IP address (" << ip << ")!";
    static const uint8_t mask = 0xff;
    setValue(FPGA_IP,
             ((ip_subaddr.at(0) & mask) << 24) | ((ip_subaddr.at(1) & mask) << 16) | ((ip_subaddr.at(2) & mask) << 8) |
                 (ip_subaddr.at(3) & mask));
    return *this;
  }

  std::string SystemRegister::daqIP() const {
    std::ostringstream oss;
    const auto& ip = value(DAQ_IP);
    static const uint8_t mask = 0xff;
    oss << ((ip >> 24) & mask) << "." << ((ip >> 16) & mask) << "." << ((ip >> 8) & mask) << "." << (ip & mask);
    return oss.str();
  }

  SystemRegister& SystemRegister::setDaqIP(const std::string& ip) {
    std::stringstream ss(ip);
    std::vector<int> ip_subaddr;
    std::string substr;
    while (ss.good()) {
      getline(ss, substr, '.');
      ip_subaddr.emplace_back(std::stoi(substr));
    }
    if (ip_subaddr.size() != 4)
      throw SRS_ERROR("SystemRegister:setDaqIP") << "Failed to parse DAQ IP address (" << ip << ")!";
    static const uint8_t mask = 0xff;
    setValue(DAQ_IP,
             ((ip_subaddr.at(0) & mask) << 24) | ((ip_subaddr.at(1) & mask) << 16) | ((ip_subaddr.at(2) & mask) << 8) |
                 (ip_subaddr.at(3) & mask));
    return *this;
  }

  uint16_t SystemRegister::daqPort() const { return value(DAQPORT) & 0xffff; }

  uint16_t SystemRegister::scPort() const { return value(SCPORT) & 0xffff; }

  SystemRegister::ClockSource SystemRegister::clockSource() const {
    if (clockSelection().ethernetClockSel()) {
      if (clockStatus().dtc1ClkLocked())
        return ETH;
      return LOCAL;
    }
    if (clockSelection().dtcClockInhibit())
      return LOCAL;
    if (clockStatus().dtc0ClkLocked())
      return DTC;
    return LOCAL;
  }

  SystemRegister::DtcConfig SystemRegister::dtcConfig() const { return DtcConfig(value(DTCC_CTRL)); }

  SystemRegister::DtcConfig& SystemRegister::dtcConfig() {
    return *dynamic_cast<DtcConfig*>(operator[](DTCC_CTRL).get());
  }

  SystemRegister& SystemRegister::setDtcConfig(const DtcConfig& dc) {
    setValue(DTCC_CTRL, (uint32_t)dc);
    return *this;
  }

  SystemRegister::ClockSel SystemRegister::clockSelection() const { return ClockSel(value(MCLK_SEL)); }

  SystemRegister::ClockSel& SystemRegister::clockSelection() {
    return *dynamic_cast<ClockSel*>(operator[](MCLK_SEL).get());
  }

  SystemRegister& SystemRegister::setClockSelection(const ClockSel& cs) {
    setValue(MCLK_SEL, (uint32_t)cs);
    return *this;
  }

  SystemRegister::ClockStatus SystemRegister::clockStatus() const { return ClockStatus(value(MCLK_STATUS)); }

  uint16_t SystemRegister::hwVersion() const { return value(VERSION_HW) & 0xffff; }

  void SystemRegister::print(std::ostream& os) const {
    os << "==================== System register ====================\n"
       << "Versions: FW: 0x" << std::hex << fwVersion() << ", HW: " << hwVersion() << std::dec << "\n"
       << "Vendor id: " << vendorId() << "\n"
       << "FEC: IP: " << fecIP() << ", MAC: " << macId() << "\n"
       << "DAQ IP: " << daqIP() << ":" << daqPort() << "\n"
       << "Clock source: " << clockSource() << "\n";
    os << "----- DTC configuration:\n";
    dtcConfig().print(os);
    os << "\n----- Clock selection:\n";
    clockSelection().print(os);
    os << "\n----- Clock status:\n";
    clockStatus().print(os);
    os << std::endl;
  }

  //-----------------------------------------------------------------------------------------------

  const fnames_t SystemRegister::fieldNames = {{VERSION, "VERSION"},
                                               {FPGAMAC_VENDORID, "FPGAMAC_VENDORID"},
                                               {FPGAMAC_ID, "FPGAMAC_ID"},
                                               {FPGA_IP, "FPGA_IP"},
                                               {DAQPORT, "DAQPORT"},
                                               {SCPORT, "SCPORT"},
                                               {FRAMEDLY, "FRAMEDLY"},
                                               {TOTFRAMES, "TOTFRAMES"},
                                               {ETHMODE, "ETHMODE"},
                                               {SCMODE, "SCMODE"},
                                               {DAQ_IP, "DAQ_IP"},
                                               {DTCC_CTRL, "DTCC_CTRL"},
                                               {MCLK_SEL, "MCLK_SEL"},
                                               {MCLK_STATUS, "MCLK_STATUS"},
                                               {VERSION_HW, "VERSION_HW"},
                                               {SYS_RSTREG, "SYS_RSTREG"}};

  std::ostream& operator<<(std::ostream& os, const SystemRegister::Field& field) {
    if (SystemRegister::fieldNames.count(field) == 0)
      return os << "[invalid]";
    return os << SystemRegister::fieldNames.at(field);
  }

  //-----------------------------------------------------------------------------------------------

  bool SystemRegister::ClockSel::dtcClockInhibit() const { return (word_ >> 0) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setDtcClockInhibit(bool inh) {
    set(0, inh, 1);
    return *this;
  }

  bool SystemRegister::ClockSel::dtcTriggerInhibit() const { return (word_ >> 1) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setDtcTriggerInhibit(bool inh) {
    set(1, inh, 1);
    return *this;
  }

  bool SystemRegister::ClockSel::dtcSwapPorts() const { return (word_ >> 2) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setDtcSwapPorts(bool swp) {
    set(2, swp, 1);
    return *this;
  }

  bool SystemRegister::ClockSel::dtcSwapLanes() const { return (word_ >> 3) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setDtcSwapLanes(bool swp) {
    set(3, swp, 1);
    return *this;
  }

  bool SystemRegister::ClockSel::dtcTriggerPolInvert() const { return (word_ >> 4) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setDtcTriggerPolInvert(bool inv) {
    set(4, inv, 1);
    return *this;
  }

  bool SystemRegister::ClockSel::ethernetClockSel() const { return (word_ >> 7) & 0x1; }

  SystemRegister::ClockSel& SystemRegister::ClockSel::setEthernetClockSel(bool sel) {
    set(7, sel, 1);
    return *this;
  }

  void SystemRegister::ClockSel::print(std::ostream& os) const {
    os << "DTC clock inhibit: " << std::boolalpha << dtcClockInhibit() << "\n"
       << "DTC trigger inhibit: " << std::boolalpha << dtcTriggerInhibit() << "\n"
       << "DTC swap ports: " << std::boolalpha << dtcSwapPorts() << "\n"
       << "DTC swap lanes: " << std::boolalpha << dtcSwapLanes() << "\n"
       << "DTC trigger polarity invert: " << std::boolalpha << dtcTriggerPolInvert() << "\n"
       << "ETH clock: " << std::boolalpha << ethernetClockSel() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  bool SystemRegister::ClockStatus::dtc0ClkLocked() const { return (word_ >> 0) & 0x1; }

  bool SystemRegister::ClockStatus::dtc1ClkLocked() const { return (word_ >> 1) & 0x1; }

  SystemRegister::ClockStatus::ClockSelection SystemRegister::ClockStatus::clockSelection() const {
    return ClockSelection((word_ >> 4) & 0x3);
  }

  double SystemRegister::ClockStatus::clockFrequency() const { return ((word_ >> 16) & 0xffff) * 1.e-2; }

  void SystemRegister::ClockStatus::print(std::ostream& os) const {
    os << "DTC0 clock locked: " << std::boolalpha << dtc0ClkLocked() << "\n"
       << "DTC1 clock locked: " << std::boolalpha << dtc1ClkLocked() << "\n"
       << "Clock selection: " << clockSelection() << "\n"
       << "Clock frequency: " << clockFrequency() << " ppm\n";
  }

  std::ostream& operator<<(std::ostream& os, const SystemRegister::ClockSource& src) {
    switch (src) {
      case SystemRegister::UNKNOWN:
      default:
        return os << "unknown";
      case SystemRegister::LOCAL:
        return os << "local osc.";
      case SystemRegister::DTC:
        return os << "DTC";
      case SystemRegister::ETH:
        return os << "ethernet RX";
    }
  }

  std::ostream& operator<<(std::ostream& os, const SystemRegister::ClockStatus::ClockSelection& sel) {
    switch (sel) {
      case SystemRegister::ClockStatus::LOCAL:
        return os << "local";
      case SystemRegister::ClockStatus::DTC:
        return os << "DTC";
      case SystemRegister::ClockStatus::ETH_RX:
        return os << "ETH";
      case SystemRegister::ClockStatus::INVALID:
      default:
        return os << "invalid";
    }
  }

  //-----------------------------------------------------------------------------------------------

  bool SystemRegister::DtcConfig::dataOverEth() const { return word_ & 0x1; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setDataOverEth(bool doe) {
    set(0, doe, 1);
    return *this;
  }

  bool SystemRegister::DtcConfig::flowControl() const { return !((word_ >> 1) & 0x1); }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setFlowControl(bool cnt) {
    set(1, !cnt, 1);
    return *this;
  }

  SystemRegister::DtcConfig::PaddingType SystemRegister::DtcConfig::paddingType() const {
    return static_cast<PaddingType>((word_ >> 8) & 0x3);
  }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setPaddingType(SystemRegister::DtcConfig::PaddingType type) {
    set(8, type, 2);
    return *this;
  }

  bool SystemRegister::DtcConfig::dataChannelTrgId() const { return (word_ >> 10) & 0x1; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setDataChannelTrgId(bool ti) {
    set(10, ti, 1);
    return *this;
  }

  bool SystemRegister::DtcConfig::dataFrameTrgId() const { return (word_ >> 11) & 0x1; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setDataFrameTrgId(bool ti) {
    set(11, ti, 1);
    return *this;
  }

  uint8_t SystemRegister::DtcConfig::trailerWordCount() const { return (word_ >> 12) & 0xf; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setTrailerWordCount(uint8_t cnt) {
    set(12, cnt, 4);
    return *this;
  }

  uint8_t SystemRegister::DtcConfig::paddingByte() const { return (word_ >> 16) & 0xff; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setPaddingByte(uint8_t bt) {
    set(16, bt, 8);
    return *this;
  }

  uint8_t SystemRegister::DtcConfig::trailerByte() const { return (word_ >> 24) & 0xff; }

  SystemRegister::DtcConfig& SystemRegister::DtcConfig::setTrailerByte(uint8_t bt) {
    set(24, bt, 8);
    return *this;
  }

  void SystemRegister::DtcConfig::print(std::ostream& os) const {
    os << "Data over ETH: " << std::boolalpha << dataOverEth() << "\n"
       << "Flow control: " << std::boolalpha << flowControl() << "\n"
       << "Padding type: " << (uint16_t)paddingType() << "\n"
       << "Data channel trigger Id: " << std::boolalpha << dataChannelTrgId() << "\n"
       << "Data frame trigger Id: " << std::boolalpha << dataFrameTrgId() << "\n"
       << "Trailer word count: " << (uint16_t)trailerWordCount() << "\n"
       << "Padding byte: 0x" << std::hex << (uint16_t)paddingByte() << std::dec << "\n"
       << "Trailer byte: 0x" << std::hex << (uint16_t)trailerByte() << std::dec << "\n";
  }

  std::ostream& operator<<(std::ostream& os, const SystemRegister::DtcConfig::PaddingType& type) {
    switch (type) {
      case SystemRegister::DtcConfig::NONE:
      default:
        return os << "none";
      case SystemRegister::DtcConfig::P_16BIT:
        return os << "16-bit";
      case SystemRegister::DtcConfig::P_32BIT:
        return os << "32-bit";
      case SystemRegister::DtcConfig::P_64BIT:
        return os << "64-bit";
    }
  }
}  // namespace srs
