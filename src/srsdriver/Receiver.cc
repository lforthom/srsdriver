/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <arpa/inet.h>
#include <byteswap.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <cstring>

#include "srsdriver/Receiver.h"
#include "srsutils/Logging.h"

#define RDTSC_KERNEL_CALLS 1

#ifdef RDTSC_KERNEL_CALLS
/// CPU reference cycles counter
static uint64_t rdtsc() {
  unsigned a{0}, d{0};
  __asm__ volatile("rdtsc" : "=a"(a), "=d"(d));
  return ((uint64_t)a) | (((uint64_t)d) << 32);
}
#endif

namespace srs {
  Receiver::Receiver(port_t port_in, bool invert)
      : fd_(socket(AF_INET, SOCK_DGRAM /*UDP*/ | SOCK_NONBLOCK, 0 /*IP*/)), invert_words_(invert) {
    if (fd_ < 0)
      throw SRS_ERROR("Receiver") << "Failed to create socket!";

    uint64_t recv_buf_size = 20 * 2014 * 2014;  // in bytes
    if (setsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &recv_buf_size, sizeof(recv_buf_size)) != 0)
      throw SRS_ERROR("Receiver") << "Failed to set buffer size!";
    int enable = 1;
    if (setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) != 0)
      throw SRS_ERROR("Messenger") << "Failed to set reusable addresses: " << strerror(errno) << " (" << errno << ").";
    /*ioctl(fd_, FIONBIO, &enable);
    if (fcntl(fd_, F_SETFL, fcntl(fd_, F_GETFL, 0) | O_NONBLOCK) == -1)*/
    struct timeval tv {};
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    if (setsockopt(fd_, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) != 0)
      throw SRS_ERROR("Receiver") << "Failed to set receive timeout: " << strerror(errno) << " (" << errno << ").";

    bzero(&cliaddr_, sizeof(cliaddr_));
    cliaddr_.sin_family = AF_INET;
    // specify the destination port
    cliaddr_.sin_port = htons((int)port_in);
    if (bind(fd_, reinterpret_cast<struct sockaddr*>(&cliaddr_), sizeof(cliaddr_)) == -1) {
      close(fd_);
      throw SRS_ERROR("Receiver") << "Failed to bind to server: " << strerror(errno) << " (" << errno << ").";
    }
    SRS_INFO("Receiver") << "Bound a new receiver to port " << port() << ".";
  }

  Receiver::~Receiver() {
    if (fd_ > 0)
      close(fd_);
  }

  words_t Receiver::retrieve(size_t max_words) const {
    if (empty())
      return {};

    words_t data(max_words);
    socklen_t len = sizeof(cliaddr_);

#ifdef RDTSC_KERNEL_CALLS
    uint64_t before = rdtsc();
#endif
    ssize_t ret = recvfrom(fd_,
                           static_cast<void*>(data.data()),
                           max_words * sizeof(uint32_t),
                           0,
                           reinterpret_cast<struct sockaddr*>(const_cast<sockaddr_in*>(&cliaddr_)),
                           &len);
#ifdef RDTSC_KERNEL_CALLS
    uint64_t after = rdtsc();
    total_ += (after - before);
#endif

    if (ret < 0) {
      if (errno == EAGAIN)
        throw SRS_ERROR("Receiver:retrieve") << "Timeout reached! Return: " << ret << ".";
      else
        SRS_WARNING("Receiver:retrieve") << "Error during readout! Return: " << ret << ":" << strerror(errno) << " ("
                                         << errno << ")";
      data.clear();
      return data;
    }

    data.resize(ret / sizeof(uint32_t));
    if (invert_words_)
      std::for_each(data.begin(), data.end(), [](auto& dt) { dt = bswap_32(dt); });
    SRS_DEBUG("Receiver:retrieve") << "Received packet from " << address() << ":" << port() << " of length " << ret
                                   << " / " << max_words << ".";

    return data;
  }

  bool Receiver::empty() const {
    int count{0};
    ioctl(fd_, FIONREAD, &count);
    return count == 0;
  }

  std::string Receiver::address() const { return inet_ntoa(cliaddr_.sin_addr); }

  port_t Receiver::port() const { return ntohs(cliaddr_.sin_port); }

  void Receiver::resetTotal() { total_ = 0ull; }
}  // namespace srs
