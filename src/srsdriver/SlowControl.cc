/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <byteswap.h>

#include <bitset>
#include <future>

#include "srsdriver/ApvAddress.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  SlowControl::SlowControl(const std::string& ip_addr) : sock_(new Messenger(ip_addr)) {
    const auto apvapp = readApvAppRegister();
    eb_mode_ = apvapp.ebMode();
    SRS_DEBUG("SlowControl").log([this, &apvapp](auto& log) {
      readSystemRegister().print(log);
      apvapp.print(log);
    });
  }

  SystemRegister SlowControl::readSystemRegister() {
    return SystemRegister(sock_->readBurst(P_SYS, SystemRegister().size()));
  }

  ApvRegister SlowControl::readApvRegister(const ApvAddress& poi) {
    return ApvRegister(sock_->readBurst(P_APV, ApvRegister().size(), (uint32_t)poi));
  }

  PllRegister SlowControl::readPllRegister(const ApvAddress& poi) {
    return PllRegister(sock_->readBurst(P_APV, PllRegister().size(), (uint32_t)poi));
  }

  ApvAppRegister SlowControl::readApvAppRegister() {
    return ApvAppRegister(sock_->readBurst(P_APVAPP, ApvAppRegister().size()));
  }

  AdcCardRegister SlowControl::readAdcCardRegister() {
    return AdcCardRegister(sock_->readBurst(P_ADCCARD, AdcCardRegister().size()));
  }

  void SlowControl::addFec(port_t port) { fecs_.emplace_back(new Receiver(port, true)); }

  void SlowControl::configureAdcs() {
    // ADC card registers
    wordspairs_t adc0 = {{AdcCardRegister::HYBRID_RST_N, 0xfff /*reset all APVs*/},
                         {AdcCardRegister::PWRDOWN_CH0, 0},
                         {AdcCardRegister::PWRDOWN_CH1, 0},
                         {AdcCardRegister::EQ_LEVEL_0, 0},
                         {AdcCardRegister::EQ_LEVEL_1, 0},
                         {AdcCardRegister::TRGOUT_ENABLE, 0},
                         {AdcCardRegister::BCLK_ENABLE, 0xff}};
    sock_->writePairs(P_ADCCARD, 0, adc0);
  }

  void SlowControl::setReadoutEnable(bool en) {
    for (size_t i = 0; i < sock_->maxReadoutAttempts(); ++i) {
      sock_->writePairs(P_APVAPP, 0, {{ApvAppRegister::RO_ENABLE, en}});
      if (readoutEnabled() == en)
        return;
      std::this_thread::sleep_for(std::chrono::milliseconds(500));  // add a bit of delay before any retrieval
    }
    throw SRS_ERROR("SlowControl:setReadoutEnable")
        << "Failed to set the readout mode to " << std::boolalpha << en << "!";
  }

  bool SlowControl::readoutEnabled() {
    const auto lst = sock_->readList(P_APVAPP, 0, {ApvAppRegister::RO_ENABLE});
    if (lst.size() != 1)
      throw SRS_ERROR("SlowControl:readoutEnabled") << "Failed to retrieve the readout word: " << lst.size() << ".";
    SRS_DEBUG("SlowControl:readoutEnabled") << "Readout enabled: " << std::boolalpha << (bool)lst.at(0);
    return lst.at(0);
  }

  void SlowControl::rebootFec() {
    sock_->writePairs(P_SYS, 0, {{SystemRegister::SYS_RSTREG, SystemRegister::REBOOT_FEC}});
  }

  void SlowControl::warmInit() {
    sock_->writePairs(P_SYS, 0, {{SystemRegister::SYS_RSTREG, SystemRegister::WARM_INIT}});
  }

  void SlowControl::setDaqIP(const std::string& daq_ipaddr, port_t port) {
    struct in_addr sa {};
    if (::inet_pton(AF_INET, daq_ipaddr.c_str(), &sa) != 1)
      throw SRS_ERROR("SlowControl:setDaqIP") << "pton failed in DAQ IP setting.";
    wordspairs_t req = {{SystemRegister::DAQPORT, port}, {SystemRegister::DAQ_IP, bswap_32(sa.s_addr)}};
    sock_->writePairs(P_SYS, 0, req);
  }

  SrsFrameCollection SlowControl::read(size_t fec_id, std::atomic_bool& running) {
    SrsFrameCollection frames;

    static const size_t num_words = 9000;
    while (running) {
      try {
        auto buf = fecs_[fec_id]->retrieve(num_words);
        if (buf.empty())
          continue;
        if (buf.size() == 1 && buf.at(0) == SrsData::GLOBAL_TRAILER)
          // event trailer received -> send as a trigger-indexed payload
          return frames;
        frames.emplace_back(buf, eb_mode_);
      } catch (const LogMessage&) {
        continue;
      }
    }
    /* SRSUNLOCK; */
    return frames;
  }

  void SlowControl::readout(std::vector<SrsFrameCollection>& out,
                            size_t fec_id,
                            std::atomic_bool& running,
                            bool unpack) {
    SrsFrameCollection frames;
    size_t num_frames = 0;
    static const size_t num_words = 9000;
    while (running) {
      auto buf = fecs_[fec_id]->retrieve(num_words);
      if (buf.empty())
        continue;
      if (unpack) {
        if (buf.size() == 1 && buf.at(0) == SrsData::GLOBAL_TRAILER) {
          // event trailer received -> send as a trigger-indexed payload
          out.emplace_back(frames);
          num_frames += frames.size();
          frames.clear();
          SRS_DEBUG("SlowControl:readout") << "so far, collected " << out.size() << " trigger(s) with "
                                           << utils::s("frame", num_frames, true) << " from FEC #" << fec_id << ".";
        } else
          frames.emplace_back(buf, eb_mode_);
      } else {
        SRS_INFO("SlowControl:readout") << "Retrieved " << buf.size() << " raw frames.";
        size_t i = 0;
        for (const auto& frm : buf)
          SRS_INFO("SlowControl:readout")
              << "Frame " << (i++) << ": " << std::hex << (frm & 0xffff) << " " << ((frm >> 16) & 0xffff);
      }
    }
    SRS_INFO("SlowControl:readout") << "collected " << utils::s("frame", num_frames, true) << " in "
                                    << utils::s("trigger", out.size(), true) << " -> mean frame(s)/trigger for FEC #"
                                    << fec_id << ": " << num_frames * 1. / out.size() << ".";
  }

  std::vector<SrsFrameCollection> SlowControl::readoutFor(std::chrono::milliseconds duration,
                                                          size_t fec_id,
                                                          bool unpack) {
    std::vector<srs::SrsFrameCollection> frames;
    std::atomic_bool running = {true};
    auto future = std::async(
        std::launch::async, &srs::SlowControl::readout, this, std::ref(frames), fec_id, std::ref(running), unpack);
    if (future.wait_for(duration) == std::future_status::timeout)
      running = false;
    return frames;
  }
}  // namespace srs
