/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <byteswap.h>
#include <unistd.h>

#include <cerrno>
#include <chrono>
#include <cmath>
#include <cstring>
#include <fstream>
#include <limits>
#include <thread>
#include <utility>

#include "srsdriver/Messenger.h"
#include "srsdriver/SlowControlError.h"
#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  Messenger::Messenger(std::string ipaddr)
      : max_num_attempts_readout_(10),
        num_debug_list_(25),
        delay_between_readouts_(20),  // in ms
        ip_addr_(std::move(ipaddr)) {
    //--- connection part
    if ((fd_ = socket(AF_INET, SOCK_DGRAM /*UDP*/, 0 /*IP*/)) < 0)
      throw SRS_ERROR("Messenger") << "Failed to create socket!";

    /*{ struct ifreq ifr; // check MTU value
      strcpy(ifr.ifr_name, "enp5s0");
      if (!::ioctl(fd_, SIOCGIFMTU, &ifr))
        SRS_INFO("Messenger") << "MTU " << ifr.ifr_mtu << " enabled.";
    }*/

    // set communication timeout
    struct timeval socket_timeout = {0, 500000};  // in us
    if (setsockopt(fd_, SOL_SOCKET, SO_RCVTIMEO, &socket_timeout, sizeof(socket_timeout)) != 0)
      throw SRS_ERROR("Messenger") << "Failed to set timeout: " << strerror(errno) << " (" << errno << ").";

    int enable = 1;
    if (setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) != 0)
      throw SRS_ERROR("Messenger") << "Failed to set reusable addresses: " << strerror(errno) << " (" << errno << ").";
#ifdef SO_REUSEPORT
    if (setsockopt(fd_, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(int)) != 0)
      throw SRS_ERROR("Messenger") << "Failed to set reusable ports: " << strerror(errno) << " (" << errno << ").";
#endif

    bzero(&servaddr_, sizeof(servaddr_));
    servaddr_.sin_family = AF_INET;
    //inet_aton((char*)ip_addr_.c_str(), reinterpret_cast<struct in_addr*>(&servaddr_.sin_addr.s_addr));
    // bind to the system register to set it as source port
    servaddr_.sin_port = htons((int)P_SYS);
    if (::bind(fd_, reinterpret_cast<struct sockaddr*>(&servaddr_), sizeof(servaddr_)) == -1) {
      close(fd_);
      throw SRS_ERROR("Messenger") << "Failed to bind to server: " << strerror(errno) << " (" << errno << ").";
    }
  }

  Messenger::~Messenger() {
    if (ai_)
      freeaddrinfo(ai_);
    // close the socket connection
    if (fd_ > 0)
      close(fd_);
  }

  words_t Messenger::parseCommands(const std::string& filename, std::string& ip, port_t& port, MessageData* data) {
    // first read the file into a strings buffer
    std::ifstream file(filename);
    if (!file.is_open())
      throw SRS_ERROR("Messenger:parseCommands") << "Failed to open file \"" << filename << "\"!";
    std::string buf;
    std::vector<std::string> content;
    while (std::getline(file, buf))
      if (buf[buf.find_first_not_of(' ')] != '#')  // skip comments
        content.emplace_back(buf);

    // then start casting into uint32_t words
    words_t out;
    ip = content[0];
    port = std::stoi(content[1]);
    for (size_t i = 2; i < content.size(); ++i)
      out.emplace_back(std::stoul(content[i], nullptr, 16));  // parse hexadecimal

    if (data) {  // if message data is to be populated
      data->request_id = out[REQUEST_ID];
      data->sub_address = out[SUB_ADDRESS];
      data->cmd_field1 = out[CMD_FIELD1];
      data->cmd_field2 = out[CMD_FIELD2];
      for (size_t i = CMD_FIELD2 + 1; i < out.size(); ++i)
        data->data_raw.emplace_back(out[i]);
      if (SlowControlCommand(out[CMD_FIELD1] >> 16) == WRITE_PAIRS)
        for (size_t i = CMD_FIELD2 + 1; i < out.size(); i += 2)
          data->data_pairs.emplace_back(std::make_pair(out[i], out[i + 1]));
    }

    // debugging information
    SRS_DEBUG("Messenger:parseCommands").log([&out, &ip, &port](auto& log) {
      log << ">>>> " << SlowControlCommand(out[CMD_FIELD1] >> 16) << " command\n"
          << " host: " << ip << ":" << port << "\n"
          << " sub.address: 0x" << utils::format("%08x", out[SUB_ADDRESS]) << "\n"
          << " commands:\n";
      for (size_t i = CMD_FIELD2 + 1; i < out.size(); ++i) {
        log << utils::format("    0x%08x", out[i]);
        if (SlowControlCommand(out[CMD_FIELD1] >> 16) == WRITE_PAIRS && i % 2 == 0)
          log << "\t->";
        else
          log << "\n";
      }
      /*log << " raw data:\n";
      for (const auto& w : out)
        log << utils::format("    0x%08x\n", w);*/
    });

    return out;
  }

  std::vector<std::string> Messenger::writeCommands(
      const std::string& ip, const port_t& port, int sub_addr, const wordspairs_t& data, bool print_banner) {
    std::vector<std::string> words;
    if (print_banner) {
      words.emplace_back(std::string(80, '#'));
      words.emplace_back("# This configuration has been automatically generated");
      words.emplace_back("# on " + utils::now() + " by srs::Messenger's " + std::string(__func__) + " member.");
      words.emplace_back(std::string(80, '#'));
    }
    words.emplace_back(ip);
    words.emplace_back(std::to_string(port));
    for (const auto& word : writePairsRequest(sub_addr, data))
      words.emplace_back(utils::format("%08x", word));
    return words;
  }

  words_t Messenger::inquire(port_t port, const words_t& req) {
    // send the command
    send(port, req);
    // then retrieve the data
    size_t max_len = NUM_PAYLOAD_FIELDS + 2 * (req.size() - NUM_PAYLOAD_FIELDS);
    try {
      return retrieve(max_len);
    } catch (const LogMessage& msg) {
      throw SRS_ERROR("Messenger:inquire") << "Failed to retrieve from port " << port << ".\n" << msg.message();
    }
  }

  bool Messenger::bind(port_t port) {
    servaddr_.sin_port = htons((int)port);  // specify the destination port

    {  // binding part
      struct addrinfo ai_hint;
      bzero(&ai_hint, sizeof(ai_hint));
      ai_hint.ai_flags = AI_CANONNAME;  // canonical name
      ai_hint.ai_family = AF_INET;
      ai_hint.ai_socktype = SOCK_DGRAM;
      if (getaddrinfo(const_cast<char*>(ip_addr_.c_str()), nullptr, &ai_hint, &ai_) != 0)
        throw SRS_ERROR("Messenger:bind")
            << "Failed to retrieve the address info: " << strerror(errno) << " (" << errno << ").";
    }
    if (ai_->ai_addr->sa_family != AF_INET)  // retrieve the hostname
      throw SRS_ERROR("Messenger:bind") << "Unknown AF_xxx: " << ai_->ai_addr->sa_family << ".";
    if (inet_ntop(
            AF_INET, &(reinterpret_cast<struct sockaddr_in*>(ai_->ai_addr))->sin_addr, host_.data(), host_.size()) ==
        nullptr)
      throw SRS_ERROR("Messenger:bind") << "Failed to retrieve host: " << strerror(errno) << " (" << errno << ").";
    if (inet_pton(AF_INET, (ai_->ai_canonname ? host_.data() : ai_->ai_canonname), &servaddr_.sin_addr) <= 0) {
      close(fd_);
      throw SRS_ERROR("Messenger:bind") << "Invalid network address ("
                                        << (ai_->ai_canonname ? host_.data() : ai_->ai_canonname)
                                        << "):" << strerror(errno) << " (" << errno << ").";
    }

    SRS_DEBUG("Messenger:bind") << "Bound to port " << port << " on server " << ip_addr_ << " "
                                << "(host: " << host_.data() << ").";

    return true;
  }

  bool Messenger::send(port_t port, const words_t& req) {
    // specify the destination port
    bind(port);

    //--- sending part
    words_t req_swapped;
    std::transform(
        req.begin(), req.end(), std::back_inserter(req_swapped), [](const auto& elem) { return bswap_32(elem); });
    if (req_swapped.empty())
      return false;
    int ret = sendto(fd_,
                     req_swapped.data(),
                     req_swapped.size() * sizeof(req_swapped.at(0)),
                     0,
                     reinterpret_cast<struct sockaddr*>(&servaddr_),
                     sizeof(servaddr_));
    if (ret == -1) {
      SRS_ERROR("Messenger:send") << "Failed to send data to the server at port " << port << ": " << strerror(errno)
                                  << " (" << errno << ").";
      return false;
    }
    SRS_DEBUG("Messenger:send").log([&](auto& log) {
      log << "Sent " << std::dec << ret / sizeof(uint32_t) << "/" << req_swapped.size()
          << " words to the server at port " << port << ".\n";
      for (size_t i = 0; i < req.size(); ++i) {
        if (i > num_debug_list_) {
          log << utils::format("[...]\n  %3lu: 0x%08x\n", req.size() - 1, req[req.size() - 1]);
          break;
        }
        log << utils::format("  %3lu: 0x%08x\n", i, req[i]);
      }
    });
    std::this_thread::sleep_for(
        std::chrono::milliseconds(delay_between_readouts_));  // add a bit of delay before any retrieval
    return true;
  }

  words_t Messenger::retrieve(size_t max_words) {
    words_t data(max_words);
    if (max_words == 0)
      return data;
    socklen_t len = sizeof(servaddr_);
    size_t num_attempts = 0;
    words_t words_parsed;
    while (num_attempts++ < max_num_attempts_readout_) {
      ssize_t ret = recvfrom(fd_,
                             static_cast<void*>(data.data()),
                             max_words * sizeof(data.at(0)),
                             0,
                             reinterpret_cast<struct sockaddr*>(&servaddr_),
                             &len);
      if (ret >= 0) {
        size_t num_ffs = false, num_zeros = 0;
        words_parsed = parse(data);
        for (const auto& word : words_parsed) {
          if (word == 0x0)
            num_zeros++;
          else if (word == std::numeric_limits<word_t>::max())
            num_ffs++;
        }
        if (words_parsed.size() > 1 && num_zeros == words_parsed.size()) {
          SRS_WARNING("Messenger:retrieve") << "Attempt " << num_attempts << ": retrieved " << num_zeros << "/"
                                            << words_parsed.size() << " zero-qualified words.";
          continue;
        }
        if (num_ffs == words_parsed.size()) {
          SRS_WARNING("Messenger:retrieve") << "Attempt " << num_attempts << ": retrieved " << num_ffs << "/"
                                            << words_parsed.size() << " 0xff..ff-qualified words.";
          continue;
        }
        SRS_DEBUG("Messenger:retrieve") << "After " << utils::s("attempt", num_attempts, true) << ", "
                                        << "received packet from " << std::dec << inet_ntoa(servaddr_.sin_addr) << ":"
                                        << ntohs(servaddr_.sin_port) << " of length " << ret << " / " << len << " / "
                                        << max_words;
        return words_parsed;
      }
      if (errno == EAGAIN)
        SRS_DEBUG("Messenger:retrieve") << "Timeout reached! Return: " << ret << ".";
      else if (errno == EBADF)
        throw SRS_ERROR("Messenger:retrieve") << "Communication ended. Aborting.";
      else
        SRS_WARNING("Messenger:retrieve")
            << "Error during readout! Return: " << ret << ", " << strerror(errno) << " (" << errno << ").";
      std::this_thread::sleep_for(
          std::chrono::milliseconds(delay_between_readouts_));  // add a bit of delay before any further retrieval
    }
    throw SRS_ERROR("Messenger:retrieve") << "Failed to retrieve a proper UDP packet after "
                                          << utils::s("attempt", max_num_attempts_readout_, true) << "!";
  }

  words_t Messenger::parse(const words_t& data) const {
    words_t out, data_swapped;
    for (const auto& ln : data)
      data_swapped.emplace_back(bswap_32(ln));
    size_t num_err = 0ull;
    // first check request identifier
    if (data_swapped[SUB_ADDRESS] != req_cnt_)
      SRS_DEBUG("Messenger:parse")  // should be a warning
          << "Request identifier, " << data_swapped[SUB_ADDRESS] << " != " << req_cnt_;
    if ((data_swapped[SUB_ADDRESS] >> 31) != 0x0)
      throw SRS_ERROR("Messenger:parse") << "Failed to parse the answer! Wrong request identifier!";
    // then strip the error words
    for (size_t i = NUM_PAYLOAD_FIELDS; i < data_swapped.size() - 1; i += 2) {
      if (data_swapped[i] != 0x0) {
        SlowControlError err(data_swapped[i]);
        if (!err.frameReceiverErrors() && !err.frameDecoderErrors())
          // must be a I2C acknowledgment word
          SRS_DEBUG("Messenger:parse").log([&i, &err](auto& log) {
            log << "word " << (i - NUM_PAYLOAD_FIELDS) << ": ";
            err.print(log);
          });
        else {
          SRS_WARNING("Messenger:parse").log([&i, &err](auto& log) {
            log << "Error in word " << (i - NUM_PAYLOAD_FIELDS) << ":";
            err.print(log);
          });
          ++num_err;
        }
      }
      out.emplace_back(data_swapped[i + 1]);
    }
    // display a warning if error(s) is (are) encountered
    if (num_err > 0)
      SRS_WARNING("Messenger:parse") << "Found " << utils::s("error", num_err, true) << " in reply.";
    SRS_DEBUG("Messenger:parse").log([this, &out](auto& log) {
      log << "Parsed " << out.size() << " words:";
      for (size_t i = 0; i < out.size(); ++i) {
        if (i > num_debug_list_) {
          log << utils::format("\n[...]\n  %3lu: 0x%08x", out.size() - 1, out[out.size() - 1]);
          break;
        }
        log << utils::format("\n  %3lu: 0x%08x", i, out[i]);
      }
    });
    return out;
  }

  words_t Messenger::newRequest(int sub_addr, SlowControlCommand cmd) {
    words_t req(NUM_PAYLOAD_FIELDS);
    //req[REQUEST_ID] = 0x80000000+(++req_cnt_);
    req[REQUEST_ID] = 0x80000000;
    req[SUB_ADDRESS] = sub_addr;
    req[CMD_FIELD1] = (cmd << 16) | 0xffff;
    return req;
  }

  words_t Messenger::writePairsRequest(int sub_addr, const wordspairs_t& pairs) {
    auto req = newRequest(sub_addr, WRITE_PAIRS);
    req[CMD_FIELD2] = 0x12345678;  // whatever
    for (const auto& pair : pairs) {
      req.emplace_back(pair.first);
      req.emplace_back(pair.second);
    }
    return req;
  }

  words_t Messenger::writeBurstRequest(int sub_addr, int first_addr, const words_t& data) {
    auto req = newRequest(sub_addr, WRITE_BURST);
    req[CMD_FIELD2] = first_addr;
    std::copy(data.begin(), data.end(), req.end());
    return req;
  }

  words_t Messenger::readBurstRequest(size_t num_words, int sub_addr, int first_addr) {
    auto req = newRequest(sub_addr, READ_BURST);
    req[CMD_FIELD2] = first_addr;
    for (size_t i = 0; i < num_words; ++i)
      req.emplace_back(first_addr + i + 1);  // dummy fields
    return req;
  }

  words_t Messenger::readListRequest(int sub_addr, const words_t& addr) {
    auto req = newRequest(sub_addr, READ_LIST);
    req[CMD_FIELD2] = 0x12345678;  // whatever
    for (const auto& ad : addr)
      req.emplace_back(ad);
    return req;
  }

  bool Messenger::writePairs(port_t port, int sub_addr, const wordspairs_t& pairs) {
    if (pairs.empty())
      throw SRS_ERROR("Messenger:writePairs") << "Empty request to port " << port << "/subaddress " << sub_addr << "!";
    // send the command
    bool out = send(port, writePairsRequest(sub_addr, pairs));
    std::this_thread::sleep_for(
        std::chrono::milliseconds(delay_between_readouts_));  // add a bit of delay before any retrieval
    return out;
  }

  bool Messenger::writeBurst(port_t port, int sub_addr, int first_addr, const words_t& data) {
    if (data.empty())
      throw SRS_ERROR("Messenger:writeBurst") << "Empty request to port " << port << "/subaddress " << sub_addr << "!";
    // send the command
    bool out = send(port, writeBurstRequest(sub_addr, first_addr, data));
    std::this_thread::sleep_for(
        std::chrono::milliseconds(delay_between_readouts_));  // add a bit of delay before any retrieval
    return out;
  }

  words_t Messenger::readBurst(port_t port, size_t num_words, int sub_addr, int first_addr) {
    return inquire(port, readBurstRequest(num_words, sub_addr, first_addr));
  }

  words_t Messenger::readList(port_t port, int sub_addr, const words_t& addr) {
    return inquire(port, readListRequest(sub_addr, addr));
  }
}  // namespace srs
