/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bitset>
#include <sstream>

#include "srsdriver/ApvAppRegister.h"
#include "srsutils/Logging.h"

namespace srs {
  ApvAppRegister::ApvAppRegister() {
    // replace generic words objects with custom words objects
    (*this)[BCLK_MODE].reset(new TriggerMode(*operator[](BCLK_MODE)));
    (*this)[EVBLD_CHENABLE].reset(new ChannelEnable(*operator[](EVBLD_CHENABLE)));
    (*this)[EVBLD_EVENTINFODATA].reset(new EbEventInfoData(*operator[](EVBLD_EVENTINFODATA)));
    (*this)[APZ_ZEROSUPP_THR].reset(new ApzZeroSuppThreshold(*operator[](APZ_ZEROSUPP_THR)));
    (*this)[APZ_ZEROSUPP_PRMS].reset(new ApzZeroSuppParameters(*operator[](APZ_ZEROSUPP_PRMS)));
  }

  ApvAppRegister::ApvAppRegister(const words_t& words) : Config(words) {
    // replace generic words objects with custom words objects
    (*this)[BCLK_MODE].reset(new TriggerMode(*operator[](BCLK_MODE)));
    (*this)[EVBLD_CHENABLE].reset(new ChannelEnable(*operator[](EVBLD_CHENABLE)));
    (*this)[EVBLD_EVENTINFODATA].reset(new EbEventInfoData(*operator[](EVBLD_EVENTINFODATA)));
    (*this)[APZ_ZEROSUPP_THR].reset(new ApzZeroSuppThreshold(*operator[](APZ_ZEROSUPP_THR)));
    (*this)[APZ_ZEROSUPP_PRMS].reset(new ApzZeroSuppParameters(*operator[](APZ_ZEROSUPP_PRMS)));
  }

  uint8_t ApvAppRegister::triggerBurst() const { return value(BCLK_TRGBURST) & 0xff; }

  ApvAppRegister& ApvAppRegister::setTriggerBurst(uint8_t bst) {
    (*this)[BCLK_TRGBURST]->set(0, bst, 8);
    return *this;
  }

  uint16_t ApvAppRegister::triggerSeqPeriod() const { return value(BCLK_FREQ) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setTriggerSeqPeriod(uint16_t per) {
    (*this)[BCLK_FREQ]->set(0, per, 16);
    return *this;
  }

  uint16_t ApvAppRegister::triggerDelay() const { return value(BCLK_TRGDELAY) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setTriggerDelay(uint16_t del) {
    (*this)[BCLK_TRGDELAY]->set(0, del, 16);
    return *this;
  }

  uint16_t ApvAppRegister::testPulseDelay() const { return value(BCLK_TPDELAY) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setTestPulseDelay(uint16_t del) {
    (*this)[BCLK_TPDELAY]->set(0, del, 16);
    return *this;
  }

  uint16_t ApvAppRegister::roSync() const { return value(BCLK_ROSYNC) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setRoSync(uint16_t snc) {
    (*this)[BCLK_ROSYNC]->set(0, snc, 16);
    return *this;
  }

  uint32_t ApvAppRegister::adcStatus() const { return value(ADC_STATUS); }

  ChannelEnable ApvAppRegister::ebChannelEnableMask() const { return ChannelEnable(value(EVBLD_CHENABLE)); }

  ChannelEnable& ApvAppRegister::ebChannelEnableMask() {
    return *dynamic_cast<ChannelEnable*>(operator[](EVBLD_CHENABLE).get());
  }

  ApvAppRegister& ApvAppRegister::setEbChannelEnableMask(const ChannelEnable& msk) {
    setValue(EVBLD_CHENABLE, msk);
    return *this;
  }

  uint16_t ApvAppRegister::ebCaptureWindow() const { return value(EVBLD_DATALENGTH) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setEbCaptureWindow(uint16_t win) {
    (*this)[EVBLD_DATALENGTH]->set(0, win, 16);
    return *this;
  }

  ApvAppRegister::EventBuilderMode ApvAppRegister::ebMode() const { return EventBuilderMode(value(EVBLD_MODE) & 0xff); }

  ApvAppRegister& ApvAppRegister::setEbMode(const ApvAppRegister::EventBuilderMode& mode) {
    (*this)[EVBLD_MODE]->set(0, mode, 8);
    return *this;
  }

  ApvAppRegister::EbEventInfoData ApvAppRegister::ebEventInfoData() const {
    return EbEventInfoData(value(EVBLD_EVENTINFODATA));
  }

  ApvAppRegister::EbEventInfoData& ApvAppRegister::ebEventInfoData() {
    return *dynamic_cast<EbEventInfoData*>(operator[](EVBLD_EVENTINFODATA).get());
  }

  uint16_t ApvAppRegister::apzSyncDet() const { return value(APZ_SYNC_DET) & 0xffff; }

  uint8_t ApvAppRegister::ebDataFormat() const { return value(EVBLD_EVENTINFOTYPE) & 0xff; }

  ApvAppRegister& ApvAppRegister::setEbDataFormat(uint8_t df) {
    (*this)[EVBLD_EVENTINFOTYPE]->set(0, df, 8);
    return *this;
  }

  ApvAppRegister& ApvAppRegister::setEbEventInfoData(const EbEventInfoData& ed) {
    setValue(EVBLD_EVENTINFODATA, ed);
    return *this;
  }

  bool ApvAppRegister::readoutEnabled() const { return value(RO_ENABLE) & 0x1; }

  ApvAppRegister& ApvAppRegister::setReadoutEnabled(bool ro) {
    (*this)[RO_ENABLE]->set(0, ro, 1);
    return *this;
  }

  ApvAppRegister::ApzStatus ApvAppRegister::apzStatus() const { return ApzStatus(value(APZ_STATUS)); }

  uint8_t ApvAppRegister::apzApvSelect() const { return value(APZ_APVSELECT) & 0xff; }

  ApvAppRegister& ApvAppRegister::setApzApvSelect(uint8_t sel) {
    (*this)[APZ_APVSELECT]->set(0, sel, 8);
    return *this;
  }

  uint8_t ApvAppRegister::apzNumSamples() const { return value(APZ_NSAMPLES) & 0xff; }

  ApvAppRegister& ApvAppRegister::setApzNumSamples(uint8_t ns) {
    (*this)[APZ_NSAMPLES]->set(0, ns, 8);
    return *this;
  }

  ApvAppRegister::ApzZeroSuppThreshold ApvAppRegister::apzZeroSuppThreshold() const {
    return ApzZeroSuppThreshold(value(APZ_ZEROSUPP_THR));
  }

  ApvAppRegister::ApzZeroSuppThreshold& ApvAppRegister::apzZeroSuppThreshold() {
    return *dynamic_cast<ApzZeroSuppThreshold*>(operator[](APZ_ZEROSUPP_THR).get());
  }

  ApvAppRegister& ApvAppRegister::setApzZeroSuppThreshold(const ApzZeroSuppThreshold& zst) {
    setValue(APZ_ZEROSUPP_THR, zst);
    return *this;
  }

  ApvAppRegister::ApzZeroSuppParameters ApvAppRegister::apzZeroSuppParameters() const {
    return ApzZeroSuppParameters(value(APZ_ZEROSUPP_PRMS) & 0xffff);
  }

  ApvAppRegister::ApzZeroSuppParameters& ApvAppRegister::apzZeroSuppParameters() {
    return *dynamic_cast<ApzZeroSuppParameters*>(operator[](APZ_ZEROSUPP_PRMS).get());
  }

  ApvAppRegister& ApvAppRegister::setApzZeroSuppParameters(const ApvAppRegister::ApzZeroSuppParameters& par) {
    (*this)[APZ_ZEROSUPP_PRMS]->set(0, par, 5);
    return *this;
  }

  uint16_t ApvAppRegister::apvSyncLowThreshold() const { return value(APV_SYNC_LOWTHR) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setApvSyncLowThreshold(uint16_t thr) {
    setValue(APV_SYNC_LOWTHR, ConfigWord(thr));
    return *this;
  }

  uint16_t ApvAppRegister::apvSyncHighThreshold() const { return value(APV_SYNC_HIGHTHR) & 0xffff; }

  ApvAppRegister& ApvAppRegister::setApvSyncHighThreshold(uint16_t thr) {
    setValue(APV_SYNC_HIGHTHR, ConfigWord(thr));
    return *this;
  }

  uint8_t ApvAppRegister::apzCommand() const { return value(APZ_CMD) & 0xff; }

  ApvAppRegister& ApvAppRegister::setApzCommand(uint8_t cmd) {
    setValue(APZ_CMD, ConfigWord(cmd));
    return *this;
  }

  void ApvAppRegister::print(std::ostream& os) const {
    os << "==================== APVapp register ====================\n"
       << "---------------------- APV trigger ----------------------\n"
       << "----- Trigger sequencer mode:\n";
    triggerMode().print(os);
    os << "\n"
       << "Trigger burst: " << (uint16_t)triggerBurst() << "\n"
       << "Trigger sequencer period: " << triggerSeqPeriod() << "\n"
       << "Trigger/APV trigger delay: " << triggerDelay() << "\n"
       << "Test pulse delay: " << testPulseDelay() << "\n"
       << "Readout sync delay: " << roSync() << "\n"
       << "ADC deserialisation status: 0x" << std::hex << adcStatus() << std::dec << "\n"
       << "--------------------- Event builder ---------------------\n"
       << "----- Channel-enable mask:\n";
    ebChannelEnableMask().print(os);
    os << "\n"
       << "Capture window length: " << ebCaptureWindow() << "\n"
       << "Mode: " << ebMode() << "\n"
       << "Data format: 0x" << std::hex << (uint16_t)ebDataFormat() << std::dec << "\n"
       << "----- Event info format:\n";
    ebEventInfoData().print(os);
    os << "\n"
       << "Readout enabled: " << std::boolalpha << readoutEnabled() << "\n"
       << "--------------------- APZ registers ---------------------\n"
       << "Front-end ASIC detected at ADC channels: " << std::bitset<16>(apzSyncDet()) << "\n";
    POI<2>(apzSyncDet(), {0, 2, 4, 6, 8, 10, 12, 14}).print(os);
    os << "\n"
       << "----- APZ processor status:\n";
    apzStatus().print(os);
    os << "\n"
       << "APV selected: " << (uint16_t)apzApvSelect() << "\n"
       << "Number of time-sample/trigger: " << (uint32_t)apzNumSamples() << "\n"
       << "----- Zero-suppression threshold: \n";
    apzZeroSuppThreshold().print(os);
    os << "\n"
       << "----- Zero-suppression parameters:\n";
    apzZeroSuppParameters().print(os);
    os << "\n"
       << "APV sync-pulse detection  low thres.: " << apvSyncLowThreshold() << "\n"
       << "APV sync-pulse detection high thres.: " << apvSyncHighThreshold() << "\n"
       << "APZ processor command register: " << (uint32_t)apzCommand() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  const fnames_t ApvAppRegister::fieldNames = {{BCLK_MODE, "BCLK_MODE"},
                                               {BCLK_TRGBURST, "BCLK_TRGBURST"},
                                               {BCLK_FREQ, "BCLK_FREQ"},
                                               {BCLK_TRGDELAY, "BCLK_TRGDELAY"},
                                               {BCLK_TPDELAY, "BCLK_TPDELAY"},
                                               {BCLK_ROSYNC, "BCLK_ROSYNC"},
                                               {ADC_STATUS, "ADC_STATUS"},
                                               {EVBLD_CHENABLE, "EVBLD_CHENABLE"},
                                               {EVBLD_DATALENGTH, "EVBLD_DATALENGTH"},
                                               {EVBLD_MODE, "EVBLD_MODE"},
                                               {EVBLD_EVENTINFOTYPE, "EVBLD_EVENTINFOTYPE"},
                                               {EVBLD_EVENTINFODATA, "EVBLD_EVENTINFODATA"},
                                               {RO_ENABLE, "RO_ENABLE"},
                                               {APZ_SYNC_DET, "APZ_SYNC_DET"},
                                               {APZ_STATUS, "APZ_STATUS"},
                                               {APZ_APVSELECT, "APZ_APVSELECT"},
                                               {APZ_NSAMPLES, "APZ_NSAMPLES"},
                                               {APZ_ZEROSUPP_THR, "APZ_ZEROSUPP_THR"},
                                               {APZ_ZEROSUPP_PRMS, "APZ_ZEROSUPP_PRMS"},
                                               {APV_SYNC_LOWTHR, "APV_SYNC_LOWTHR"},
                                               {APV_SYNC_HIGHTHR, "APV_SYNC_HIGHTHR"},
                                               {APZ_CMD, "APZ_CMD"}};

  std::ostream& operator<<(std::ostream& os, const ApvAppRegister::Field& field) {
    if (ApvAppRegister::fieldNames.count(field) == 0)
      return os << "[invalid]";
    return os << ApvAppRegister::fieldNames.at(field);
  }

  //-----------------------------------------------------------------------------------------------

  ApvAppRegister::TriggerMode ApvAppRegister::triggerMode() const { return TriggerMode(value(BCLK_MODE)); }

  ApvAppRegister::TriggerMode& ApvAppRegister::triggerMode() {
    return *dynamic_cast<TriggerMode*>(operator[](BCLK_MODE).get());
  }

  ApvAppRegister& ApvAppRegister::setTriggerMode(const TriggerMode& mode) {
    setValue(BCLK_MODE, mode);
    return *this;
  }

  bool ApvAppRegister::TriggerMode::apvReset() const { return (word_ >> 0) & 0x1; }

  ApvAppRegister::TriggerMode& ApvAppRegister::TriggerMode::setApvReset(bool en) {
    set(0, en, 1);
    return *this;
  }

  bool ApvAppRegister::TriggerMode::apvTestPulse() const { return (word_ >> 1) & 0x1; }

  ApvAppRegister::TriggerMode& ApvAppRegister::TriggerMode::setApvTestPulse(bool en) {
    set(1, en, 1);
    return *this;
  }

  bool ApvAppRegister::TriggerMode::externalTrigger() const { return (word_ >> 2) & 0x1; }

  ApvAppRegister::TriggerMode& ApvAppRegister::TriggerMode::setExternalTrigger(bool en) {
    set(2, en, 1);
    return *this;
  }

  bool ApvAppRegister::TriggerMode::nimTriggerPolarity() const { return (word_ >> 3) & 0x1; }

  ApvAppRegister::TriggerMode& ApvAppRegister::TriggerMode::setNimTriggerPolarity(bool en) {
    set(3, en, 1);
    return *this;
  }

  void ApvAppRegister::TriggerMode::print(std::ostream& os) const {
    os << "APV reset: " << std::boolalpha << apvReset() << "\n"
       << "APV test pulse: " << std::boolalpha << apvTestPulse() << "\n"
       << "External trigger: " << std::boolalpha << externalTrigger() << "\n"
       << "NIM trigger polarity: " << std::boolalpha << nimTriggerPolarity() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  bool ApvAppRegister::ApzStatus::phaseCalibBusy() const { return word_ & 0x1; }

  bool ApvAppRegister::ApzStatus::pedestalCalibBusy() const { return (word_ >> 1) & 0x1; }

  bool ApvAppRegister::ApzStatus::phaseAligned() const { return (word_ >> 2) & 0x1; }

  bool ApvAppRegister::ApzStatus::watchdogFlag() const { return (word_ >> 3) & 0x1; }

  bool ApvAppRegister::ApzStatus::calibAllDone() const { return (word_ >> 4) & 0x1; }

  bool ApvAppRegister::ApzStatus::commandDone() const { return (word_ >> 5) & 0x1; }

  bool ApvAppRegister::ApzStatus::apzBypassN() const { return (word_ >> 6) & 0x1; }

  bool ApvAppRegister::ApzStatus::apzEnabled() const { return (word_ >> 7) & 0x1; }

  uint8_t ApvAppRegister::ApzStatus::calibAllCurrent() const { return (word_ >> 8) & 0xf; }

  uint16_t ApvAppRegister::ApzStatus::apzChannelStatus() const { return (word_ >> 16) & 0xff; }

  void ApvAppRegister::ApzStatus::print(std::ostream& os) const {
    os << "Phase calibration busy: " << std::boolalpha << phaseCalibBusy() << "\n"
       << "Pedestal calibration busy: " << std::boolalpha << pedestalCalibBusy() << "\n"
       << "Phase-aligned: " << std::boolalpha << phaseAligned() << "\n"
       << "Watchdog flag: " << std::boolalpha << watchdogFlag() << "\n"
       << "Calibration done: " << std::boolalpha << calibAllDone() << "\n"
       << "Command done: " << std::boolalpha << commandDone() << "\n"
       << "Bypassing APZ code: " << std::boolalpha << apzBypassN() << "\n"
       << "APZ code enabled: " << std::boolalpha << apzEnabled() << "\n"
       << "Current channel calibrated (calibrate all): " << (uint16_t)calibAllCurrent() << "\n"
       << "Channel statuses: " << std::bitset<16>(apzChannelStatus()) << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  uint8_t ApvAppRegister::ApzZeroSuppThreshold::fractionalThreshold() const { return (word_ >> 2) & 0x3f; }

  ApvAppRegister::ApzZeroSuppThreshold& ApvAppRegister::ApzZeroSuppThreshold::setFractionalThreshold(uint8_t ft) {
    set(2, ft, 6);
    return *this;
  }

  uint8_t ApvAppRegister::ApzZeroSuppThreshold::integerThreshold() const { return (word_ >> 8) & 0x3f; }

  ApvAppRegister::ApzZeroSuppThreshold& ApvAppRegister::ApzZeroSuppThreshold::setIntegerThreshold(uint8_t it) {
    set(8, it, 6);
    return *this;
  }

  void ApvAppRegister::ApzZeroSuppThreshold::print(std::ostream& os) const {
    os << "Threshold:\n"
       << "Fractional part: " << (uint16_t)fractionalThreshold() << "\n"
       << "   Integer part: " << (uint16_t)integerThreshold() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  bool ApvAppRegister::ApzZeroSuppParameters::peakFindMode() const { return word_ & 0x1; }

  ApvAppRegister::ApzZeroSuppParameters& ApvAppRegister::ApzZeroSuppParameters::setPeakFindMode(bool pfm) {
    set(0, pfm, 1);
    return *this;
  }

  bool ApvAppRegister::ApzZeroSuppParameters::disablePedestalCorrection() const { return (word_ >> 2) & 0x1; }

  ApvAppRegister::ApzZeroSuppParameters& ApvAppRegister::ApzZeroSuppParameters::setDisablePedestalCorrection(bool dpc) {
    set(2, dpc, 1);
    return *this;
  }

  bool ApvAppRegister::ApzZeroSuppParameters::forceSignal() const { return (word_ >> 3) & 0x1; }

  ApvAppRegister::ApzZeroSuppParameters& ApvAppRegister::ApzZeroSuppParameters::setForceSignal(bool fs) {
    set(3, fs, 1);
    return *this;
  }

  bool ApvAppRegister::ApzZeroSuppParameters::thresholdMode() const { return (word_ >> 4) & 0x1; }

  ApvAppRegister::ApzZeroSuppParameters& ApvAppRegister::ApzZeroSuppParameters::setThresholdMode(bool tm) {
    set(4, tm, 1);
    return *this;
  }

  void ApvAppRegister::ApzZeroSuppParameters::print(std::ostream& os) const {
    os << "Peak find mode: " << std::boolalpha << peakFindMode() << "\n"
       << "Disable pedestal correction: " << std::boolalpha << disablePedestalCorrection() << "\n"
       << "Force signal: " << std::boolalpha << forceSignal() << "\n"
       << "Threshold mode: " << std::boolalpha << thresholdMode() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  std::ostream& operator<<(std::ostream& os, const ApvAppRegister::EventBuilderMode& mode) {
    switch (mode) {
      case ApvAppRegister::FRAME_EVENT_CNT:
      default:
        return os << "single-FEC, frame-of-event (8-bit)";
      case ApvAppRegister::FRAME_RUN_CNT:
        return os << "test mode, frame-of-run (32-bit)";
      case ApvAppRegister::TIMESTAMP:
        return os << "multiple-FEC, timestamp (24-bit)";
    }
  }

  //-----------------------------------------------------------------------------------------------

  ApvAppRegister::EbEventInfoData::HeaderInfo ApvAppRegister::EbEventInfoData::headerInfo() const {
    return static_cast<HeaderInfo>((word_ >> 8) & 0xff);
  }

  ApvAppRegister::EbEventInfoData& ApvAppRegister::EbEventInfoData::setHeaderInfo(const HeaderInfo& hi) {
    set(8, hi, 8);
    return *this;
  }

  uint16_t ApvAppRegister::EbEventInfoData::runLabel() const { return (word_ >> 16) & 0xffff; }

  ApvAppRegister::EbEventInfoData& ApvAppRegister::EbEventInfoData::setRunLabel(uint16_t lab) {
    set(16, lab, 16);
    return *this;
  }

  std::ostream& operator<<(std::ostream& os, const ApvAppRegister::EbEventInfoData::HeaderInfo& eid) {
    switch (eid) {
      case ApvAppRegister::EbEventInfoData::LABEL_AND_DATALENGTH:
      default:
        return os << "HINFO_LABEL[31:24] | EVBLD_DATALENGTH[15:0]";
      case ApvAppRegister::EbEventInfoData::TRIGGER_AND_DATALENGTH:
        return os << "TRIGGER_COUNTER[31:24] | EVBLD_DATALENGTH[15:0]";
      case ApvAppRegister::EbEventInfoData::TRIGGER_ONLY:
        return os << "TRIGGER_COUNTER[31:0]";
    }
  }

  void ApvAppRegister::EbEventInfoData::print(std::ostream& os) const {
    os << "Header info: " << headerInfo() << " (" << (uint16_t)headerInfo() << ")\n"
       << "Run label: 0x" << std::hex << runLabel() << std::dec << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  ChannelEnable::ChannelEnable(uint32_t word) : POI<2>(word, pattern_t{0, 2, 4, 6, 8, 10, 12, 14}) {}

  ChannelEnable::ChannelEnable() : POI<2>(0, pattern_t{0, 2, 4, 6, 8, 10, 12, 14}) {}

  ChannelEnable::~ChannelEnable() {}

  ChannelEnable& ChannelEnable::setMode(size_t ch, Mode mode) {
    setEnabled(ch, (uint8_t)mode);
    if (pattern_.size() == 0 || ch >= pattern_.size())
      SRS_WARNING("ChannelEnable:setMode") << "Channel #" << ch << " was not found in POI.";
    else
      SRS_DEBUG("ChannelEnable:setMode") << "Set channel " << ch << " to " << mode << ". "
                                         << "LSB: " << pattern_.at(ch) << ". New word: " << std::bitset<16>(word_);
    return *this;
  }

  void ChannelEnable::print(std::ostream& os) const {
    os << "PoI: " << std::bitset<16>(*this) << "\n";
    for (size_t i = 0; i < size(); ++i)
      os << "* HDMI channel " << i << ": " << mode(i) << "\n";
  }

  std::ostream& operator<<(std::ostream& os, const ChannelEnable::Mode& mode) {
    switch (mode) {
      case ChannelEnable::DISABLED:
      default:
        return os << "disabled";
      case ChannelEnable::MASTER:
        return os << "master";
      case ChannelEnable::SLAVE:
        return os << "slave";
      case ChannelEnable::ALL_APVS:
        return os << "master/slave";
    }
  }
}  // namespace srs
