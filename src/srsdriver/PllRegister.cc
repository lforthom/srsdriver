/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srsdriver/PllRegister.h"
#include "srsutils/Logging.h"

namespace srs {
  PllRegister::PllRegister() {
    // replace generic words objects with custom words objects
    (*this)[CSR1_FINEDELAY].reset(new Csr1FineDelay(*operator[](CSR1_FINEDELAY)));
  }

  PllRegister::PllRegister(const words_t& words) : Config(words) {
    // replace generic words objects with custom words objects
    (*this)[CSR1_FINEDELAY].reset(new Csr1FineDelay(*operator[](CSR1_FINEDELAY)));
  }

  PllRegister::Csr1FineDelay PllRegister::csr1FineDelay() const { return Csr1FineDelay(value(CSR1_FINEDELAY) & 0xff); }

  PllRegister::Csr1FineDelay& PllRegister::csr1FineDelay() {
    return *dynamic_cast<Csr1FineDelay*>(operator[](CSR1_FINEDELAY).get());
  }

  PllRegister& PllRegister::setCsr1FineDelay(const PllRegister::Csr1FineDelay& del) {
    setValue(CSR1_FINEDELAY, del);
    return *this;
  }

  uint16_t PllRegister::triggerDelay() const { return value(TRG_DELAY) & 0xff; }

  PllRegister& PllRegister::setTriggerDelay(uint16_t td) {
    setValue(TRG_DELAY, td);
    return *this;
  }

  void PllRegister::print(std::ostream& os) const {
    os << "===================== PLL register ======================\n"
       << "----- CSR1 fine delay:\n";
    csr1FineDelay().print(os);
    os << "\n"
       << "Trigger delay (clock cycles): " << triggerDelay() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  const fnames_t PllRegister::fieldNames = {{CSR1_FINEDELAY, "CSR1_FINEDELAY"}, {TRG_DELAY, "TRG_DELAY"}};

  std::ostream& operator<<(std::ostream& os, const PllRegister::Field& field) {
    return os << PllRegister::fieldNames.at(field);
  }

  //-----------------------------------------------------------------------------------------------

  uint8_t PllRegister::Csr1FineDelay::phaseAdjust() const { return word_ & 0x7; }

  PllRegister::Csr1FineDelay& PllRegister::Csr1FineDelay::setPhaseAdjust(uint8_t adj) {
    if (adj > 11)
      throw SRS_ERROR("PllRegister:Csr1FineDelay:setPhaseAdjust") << "Clock fine-phase adjust should be <= 11.";
    set(0, adj, 4);
    return *this;
  }

  bool PllRegister::Csr1FineDelay::phaseFlip() const { return (word_ >> 4) & 0x1; }

  PllRegister::Csr1FineDelay& PllRegister::Csr1FineDelay::setPhaseFlip(bool pf) {
    set(4, pf, 1);
    return *this;
  }

  bool PllRegister::Csr1FineDelay::triggerDelayEnabled() const { return (word_ >> 5) & 0x1; }

  PllRegister::Csr1FineDelay& PllRegister::Csr1FineDelay::setTriggerDelayEnabled(bool td) {
    set(5, td, 1);
    return *this;
  }

  void PllRegister::Csr1FineDelay::print(std::ostream& os) const {
    os << "Fine-phase adjust: " << (int)phaseAdjust() << "\n"
       << "Clock phase flip: " << std::boolalpha << phaseFlip() << "\n"
       << "Access to trigger delay register: " << std::boolalpha << triggerDelayEnabled() << "\n";
  }
}  // namespace srs
