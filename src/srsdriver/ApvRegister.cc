/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bitset>
#include <iostream>

#include "srsdriver/ApvRegister.h"

namespace srs {
  ApvRegister::ApvRegister() {
    // replace generic words objects with custom words objects
    (*this)[MODE].reset(new ApvMode(*operator[](MODE)));
  }

  ApvRegister::ApvRegister(const words_t& words) : Config(words) {
    // replace generic words objects with custom words objects
    (*this)[MODE].reset(new ApvMode(*operator[](MODE)));
  }

  uint16_t ApvRegister::error() const { return value(ERROR); }

  ApvRegister::ApvMode ApvRegister::mode() const { return ApvMode(value(MODE)); }

  ApvRegister::ApvMode& ApvRegister::mode() { return *dynamic_cast<ApvMode*>(operator[](MODE).get()); }

  ApvRegister& ApvRegister::setMode(const ApvRegister::ApvMode& mode) {
    setValue(MODE, mode);
    return *this;
  }

  uint16_t ApvRegister::latency() const { return value(LATENCY) & 0xff; }

  ApvRegister& ApvRegister::setLatency(uint16_t lat) {
    setValue(LATENCY, lat);
    return *this;
  }

  uint16_t ApvRegister::muxGain() const { return value(MUXGAIN) & 0xff; }

  ApvRegister& ApvRegister::setMuxGain(uint16_t mg) {
    setValue(MUXGAIN, mg);
    return *this;
  }

  uint16_t ApvRegister::iPre() const { return value(IPRE) & 0xff; }

  ApvRegister& ApvRegister::setIPre(uint16_t ip) {
    setValue(IPRE, ip);
    return *this;
  }

  uint16_t ApvRegister::iPcasc() const { return value(IPCASC) & 0xff; }

  ApvRegister& ApvRegister::setIPcasc(uint16_t ip) {
    setValue(IPCASC, ip);
    return *this;
  }

  uint16_t ApvRegister::iPsf() const { return value(IPSF) & 0xff; }

  ApvRegister& ApvRegister::setIPsf(uint16_t ip) {
    setValue(IPSF, ip);
    return *this;
  }

  uint16_t ApvRegister::iSha() const { return value(ISHA) & 0xff; }

  ApvRegister& ApvRegister::setISha(uint16_t is) {
    setValue(ISHA, is);
    return *this;
  }

  uint16_t ApvRegister::iSsf() const { return value(ISSF) & 0xff; }

  ApvRegister& ApvRegister::setISsf(uint16_t is) {
    setValue(ISSF, is);
    return *this;
  }

  uint16_t ApvRegister::iPsp() const { return value(IPSP) & 0xff; }

  ApvRegister& ApvRegister::setIPsp(uint16_t ip) {
    setValue(IPSP, ip);
    return *this;
  }

  uint16_t ApvRegister::iMuxin() const { return value(IMUXIN) & 0xff; }

  ApvRegister& ApvRegister::setIMuxin(uint16_t im) {
    setValue(IMUXIN, im);
    return *this;
  }

  uint16_t ApvRegister::iCal() const { return value(ICAL) & 0xff; }

  ApvRegister& ApvRegister::setICal(uint16_t ic) {
    setValue(ICAL, ic);
    return *this;
  }

  uint16_t ApvRegister::vPsp() const { return value(VPSP) & 0xff; }

  ApvRegister& ApvRegister::setVPsp(uint16_t vp) {
    setValue(VPSP, vp);
    return *this;
  }

  uint16_t ApvRegister::vFs() const { return value(VFS) & 0xff; }

  ApvRegister& ApvRegister::setVFs(uint16_t vf) {
    setValue(VFS, vf);
    return *this;
  }

  uint16_t ApvRegister::vFp() const { return value(VFP) & 0xff; }

  ApvRegister& ApvRegister::setVFp(uint16_t vf) {
    setValue(VFP, vf);
    return *this;
  }

  uint16_t ApvRegister::cDrv() const { return value(CDRV) & 0xff; }

  ApvRegister& ApvRegister::setCDrv(uint16_t cd) {
    setValue(CDRV, cd);
    return *this;
  }

  uint16_t ApvRegister::cSel() const { return value(CSEL) & 0xff; }

  ApvRegister& ApvRegister::setCSel(uint16_t cs) {
    setValue(CSEL, cs);
    return *this;
  }

  void ApvRegister::print(std::ostream& os) const {
    os << "===================== APV register ======================\n"
       << "----- APV mode:\n";
    mode().print(os);
    os << "\n"
       << "Latency: " << latency() << "\n"
       << "Output buffer gain: " << muxGain() << "\n"
       << "Preamp. current: " << iPre() << "\n"
       << "iPsf: " << iPsf() << "\n"
       << "iSha: " << iSha() << "\n"
       << "iSsf: " << iSsf() << "\n"
       << "iPsp: " << iPsp() << "\n"
       << "Output buff. pedestal: " << iMuxin() << "\n"
       << "Cal. pulse strength: " << iCal() << "\n"
       << "vPsp: " << vPsp() << "\n"
       << "vFs: " << vFs() << "\n"
       << "Calib. channel mask: " << cDrv() << "\n"
       << "Calib. fine phase: " << cSel() << "\n";
  }

  void ApvRegister::ApvMode::print(std::ostream& os) const {
    os << "Analogue bias: " << std::boolalpha << analogueBias() << "\n"
       << "Trigger mode: " << triggerMode() << "\n"
       << "Calibration inhibit: " << std::boolalpha << calibrationInhibit() << "\n"
       << "Readout mode: " << readoutMode() << "\n"
       << "Readout frequency: " << readoutFrequency() << "\n"
       << "Preamplifier polarity: " << preampPolarity() << "\n";
  }

  //-----------------------------------------------------------------------------------------------

  const fnames_t ApvRegister::fieldNames = {{ERROR, "ERROR"},
                                            {MODE, "MODE"},
                                            {LATENCY, "LATENCY"},
                                            {MUXGAIN, "MUXGAIN"},
                                            {IPRE, "IPRE"},
                                            {IPCASC, "IPCASC"},
                                            {IPSF, "IPSF"},
                                            {ISHA, "ISHA"},
                                            {ISSF, "ISSF"},
                                            {IPSP, "IPSP"},
                                            {IMUXIN, "IMUXIN"},
                                            {ICAL, "ICAL"},
                                            {VPSP, "VPSP"},
                                            {VFS, "VFS"},
                                            {VFP, "VFP"},
                                            {CDRV, "CDRV"},
                                            {CSEL, "CSEL"}};

  std::ostream& operator<<(std::ostream& os, const ApvRegister::Field& field) {
    return os << ApvRegister::fieldNames.at(field);
  }

  //-----------------------------------------------------------------------------------------------

  bool ApvRegister::ApvMode::analogueBias() const { return word_ & 0x1; }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setAnalogueBias(bool ab) {
    set(0, ab, 1);
    return *this;
  }

  ApvRegister::ApvMode::TriggerMode ApvRegister::ApvMode::triggerMode() const {
    return TriggerMode((word_ >> 1) & 0x1);
  }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setTriggerMode(const ApvRegister::ApvMode::TriggerMode& tm) {
    set(1, tm, 1);
    return *this;
  }

  bool ApvRegister::ApvMode::calibrationInhibit() const { return (word_ >> 2) & 0x1; }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setCalibrationInhibit(bool ci) {
    set(2, ci, 1);
    return *this;
  }

  ApvRegister::ApvMode::ReadoutMode ApvRegister::ApvMode::readoutMode() const {
    return ReadoutMode((word_ >> 3) & 0x1);
  }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setReadoutMode(const ApvRegister::ApvMode::ReadoutMode& rm) {
    set(3, rm, 1);
    return *this;
  }

  ApvRegister::ApvMode::ReadoutFrequency ApvRegister::ApvMode::readoutFrequency() const {
    return ReadoutFrequency((word_ >> 4) & 0x1);
  }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setReadoutFrequency(const ApvRegister::ApvMode::ReadoutFrequency& freq) {
    set(4, freq, 1);
    return *this;
  }

  ApvRegister::ApvMode::PreampPolarity ApvRegister::ApvMode::preampPolarity() const {
    return PreampPolarity((word_ >> 5) & 0x1);
  }

  ApvRegister::ApvMode& ApvRegister::ApvMode::setPreampPolarity(const ApvRegister::ApvMode::PreampPolarity& pp) {
    set(5, pp, 1);
    return *this;
  }

  std::ostream& operator<<(std::ostream& os, const ApvRegister::ApvMode::TriggerMode& mode) {
    switch (mode) {
      case ApvRegister::ApvMode::T_3SAMPLE:
        return os << "3-sample";
      case ApvRegister::ApvMode::T_1SAMPLE:
        return os << "1-sample";
      default:
        return os << "invalid";
    }
  }

  std::ostream& operator<<(std::ostream& os, const ApvRegister::ApvMode::ReadoutMode& mode) {
    switch (mode) {
      case ApvRegister::ApvMode::RO_DECONVOLUTION:
        return os << "deconvolution";
      case ApvRegister::ApvMode::RO_PEAK:
        return os << "peak";
      default:
        return os << "invalid";
    }
  }

  std::ostream& operator<<(std::ostream& os, const ApvRegister::ApvMode::ReadoutFrequency& freq) {
    switch (freq) {
      case ApvRegister::ApvMode::F_20MHZ:
        return os << "20 MHz";
      case ApvRegister::ApvMode::F_40MHZ:
        return os << "40 MHz";
      default:
        return os << "invalid";
    }
  }

  std::ostream& operator<<(std::ostream& os, const ApvRegister::ApvMode::PreampPolarity& pol) {
    switch (pol) {
      case ApvRegister::ApvMode::POL_NONINV:
        return os << "non-inverting";
      case ApvRegister::ApvMode::POL_INV:
        return os << "inverting";
      default:
        return os << "invalid";
    }
  }
}  // namespace srs
