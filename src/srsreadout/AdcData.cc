/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <byteswap.h>

#include <bitset>
#include <iostream>

#include "srsreadout/AdcData.h"
#include "srsutils/Logging.h"

namespace srs {
  AdcData::AdcData(witer_t& ptr, const witer_t end) : SrsData(ptr, end) {}

  AdcData::ApvEvent AdcData::next() const {
    ApvEvent evt;
    std::bitset<8> addr;
    try {
      while (true) {
        if (nextWord() < apv_header_level_)
          if (nextWord() < apv_header_level_)
            if (nextWord() < apv_header_level_) {
              for (size_t i = 0; i < addr.size(); ++i)
                addr[i] = (nextWord() > apv_header_level_);
              evt.error = nextWord() > apv_header_level_;
              for (size_t i = 0; i < evt.data.size(); ++i)
                evt.data[i] = nextWord();
              break;
            }
      }
    } catch (const SrsFrame::NoMoreFramesException&) {
      throw;
    }
    evt.address = addr.to_ulong();
    if (evt.error)
      SRS_WARNING("AdcData:next") << "APV25 logic error flag set.";
    SRS_DEBUG("AdcData:next") << "Address: " << (uint16_t)evt.address << " -> channel: " << physChannel(evt.address)
                              << ", error: " << std::boolalpha << evt.error << ".";
    return evt;
  }

  void AdcData::print(std::ostream& os) const {
    // roll the iterator back to beginning (keeping previous position)
    auto prev_pos = ptr_;
    ptr_ = beg_;
    for (size_t i = 0; /* no end */; ++i) {
      try {
        const auto evt = next();
        os << "Address: " << (uint16_t)evt.address << " (channel: " << physChannel(evt.address) << ")\n"
           << "Error? " << std::boolalpha << evt.error << "\n"
           << "ADC data:\n";
        for (size_t j = 0; j < evt.data.size(); j += 8) {
          for (size_t k = 0; k < 8; ++k)
            os << "\t" << evt.data.at(j + k);
          os << "\n";
        }
      } catch (const SrsFrame::NoMoreFramesException&) {
        break;
      }
    }
    // rollback to previous position
    ptr_ = prev_pos;
  }

  uint16_t AdcData::nextWord() const {
    if (ptr_ == end_)
      throw SrsFrame::NoMoreFramesException();
    if (!(lsb_ = !lsb_))  // swap the readout bits mask
      return bswap_16(*ptr_ & 0xffff);
    else
      return bswap_16((*(ptr_++) >> 16) & 0xffff);
  }

  uint16_t AdcData::physChannel(uint16_t ch) { return 32 * (ch % 4) + 8 * int(ch / 4) - 31 * int(ch / 16); }
}  // namespace srs
