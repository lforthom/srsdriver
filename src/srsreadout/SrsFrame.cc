/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <byteswap.h>

#include <iostream>

#include "srsreadout/AdcData.h"
#include "srsreadout/SrsFrame.h"
#include "srsutils/Logging.h"
#include "srsutils/String.h"

namespace srs {
  SrsData::SrsData(witer_t& ptr, const witer_t end) : ptr_(ptr), beg_(ptr), end_(end) { ptr = end; }

  //===============================================================================================

  SrsFrame::SrsFrame(const words_t& words, ApvAppRegister::EventBuilderMode mode)
      : words_t(words), mode_(mode), ptr_(std::next(cbegin(), NUM_HDR)) {}

  void SrsFrame::print(std::ostream& os) const {
    if (empty()) {
      SRS_WARNING("SrsFrame:print") << "Empty SRS frame.";
      return;
    }
    for (size_t i = 0; i < /*size()*/ 10; ++i)
      os << utils::format("%8d\t%10zu", i, (uint32_t)at(i)) << "\t" << std::bitset<32>(bswap_32(at(i))) << "\t"
         << std::bitset<32>(at(i)) << "\t" << utils::format("%08x\n", at(i));
    os << "======================= SRS frame =======================\n"
       << "--- Frame counter:\n";
    frameCounter().print(os);
    os << "\n"
       << "Data source: " << dataSource() << "\n"
       << "APV index: " << (uint16_t)apvIndex() << "\n"
       << "--- Header info:\n";
    headerInfo().print(os);
    os << "\n"
       << "--------------------- frame content ---------------------\n"
       << "Size: " << size() - NUM_HDR << "\n"
       << "Content:\n";

    const auto ds = dataSource();
    switch (ds) {
      case DataSource::ADC: {
        while (true) {
          try {
            next<AdcData>().print(os);
          } catch (const NoMoreFramesException&) {
            break;
          }
        }
      } break;
      case DataSource::APV_ZS:
      case DataSource::A_CCARD:
        os << ">>> Unhandled data format: " << ds << " <<<" << std::endl;
        break;
      default:
        os << ">>> invalid/unknown data format <<<" << std::endl;
        break;
    }
  }

  SrsData* SrsFrame::nextPtr() const {
    try {
      switch (dataSource()) {
        case DataSource::ADC:
          return new AdcData(next<AdcData>());
        default:
          return nullptr;
      }
    } catch (const NoMoreFramesException&) {
      return nullptr;
    }
  }

  std::string SrsFrame::dataSourceStr() const {
    const auto& header = at(SRC_CHANNEL);
    std::string fmt;
    fmt += (char)((header >> 24) & 0xff);
    fmt += (char)((header >> 16) & 0xff);
    fmt += (char)((header >> 8) & 0xff);
    return fmt;
  }

  SrsFrame::DataSource SrsFrame::dataSource() const {
    const auto fmt = dataSourceStr();
    if (fmt == "ADC")
      return DataSource::ADC;
    if (fmt == "APZ")
      return DataSource::APV_ZS;
    if (fmt == "AZC")
      return DataSource::A_CCARD;
    else
      throw SRS_ERROR("SrsFrame:dataSource") << "Invalid data source unpacked: " << fmt << "!";
  }

  std::ostream& operator<<(std::ostream& os, const SrsFrame::DataSource& src) {
    switch (src) {
      case SrsFrame::DataSource::ADC:
        return os << "ADC mode";
      case SrsFrame::DataSource::APV_ZS:
        return os << "APV with zero-suppression";
      case SrsFrame::DataSource::A_CCARD:
        return os << "Arizona C-Card";
      default:
        return os << "unknown/invalid (" << (uint16_t)src << ")";
    }
  }

  //===============================================================================================

  void SrsFrame::HeaderInfo::print(std::ostream& os) const {
    os << "Data length: " << adcSize() << "\n"
       << "FEC id: 0x" << std::hex << (uint16_t)fecId() << std::dec << "\n";
  }

  //===============================================================================================

  SrsFrame::FrameCounter::FrameCounter(uint32_t word, ApvAppRegister::EventBuilderMode mode) : Word(word), mode(mode) {}

  bool SrsFrame::FrameCounter::check() const {
    switch (mode) {
      case ApvAppRegister::EventBuilderMode::FRAME_EVENT_CNT:  // single-FEC, frame-of-event (8-bit)
        return (word_ >> 8 == 0x0);
      case ApvAppRegister::EventBuilderMode::FRAME_RUN_CNT:  // test mode, frame-of-run (32-bit)
      case ApvAppRegister::EventBuilderMode::TIMESTAMP:      // multiple-FEC, timestamp (24-bit)
        return true;
    }
    return false;
  }

  void SrsFrame::FrameCounter::print(std::ostream& os) const {
    os << "Basic checks passed: " << std::boolalpha << check() << "\n";
    if (mode == ApvAppRegister::EventBuilderMode::FRAME_RUN_CNT) {  // test mode
      os << "Global frame counter: " << globalFrameCounter() << std::endl;
      return;
    }
    os << "Frame: " << (uint16_t)frameCounter() << "\n";
    if (mode == ApvAppRegister::EventBuilderMode::TIMESTAMP)  // multiple-FEC
      os << "Timestamp: " << timestamp() << "\n";
  }

  uint16_t SrsFrame::FrameCounter::timestamp() const {
    if (mode == ApvAppRegister::EventBuilderMode::TIMESTAMP)
      return (word_ >> 8) & 0xffffff;
    SRS_WARNING("SrsFrame:FrameCounter:timestamp")
        << "FrameCounter timestamp requested while event builder mode is " << mode << ".";
    return 0;
  }

  uint8_t SrsFrame::FrameCounter::frameCounter() const {
    if (mode == ApvAppRegister::EventBuilderMode::FRAME_RUN_CNT) {
      SRS_WARNING("SrsFrame:FrameCounter:frameCounter")
          << "FrameCounter counter requested while event builder mode is " << mode << ".";
      return 0;
    }
    return word_ & 0xff;
  }

  uint32_t SrsFrame::FrameCounter::globalFrameCounter() const {
    if (mode == ApvAppRegister::EventBuilderMode::FRAME_RUN_CNT)
      return word_;
    SRS_WARNING("SrsFrame:FameCounter:globalFrameCounter")
        << "FrameCounter global counter requested while event builder mode is " << mode << ".";
    return 0;
  }
}  // namespace srs
