/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCharts/QLineSeries>
#include <iostream>  //FIXME

#include "srscontrol/WaveformCollector.h"
#include "srsreadout/AdcData.h"
#include "srsutils/Logging.h"

QT_CHARTS_USE_NAMESPACE

using namespace std::string_literals;

WaveformCollector::WaveformCollector() : chart_view_(new QChartView(this)) {
  //chart_view_->setRenderHint(QPainter::Antialiasing);
  QChart::legend()->setVisible(true);
  QChart::legend()->setAlignment(Qt::AlignBottom);
  QChart::createDefaultAxes();
}

void WaveformCollector::switchChannels(const std::vector<bool>& ch_en) {
  for (size_t i = 0; i < ch_en.size(); ++i) {
    m_channels_[i].enabled = ch_en.at(i);
    m_channels_[i].title = QString::fromStdString("Ch."s + std::to_string(i));
  }
  SRS_INFO("WaveformCollector:switchChannels").log([this](auto& log) {
    std::string ch_en, ch_dis, sep_en, sep_dis;
    for (const auto& ch : m_channels_) {
      if (ch.second.enabled)
        ch_en += sep_en + ch.second.title.toStdString(), sep_en = ", ";
      else
        ch_dis += sep_dis + ch.second.title.toStdString(), sep_dis = ", ";
    }
    log << "Channels enabled:  " << ch_en << "\n\t"
        << "Channels disabled: " << ch_dis << ".";
  });
}

void WaveformCollector::setUnpacking(bool unpack) { unpack_raw_ = unpack; }

bool WaveformCollector::feed(const srs::SrsFrameCollection& frames, size_t max_samples) {
  QChart::removeAllSeries();
  std::vector<QLineSeries*> series(m_channels_.size(), nullptr);
  size_t num_enabled = 0;
  for (const auto& ch : m_channels_) {
    series[ch.first] = new QLineSeries;
    series[ch.first]->setName(ch.second.title);
    if (ch.second.enabled)
      ++num_enabled;
    else
      series[ch.first]->setColor(QColor(0, 0, 0, 10));
  }
  if (num_enabled == 0)
    return true;
  for (const auto& frm : frames) {
    const auto ch_id = frm.daqChannel();
    SRS_DEBUG("WaveformCollector:feed") << "Unpacking channel #" << ch_id << ".";
    if (m_channels_.count(ch_id) == 0)
      throw SRS_ERROR("WaveformCollector:feed") << "Invalid channel number retrieved: " << (int)ch_id << ".";
    const auto& ch = m_channels_.at(ch_id);
    if (!ch.enabled)  // skip non-displayed channels
      continue;
    auto* gr = series.at(ch_id);
    try {
      const auto frame = frm.next<srs::AdcData>();  // collect one frame
      while (true) {
        if (unpack_raw_) {
          try {
            const auto data = frame.next().data;
            for (size_t i = (size_t)gr->count(); i < std::min(data.size(), max_samples);
                 ++i)  // start the unpacking for this channel
              gr->append(i, data.at(i));
          } catch (const srs::SrsFrame::NoMoreFramesException&) {
            break;
          }
        } else {
          if ((size_t)gr->count() >= max_samples)
            break;
          gr->append(gr->count(), frame.nextWord());
        }
      }
    } catch (const srs::SrsFrame::NoMoreFramesException&) {
    }
  }
  for (auto* gr : series)
    QChart::addSeries(gr);  // transfers serie ownership to chart
  QChart::createDefaultAxes();
  QChart::axes(Qt::Vertical)[0]->setRange(800, 3300);
  return true;
}
