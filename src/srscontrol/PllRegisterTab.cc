/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QMessageBox>
#include <bitset>

#include "srscontrol/FecControl.h"
#include "srscontrol/PllRegisterTab.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_PllRegisterTab.h"

using namespace std::string_literals;

PllRegisterTab::PllRegisterTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::PllRegisterTab), main_(main), pll_ch_sel_(NUM_CH) {
  ui_->setupUi(this);
  connect(ui_->pll_button_refresh, SIGNAL(clicked()), this, SLOT(refresh()));
  connect(ui_->pll_button_push, SIGNAL(clicked()), this, SLOT(push()));
  ui_->pll_button_refresh->setEnabled(true);
  ui_->pll_button_push->setEnabled(false);

  // channels selector widget
  for (size_t i = 0; i < NUM_CH; ++i) {
    pll_ch_sel_[i] = this->findChild<QCheckBox*>(QString::asprintf("pll_ch%zu_sel", i));
    pll_ch_sel_[i]->setEnabled(true);
  }
  ui_->pll_chall_sel->setEnabled(true);
  ui_->pll_chall_sel->setCurrentIndex((int)NUM_CH);
  connect(ui_->pll_chall_sel, SIGNAL(currentIndexChanged(int)), this, SLOT(channelSelected(int)));
  connect(ui_->pll_clk_fine_phase, &QSlider::valueChanged, this, [=]() {
    ui_->pll_clk_fine_phase_display->setText(QString::number(ui_->pll_clk_fine_phase->value()));
  });
  connect(ui_->pll_trg_delay, &QSlider::valueChanged, this, [=]() {
    ui_->pll_trg_delay_display->setText(QString::number(ui_->pll_trg_delay->value()));
  });

  //--- tables
  std::vector<QString> labels_pll;
  for (const auto& lab : srs::PllRegister::fieldNames) {
    lut_pll_[lab.first] = labels_pll.size();
    labels_pll.emplace_back(QString::fromStdString(lab.second));
  }
  table_pll_info_.reset(new QElementsTable(labels_pll, {""}, ui_->table_pll));
  ui_->table_pll->setModel(table_pll_info_.get());
}

PllRegisterTab::~PllRegisterTab() {}

void PllRegisterTab::refresh() {
  setEnabled(true);

  // fetch PLL register from slow control
  if (main_->srs())
    try {
      const auto pll_channels = readPllChannels();
      pll_ = main_->srs()->readPllRegister(pll_channels);
    } catch (const srs::LogMessage& err) {
      QMessageBox box;
      box.critical(0, "Error", ("Failed to update the PLL register!\nError: " + err.message()).data());
      box.setFixedSize(500, 200);
      return;
    }

  // "generic" human-friendly part
  const auto& pll_csr1_fd = pll_.csr1FineDelay();
  ui_->pll_clk_fine_phase->setValue(pll_csr1_fd.phaseAdjust());
  ui_->pll_clk_phase_flip->setChecked(pll_csr1_fd.phaseFlip());
  ui_->pll_trg_del_register->setChecked(pll_csr1_fd.triggerDelayEnabled());
  ui_->pll_trg_delay->setValue(pll_.triggerDelay());

  // "advanced" table part
  ui_->table_pll->setEnabled(true);
  for (size_t i = 0; i < pll_.size(); ++i) {
    if (lut_pll_.count(i) > 0)
      table_pll_info_->setData(lut_pll_.at(i), 0, (int)pll_.value(i), false);
  }
  table_pll_info_->layoutChanged();

  // only enable the "send" button if register first fetched
  ui_->pll_button_push->setEnabled(true);
  emit statusEvent(QString::fromStdString("PLL register pulled from SRS"));
}

void PllRegisterTab::channelSelected(int ch) {
  if (ch == NUM_CH)  // custom pattern selected
    for (auto* sel : pll_ch_sel_)
      sel->setEnabled(true);
  else
    for (size_t i = 0; i < pll_ch_sel_.size(); ++i) {
      pll_ch_sel_[i]->setEnabled(false);
      pll_ch_sel_[i]->setChecked(i == (size_t)ch);
    }
}

srs::ApvAddress PllRegisterTab::readPllChannels() const {
  std::bitset<8> poi(0);  // pattern of inhibit
  for (size_t i = 0; i < pll_ch_sel_.size(); ++i)
    poi[i] = pll_ch_sel_.at(i)->isChecked();
  return srs::ApvAddress(poi.to_ullong(), srs::ApvAddress::Mode::PLL);
}

void PllRegisterTab::push() {
  if (!main_->srs()) {
    QMessageBox box;
    box.critical(0, "Error", "Failed to update the PLL register!");
    box.setFixedSize(500, 200);
  }
  ui_->pll_button_refresh->setEnabled(false);
  ui_->pll_button_push->setEnabled(false);
  // PLL words
  // retrieve the list of channels to modify and upload the configuration
  auto pll_ch = readPllChannels();
  try {
    auto& csr_del = pll_.csr1FineDelay();
    csr_del.setPhaseAdjust(ui_->pll_clk_fine_phase->value());
    csr_del.setPhaseFlip(ui_->pll_clk_phase_flip->isChecked());
    csr_del.setTriggerDelayEnabled(ui_->pll_trg_del_register->isChecked());
    pll_.setCsr1FineDelay(csr_del);
    pll_.setTriggerDelay(ui_->pll_trg_delay->value());
    if (!main_->srs()->messenger().writeConfig(srs::P_APV, pll_ch, pll_))
      throw SRS_ERROR("PllRegisterTab:push") << "Communication error.";
  } catch (const srs::LogMessage& err) {
    QMessageBox box;
    box.critical(0, "Error", ("Failed to update the PLL register!\nError: "s + err.message()).data());
    box.setFixedSize(500, 200);
  }
  //FIXME upload changed attributes in table
  emit statusEvent("PLL register " + QString::number(pll_ch) + " pushed onto the SRS");
  ui_->pll_button_refresh->setEnabled(true);
  ui_->pll_button_push->setEnabled(true);
}
