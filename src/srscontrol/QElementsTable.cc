/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>
#include <QTableView>

#include "srscontrol/QElementsTable.h"

QElementsTable::QElementsTable(const std::vector<QString>& fields, const std::vector<QString>& cols, QTableView* table)
    : fields_(fields), cols_(cols), table_(table) {
  clear(fields.empty());
}

void QElementsTable::clear(bool clear_fields) {
  if (clear_fields)
    fields_.clear();
  data_ = std::vector<std::vector<QString> >(fields_.size(), std::vector<QString>(cols_.size(), ""));
  emit layoutChanged();
}

void QElementsTable::append(const std::vector<QString>& line) {
  data_.emplace_back(line);
  fields_.emplace_back(QString::number(data_.size()));
  emit layoutChanged();
}

int QElementsTable::rowCount(const QModelIndex& /*parent*/) const { return fields_.size(); }

int QElementsTable::columnCount(const QModelIndex& /*parent*/) const { return cols_.size(); }

QVariant QElementsTable::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role == Qt::DisplayRole)
    switch (orientation) {
      case Qt::Vertical:
        if (!fields_.empty() && section < (int)fields_.size())
          return fields_.at(section);
      case Qt::Horizontal:
        if (!cols_.empty() && section < (int)cols_.size())
          return cols_.at(section);
      default:
        return QVariant();
    }
  return QVariant();
}

QVariant QElementsTable::data(const QModelIndex& index, int role) const {
  switch (role) {
    case Qt::DisplayRole: {
      if (index.row() < (int)fields_.size() && index.column() < (int)cols_.size())
        return data_[index.row()][index.column()];
      else
        return QVariant();
    }
    case Qt::BackgroundRole: {
      if (mod_rows_.count(index.row()) > 0 && mod_rows_.at(index.row()))
        return QColor(Qt::yellow);
      return QVariant();
    }
    default:
      return QVariant();
  }
}

uint32_t QElementsTable::data(int field, int col) const {
  if (field >= (int)fields_.size() || col >= (int)cols_.size())
    return 0;
  auto raw_data = data_[field][col];
  return parseData(data_[field][col]);
}

uint32_t QElementsTable::parseData(const QString& raw_data) {
  bool ok = false;
  if (raw_data.startsWith("0x") || raw_data.startsWith("0X")) {
    // hexadecimal format
    auto ret = raw_data.mid(2).toUInt(&ok, 16);
    if (!ok)
      return 0;
    return ret;
  }
  if (raw_data.startsWith("b") || raw_data.startsWith("0b") || raw_data.startsWith("0B")) {
    // binary format
    uint32_t ret;
    if (raw_data.startsWith("b"))
      ret = raw_data.mid(1).toUInt(&ok, 2);
    else
      ret = raw_data.mid(2).toUInt(&ok, 2);
    if (!ok)
      return 0;
    return ret;
  }
  uint32_t ret = raw_data.toUInt(&ok);
  if (!ok)
    return 0;
  return ret;
}

bool QElementsTable::setData(const QModelIndex& index, const QVariant& value, int role) {
  if (role == Qt::EditRole && checkIndex(index))
    return setData(index.row(), index.column(), value, true);
  return false;
}

bool QElementsTable::setData(int field, int col, const QVariant& value, bool user) {
  if (field > (int)fields_.size())
    return false;
  if (col > (int)cols_.size())
    return false;
  data_[field][col] = QString::asprintf("0x%04x", parseData(value.toString()));
  mod_rows_[field] = user;
  return true;
}

std::map<int, std::vector<QVariant> > QElementsTable::modifiedData() const {
  std::map<int, std::vector<QVariant> > out;
  for (const auto& row : mod_rows_) {
    if (row.second == false)
      continue;
    for (const auto& dat : data_[row.first])
      out[row.first].emplace_back(parseData(dat));
  }
  return out;
}

Qt::ItemFlags QElementsTable::flags(const QModelIndex& index) const {
  return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}
