/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QComboBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QTreeWidgetItem>

#include "srscontrol/BulkConfigTab.h"
#include "srscontrol/FecControl.h"
#include "srscontrol/QElementsTable.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_BulkConfigTab.h"

BulkConfigTab::BulkConfigTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::BulkConfigTab), main_(main) {
  ui_->setupUi(this);
  connect(ui_->bulk_button_addfile, SIGNAL(clicked()), this, SLOT(addFileClicked()));
  connect(ui_->bulk_button_new, SIGNAL(clicked()), this, SLOT(newCommandClicked()));
  ui_->bulk_button_clear->setEnabled(false);
  connect(ui_->bulk_button_clear, SIGNAL(clicked()), this, SLOT(clear()));
  ui_->bulk_button_config->setEnabled(false);
  connect(ui_->bulk_button_config, SIGNAL(clicked()), this, SLOT(push()));

  ui_->bulk_tree->setHeaderLabels(QStringList() << tr("Parameter") << tr("Value"));
}

BulkConfigTab::~BulkConfigTab() {}

void BulkConfigTab::addFileClicked() {
  ui_->bulk_tree->setEnabled(true);
  ui_->bulk_button_config->setEnabled(true);
  auto files = QFileDialog::getOpenFileNames(
      this, tr("Select configuration file(s)"), ".", tr("Text files (*.txt);;All files (*.*)"));
  for (const auto& filename : files) {
    newConfig(filename);
    emit statusEvent(tr("File \"") + filename + tr("\" added to the bulk configuration!"));
  }
}

void BulkConfigTab::newCommandClicked() {
  ui_->bulk_tree->setEnabled(true);
  ui_->bulk_button_config->setEnabled(true);
  newConfig();
  emit statusEvent("New configuration added to the bulk!");
}

void BulkConfigTab::newConfig(QString filename) {
  std::string ip = "";
  srs::port_t port = 0;
  srs::Messenger::MessageData data;
  srs::words_t cmds;
  if (!filename.isEmpty())
    cmds = srs::Messenger::parseCommands(filename.toStdString(), ip, port, &data);
  auto out = new QTreeWidgetItem(ui_->bulk_tree);
  out->setText(0, tr("File"));
  out->setText(1, filename);
  // UDP port
  auto i_port = new QTreeWidgetItem;
  i_port->setText(0, tr("Port"));
  i_port->setText(1, QString::number(port));
  i_port->setFlags(i_port->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
  out->insertChild(F_PORT, i_port);
  // IP address
  auto i_ipaddr = new QTreeWidgetItem;
  i_ipaddr->setText(0, tr("IP address"));
  i_ipaddr->setText(1, QString::fromStdString(ip));
  i_ipaddr->setFlags(i_ipaddr->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
  out->insertChild(F_IP, i_ipaddr);
  // configuration fragment type
  auto i_sccmd = new QTreeWidgetItem;
  i_sccmd->setText(0, tr("Slow control command type"));
  out->insertChild(F_TYPE, i_sccmd);
  auto combo = new QComboBox(this);
  std::ostringstream sc_cmd;
  sc_cmd.str("");
  sc_cmd << srs::SlowControlCommand::WRITE_PAIRS;
  combo->addItem(QString::fromStdString(sc_cmd.str()), srs::SlowControlCommand::WRITE_PAIRS);
  sc_cmd.str("");
  sc_cmd << srs::SlowControlCommand::WRITE_BURST;
  combo->addItem(QString::fromStdString(sc_cmd.str()), srs::SlowControlCommand::WRITE_BURST);
  /*sc_cmd.str(""); sc_cmd << srs::SlowControlCommand::READ_BURST;
  combo->addItem(QString::fromStdString(sc_cmd.str()), srs::SlowControlCommand::READ_BURST);
  sc_cmd.str(""); sc_cmd << srs::SlowControlCommand::READ_LIST;
  combo->addItem(QString::fromStdString(sc_cmd.str()), srs::SlowControlCommand::READ_LIST);*/
  ui_->bulk_tree->setItemWidget(i_sccmd, 1, combo);
  // sub-address
  auto i_subaddr = new QTreeWidgetItem;
  i_subaddr->setText(0, tr("Sub-address"));
  i_subaddr->setText(1, QString::asprintf("0x%08x", data.sub_address));
  i_subaddr->setFlags(i_subaddr->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
  out->insertChild(F_SUBADDR, i_subaddr);
  // command info
  auto i_cmdinfo = new QTreeWidgetItem;
  i_cmdinfo->setText(0, tr("First address (burst config. only)"));
  i_cmdinfo->setText(1, QString::asprintf("0x%08x", data.cmd_field2));
  i_cmdinfo->setFlags(i_cmdinfo->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
  out->insertChild(F_CMDINFO, i_cmdinfo);
  // content
  auto i_cmds = new QTreeWidgetItem;
  i_cmds->setText(0, tr("Registers"));
  if (!data.data_pairs.empty())
    for (const auto& pair : data.data_pairs) {
      auto i_cmd = new QTreeWidgetItem;
      i_cmd->setText(0, QString::asprintf("0x%08x", pair.first));
      i_cmd->setText(1, QString::asprintf("0x%08x", pair.second));
      i_cmd->setFlags(i_cmd->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
      i_cmds->addChild(i_cmd);
    }
  else if (!cmds.empty())
    for (const auto& cmd : cmds) {
      auto i_cmd = new QTreeWidgetItem;
      i_cmd->setText(0, QString::asprintf("0x%08x", cmd));
      i_cmd->setFlags(i_cmd->flags() | Qt::ItemIsEditable | Qt::ItemIsEnabled);
      i_cmds->addChild(i_cmd);
    }
  out->insertChild(F_WORDS, i_cmds);
  // add/remove buttons
  auto h_buttons = new QWidget(ui_->bulk_tree);
  auto lay_buttons = new QHBoxLayout;
  lay_buttons->setMargin(0);
  auto but_add = new QPushButton("+", ui_->bulk_tree);
  connect(but_add, SIGNAL(clicked()), this, SLOT(addConfig()));
  lay_buttons->addWidget(but_add);
  auto but_del = new QPushButton("-", ui_->bulk_tree);
  connect(but_del, SIGNAL(clicked()), this, SLOT(deleteLastConfig()));
  lay_buttons->addWidget(but_del);
  h_buttons->setLayout(lay_buttons);
  ui_->bulk_tree->setItemWidget(i_cmds, 1, h_buttons);
  h_buttons->setFixedWidth(45);
  // set it to expanded
  out->setExpanded(true);
  ui_->bulk_tree->addTopLevelItem(out);
  // tweak for button to hold its parent configuration
  but_add->setProperty("child", ui_->bulk_tree->indexOfTopLevelItem(out));
  but_del->setProperty("child", ui_->bulk_tree->indexOfTopLevelItem(out));
  ui_->bulk_button_clear->setEnabled(true);
}

void BulkConfigTab::addConfig() {
  // retrieve the button's custom property and its associated parent configuration
  auto item = ui_->bulk_tree->topLevelItem(sender()->property("child").toUInt())->child(F_WORDS);
  auto i_cmd = new QTreeWidgetItem;
  i_cmd->setText(0, QString::asprintf("0x%08x", 0));
  i_cmd->setText(1, QString::asprintf("0x%08x", 0));
  i_cmd->setFlags(i_cmd->flags() | Qt::ItemIsEditable);
  item->addChild(i_cmd);
}

void BulkConfigTab::deleteLastConfig() {
  // retrieve the button's custom property and its associated parent configuration
  auto item = ui_->bulk_tree->topLevelItem(sender()->property("child").toUInt())->child(F_WORDS);
  if (item->childCount() > 0)
    item->removeChild(item->child(item->childCount() - 1));
}

void BulkConfigTab::clear() {
  ui_->bulk_tree->clear();
  ui_->bulk_tree->setEnabled(false);
  ui_->bulk_button_clear->setEnabled(false);
  ui_->bulk_button_config->setEnabled(false);
  emit statusEvent(tr("Bulk configuration cleared."));
}

void BulkConfigTab::push() {
  ui_->bulk_button_clear->setEnabled(false);
  ui_->bulk_button_config->setEnabled(false);
  for (int i = 0; i < ui_->bulk_tree->topLevelItemCount(); ++i) {
    // unit is a configuration sequence (or file)
    auto file = ui_->bulk_tree->topLevelItem(i);
    const auto i_type = dynamic_cast<QComboBox*>(ui_->bulk_tree->itemWidget(file->child(F_TYPE), 1));
    if (!i_type) {
      SRS_ERROR("BulkConfigTab") << "Failed to retrieve combo box for item " << i << ".";
      continue;
    }
    const auto type = (srs::SlowControlCommand)i_type->currentData().toUInt();
    const auto port = QElementsTable::parseData(file->child(F_PORT)->text(1));
    //const auto ip = file->child(F_IP)->text(1);
    const auto sub_addr = QElementsTable::parseData(file->child(F_SUBADDR)->text(1));
    const auto i_words = file->child(F_WORDS);
    switch (type) {
      // split into the two writing modes
      case srs::SlowControlCommand::WRITE_BURST: {
        srs::words_t words;
        const auto first_addr = QElementsTable::parseData(file->child(F_CMDINFO)->text(1));
        for (int j = 0; j < i_words->childCount(); ++j)
          // loop over all the configuration words
          words.emplace_back(QElementsTable::parseData(i_words->child(j)->text(1)));
        if (!main_->srs()->messenger().writeBurst(port, sub_addr, first_addr, words)) {
          QMessageBox box;
          box.critical(0, "Error", QString::asprintf("Failed to submit the message %d to port %d", i, port));
          box.setFixedSize(500, 200);
          break;
        }
      } break;
      case srs::SlowControlCommand::WRITE_PAIRS: {
        srs::wordspairs_t pairs;
        for (int j = 0; j < i_words->childCount(); ++j)
          pairs.emplace_back(std::make_pair(QElementsTable::parseData(i_words->child(j)->text(0)),
                                            QElementsTable::parseData(i_words->child(j)->text(1))));
        if (!main_->srs()->messenger().writePairs(port, sub_addr, pairs)) {
          QMessageBox box;
          box.critical(0, "Error", QString::asprintf("Failed to submit the message %d to port %d", i, port));
          box.setFixedSize(500, 200);
          break;
        }
      } break;
      default: {
        QMessageBox box;
        box.critical(0, "Error", QString::asprintf("Item %d has an invalid mode: %d", i, type));
        box.setFixedSize(500, 200);
      } break;
    }
  }
  emit statusEvent(tr("Bulk configuration successfully pushed onto the SRS"));
  ui_->bulk_button_clear->setEnabled(true);
  ui_->bulk_button_config->setEnabled(true);
}
