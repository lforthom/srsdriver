/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QComboBox>
#include <QDateTime>
#include <QTableView>

#include "srscontrol/Logging.h"
#include "srscontrol/QElementsTable.h"
#include "srsutils/Logging.h"
#include "ui_Logging.h"

//#define SRSCONTROL_LOGGER 1

Logging::Logging(QWidget* parent)
    : QDialog(parent), ui_(new Ui::Logging), log_stream_(this), out_log_("srscontrol.log", std::ios::app) {
  ui_->setupUi(this);
  out_log_ << "\n<<<<< New logging session on " << QDateTime::currentDateTime().toString().toStdString()
           << " >>>>>\n\n";
  // close/reset buttons
  connect(ui_->but_close, SIGNAL(clicked()), this, SLOT(close()));
  connect(ui_->but_reset, SIGNAL(clicked()), this, SLOT(clearLog()));

  for (auto i = (int)srs::Logger::Level::nothing; i <= (int)srs::Logger::Level::debug; ++i) {
    std::ostringstream lev_info;
    lev_info << (srs::Logger::Level)i;
    ui_->sel_logging_level->addItem(QString::fromStdString(lev_info.str()), QVariant::fromValue<int>(i));
  }
  table_log_ = ui_->logger;
  table_log_info_.reset(new QElementsTable({}, {"Date", "Message"}, table_log_));
  table_log_->setModel(table_log_info_.get());
  table_log_->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

  // connect the SRS logger to the interface in this widget
#ifdef SRSCONTROL_LOGGER
  srs::Logger::get().setOutput(&log_stream_, false);
  srs::Logger::get().setPretty(false);
#endif

  // set initial logging level and connect to the toggler
  const auto level = srs::Logger::Level::information;
  ui_->sel_logging_level->setCurrentIndex((int)level);
  srs::Logger::get().setLevel(level);
  connect(ui_->sel_logging_level, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLoggingLevel()));
}

Logging::~Logging() {}

void Logging::changeLoggingLevel() {
  srs::Logger::get().setLevel((srs::Logger::Level)ui_->sel_logging_level->currentIndex());
}

void Logging::clearLog() { table_log_info_->clear(true); }

void Logging::log(const QString& message) {
  auto msg_parsed(message);
  msg_parsed.replace(QString("\n\t"), " → ");
  const auto now = QDateTime::currentDateTime().toString();
  table_log_info_->append({now, msg_parsed});
  table_log_->scrollToBottom();
  out_log_ << "[" << now.toStdString() << "] " << msg_parsed.toStdString() << "\n";
}

int Logging::LogBuffer::Logger::sync() {
  int ret = std::stringbuf::sync();
  // write the current buffer to the log widget
  log_->log(QString::fromStdString(str()).trimmed());
  str("");  // reset the buffer
  return ret;
}
