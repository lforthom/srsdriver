/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QMessageBox>
#include <bitset>

#include "srscontrol/ApvRegisterTab.h"
#include "srscontrol/FecControl.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_ApvRegisterTab.h"

using namespace std::string_literals;

ApvRegisterTab::ApvRegisterTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::ApvRegisterTab), main_(main), apv_ch_sel_(NUM_CH) {
  ui_->setupUi(this);
  connect(ui_->apv_button_refresh, SIGNAL(clicked()), this, SLOT(refresh()));
  ui_->apv_button_push->setEnabled(false);
  connect(ui_->apv_button_push, SIGNAL(clicked()), this, SLOT(push()));

  // channels selector widget
  for (size_t i = 0; i < NUM_CH; ++i) {
    apv_ch_sel_[i] = this->findChild<QCheckBox*>(QString::asprintf("apv_ch%zu_sel", i));
    apv_ch_sel_[i]->setEnabled(true);
  }
  ui_->apv_chall_sel->setEnabled(true);
  ui_->apv_chall_sel->setCurrentIndex((int)NUM_CH);
  connect(ui_->apv_chall_sel, SIGNAL(currentIndexChanged(int)), this, SLOT(channelSelected(int)));

  //--- human-readable parameters

  //--- tables
  table_apv_ = parent->findChild<QTableView*>("table_apv");
  std::vector<QString> labels_apv;
  for (const auto& lab : srs::ApvRegister::fieldNames) {
    lut_apv_[lab.first] = labels_apv.size();
    labels_apv.emplace_back(QString::fromStdString(lab.second));
  }
  table_apv_info_.reset(new QElementsTable(labels_apv, {""}, table_apv_));
  table_apv_->setModel(table_apv_info_.get());
  table_apv_->setEnabled(false);
}

ApvRegisterTab::~ApvRegisterTab() {}

void ApvRegisterTab::refresh() {
  // fetch APV register from slow control
  if (main_->srs())
    try {
      apv_ = main_->srs()->readApvRegister(readApvChannels());
    } catch (const srs::LogMessage& err) {
      QMessageBox box;
      box.critical(0, "Error", ("Failed to update the APV register!\nError: " + err.message()).data());
      box.setFixedSize(500, 200);
      return;
    }

  // "generic" human-friendly part
  ui_->apv_apv_error->setChecked(apv_.error());
  const auto& apv_mode = apv_.mode();
  ui_->apv_apv_mode_anbias->setChecked(apv_mode.analogueBias());
  ui_->apv_apv_mode_trgmode->setCurrentIndex((int)apv_mode.triggerMode());
  ui_->apv_apv_mode_calibinh->setChecked(apv_mode.calibrationInhibit());
  ui_->apv_apv_mode_romode->setCurrentIndex((int)apv_mode.readoutMode());
  ui_->apv_apv_mode_rofreq->setCurrentIndex((int)apv_mode.readoutFrequency());
  ui_->apv_apv_mode_preamppol->setCurrentIndex((int)apv_mode.preampPolarity());
  ui_->apv_apv_latency->setValue(apv_.latency());
  ui_->apv_apv_muxgain->setValue(apv_.muxGain());
  ui_->apv_apv_ipre->setValue(apv_.iPre());
  ui_->apv_apv_ipcasc->setValue(apv_.iPcasc());
  ui_->apv_apv_ipsf->setValue(apv_.iPsf());
  ui_->apv_apv_isha->setValue(apv_.iSha());
  ui_->apv_apv_issf->setValue(apv_.iSsf());
  ui_->apv_apv_ipsp->setValue(apv_.iPsp());
  ui_->apv_apv_imuxin->setValue(apv_.iMuxin());
  ui_->apv_apv_ical->setValue(apv_.iCal());
  ui_->apv_apv_vpsp->setValue(apv_.vPsp());
  ui_->apv_apv_vfs->setValue(apv_.vFs());
  ui_->apv_apv_vfp->setValue(apv_.vFp());
  ui_->apv_apv_cdrv->setValue(apv_.cDrv());
  ui_->apv_apv_csel->setValue(apv_.cSel());

  // "advanced" table part
  table_apv_->setEnabled(true);
  for (size_t i = 0; i < apv_.size(); ++i) {
    if (lut_apv_.count(i))
      table_apv_info_->setData(lut_apv_.at(i), 0, static_cast<int>(apv_.value(i)), false);
  }
  table_apv_info_->layoutChanged();

  // only enable the "send" button if register first fetched
  ui_->apv_button_push->setEnabled(true);
  emit statusEvent("APV register pulled from SRS");
}

void ApvRegisterTab::channelSelected(int ch) {
  if (ch == NUM_CH)  // custom pattern selected
    for (auto* sel : apv_ch_sel_)
      sel->setEnabled(true);
  else
    for (size_t i = 0; i < apv_ch_sel_.size(); ++i) {
      apv_ch_sel_[i]->setEnabled(false);
      apv_ch_sel_[i]->setCheckState(i == (size_t)ch ? Qt::Checked : Qt::Unchecked);
    }
}

srs::ApvAddress ApvRegisterTab::readApvChannels() const {
  std::bitset<NUM_CH> poi(0);  // pattern of inhibit
  for (size_t i = 0; i < apv_ch_sel_.size(); ++i)
    poi[i] = apv_ch_sel_.at(i)->isChecked();
  return srs::ApvAddress(poi.to_ulong(), static_cast<srs::ApvAddress::Mode>(ui_->apv_devices_sel->currentIndex() + 1));
}

void ApvRegisterTab::push() {
  ui_->apv_button_refresh->setEnabled(false);
  ui_->apv_button_push->setEnabled(false);
  // retrieve the list of channels to modify and upload the configuration
  auto apv_ch = readApvChannels();
  // APV words
  try {
    auto& mode = apv_.mode();
    mode.setAnalogueBias(ui_->apv_apv_mode_anbias->isChecked());
    mode.setTriggerMode(static_cast<srs::ApvRegister::ApvMode::TriggerMode>(ui_->apv_apv_mode_trgmode->currentIndex()));
    mode.setCalibrationInhibit(ui_->apv_apv_mode_calibinh->isChecked());
    mode.setReadoutMode(static_cast<srs::ApvRegister::ApvMode::ReadoutMode>(ui_->apv_apv_mode_romode->currentIndex()));
    mode.setReadoutFrequency(
        static_cast<srs::ApvRegister::ApvMode::ReadoutFrequency>(ui_->apv_apv_mode_rofreq->currentIndex()));
    mode.setPreampPolarity(
        static_cast<srs::ApvRegister::ApvMode::PreampPolarity>(ui_->apv_apv_mode_preamppol->currentIndex()));
    apv_.setMode(mode);
    apv_.setLatency(ui_->apv_apv_latency->value());
    apv_.setMuxGain(ui_->apv_apv_muxgain->value());
    apv_.setIPre(ui_->apv_apv_ipre->value());
    apv_.setIPcasc(ui_->apv_apv_ipcasc->value());
    apv_.setIPsf(ui_->apv_apv_ipsf->value());
    apv_.setISha(ui_->apv_apv_isha->value());
    apv_.setISsf(ui_->apv_apv_issf->value());
    apv_.setIPsp(ui_->apv_apv_ipsp->value());
    apv_.setIMuxin(ui_->apv_apv_imuxin->value());
    apv_.setICal(ui_->apv_apv_ical->value());
    apv_.setVPsp(ui_->apv_apv_vpsp->value());
    apv_.setVFs(ui_->apv_apv_vfs->value());
    apv_.setVFp(ui_->apv_apv_vfp->value());
    apv_.setCDrv(ui_->apv_apv_cdrv->value());
    if (!main_->srs()->messenger().writeConfig(srs::P_APV, apv_ch, apv_))
      throw SRS_ERROR("ApvRegisterTab:push") << "Communication error.";
  } catch (const srs::LogMessage& err) {
    QMessageBox box;
    box.critical(0, "Error", ("Failed to update the APV register!\nError: "s + err.message()).data());
    box.setFixedSize(500, 200);
  }
  emit statusEvent("APV register " + QString::number(apv_ch) + " pushed onto the SRS");
  ui_->apv_button_refresh->setEnabled(true);
  ui_->apv_button_push->setEnabled(true);
}
