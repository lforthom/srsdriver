/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPainter>
#include <QTimer>

#include "srscontrol/LedIndicator.h"

LedIndicator::LedIndicator(QWidget* parent)
    : QPushButton(parent),
      lit_(IDLE),
      ledOnColor_(Qt::green),
      ledIdleColor_(Qt::yellow),
      ledErrorColor_(Qt::red),
      /*ledOnPattern_(Qt::SolidPattern),
  ledIdlePattern_(Qt::SolidPattern),
  ledErrorPattern_(Qt::SolidPattern),*/
      ledSize_(15) {
  setEnabled(false);
  setLedSize(ledSize_);
  setState(State::IDLE);
}

void LedIndicator::paintEvent(QPaintEvent* ev) {
  QColor col;
  switch (lit_) {
    case OK:
      col = ledOnColor_;
      break;
    case IDLE:
    default:
      col = ledIdleColor_;
      break;
    case ERROR:
      col = ledErrorColor_;
      break;
  }
  QPalette pal = palette();
  pal.setColor(QPalette::Button, col);
  setPalette(pal);
  update();
  QPushButton::paintEvent(ev);
}

void LedIndicator::switchLedIndicator() {
  lit_ = (State)(-(short)lit_);
  repaint();
}

void LedIndicator::flashLedIndicator() {
  const auto prev = lit_;
  QTimer::singleShot(500, [this]() { setState(IDLE); });
  setState(prev);
}

void LedIndicator::setState(State state) {
  lit_ = state;
  repaint();
}

void LedIndicator::setOnColor(QColor col) {
  ledOnColor_ = col;
  repaint();
}

void LedIndicator::setIdleColor(QColor col) {
  ledIdleColor_ = col;
  repaint();
}

void LedIndicator::setErrorColor(QColor col) {
  ledErrorColor_ = col;
  repaint();
}

void LedIndicator::setLedSize(int size) {
  ledSize_ = size;
  setFixedSize(size + 5, size + 5);
  repaint();
}
