/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "srscontrol/QPinger.h"

QPinger::QPinger(
    QString ipaddr, CallbackFunction cbSuccess, CallbackFunction cbError, std::chrono::seconds period, int max_ping)
    : cb_success_(cbSuccess), cb_error_(cbError) {
  proc_.reset(new QProcess());
#if defined(WIN32)
  args_ << "-n" << QString::asprintf("%d", max_ping) << "-q";
#else
  args_ << "-W"
        << "1"
        << "-c" << QString::asprintf("%d", max_ping);
#endif
  args_ << ipaddr;
  qDebug() << args_;
  tmr_.start(period);
  connect(&tmr_, SIGNAL(timeout()), this, SLOT(startPinging()));
}

void QPinger::startPinging() {
  proc_->start("ping", args_, QIODevice::ReadOnly);
  proc_->waitForStarted();
  proc_->waitForFinished(2000);
  if (proc_->exitCode() != 0)
    cb_error_();
  else
    cb_success_();
}
