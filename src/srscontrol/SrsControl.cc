/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QMessageBox>
#include <QSplitter>
#include <QThread>
#include <QTime>
#include <QTreeWidget>
#include <QVBoxLayout>

#include "srscontrol/AboutDialog.h"
#include "srscontrol/FecControl.h"
#include "srscontrol/FecReadout.h"
#include "srscontrol/LedIndicator.h"
#include "srscontrol/SrsControl.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_SrsControl.h"

SrsControl::SrsControl(QWidget* parent)
    : QMainWindow(parent),
      ui_(new Ui::SrsControl),
      ui_log_(new Logging(nullptr)),
      ui_readout_(new FecReadout(nullptr)) {
  ui_->setupUi(this);
  tabs_fecs_ = ui_->tabs_fecs;
  //tabs_fecs_->setTabShape(QTabWidget::Triangular);
  //tabs_fecs_->setLayout(new QVBoxLayout);
  //tabs_layout_->setMargin(0);
  //tabs_layout_->setSizeConstraint(QGridLayout::SizeConstraint::SetDefaultConstraint);
  //tabs_fecs_->widget(0)->setFixedHeight()
  tree_fecs_ = ui_->status_fecs_tree;
  tree_fecs_->setHeaderLabels(QStringList() << tr("Status") << tr("R/O?") << tr("IP address") << tr("Last update")
                                            << tr("Last message"));
  connect(tree_fecs_,
          SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)),
          this,
          SLOT(itemDoubleClicked(QTreeWidgetItem*, int)));

  connect(ui_->actionAbout_SRS_control, SIGNAL(triggered()), this, SLOT(openAboutDialog()));
  connect(ui_->but_logging_panel, SIGNAL(clicked()), this, SLOT(launchLoggingPanel()));
  connect(ui_->but_readout_panel, SIGNAL(clicked()), this, SLOT(launchReadoutPanel()));
  connect(ui_->but_srs_connect, SIGNAL(clicked()), this, SLOT(addFec()));
  connect(ui_->tabs_fecs, SIGNAL(tabCloseRequested(int)), this, SLOT(removeFec(int)));
  statusBar()->showMessage(tr("Ready"));
}

SrsControl::~SrsControl() {}

void SrsControl::openAboutDialog() {
  auto about = new AboutDialog(nullptr);
  about->show();
}

void SrsControl::itemDoubleClicked(QTreeWidgetItem* it, int) {
  tabs_fecs_->setCurrentIndex(1 + tree_fecs_->indexOfTopLevelItem(it));
}

void SrsControl::launchLoggingPanel() { ui_log_->show(); }

void SrsControl::launchReadoutPanel() {
  if (ui_readout_)
    //connect(tab_readout_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));
    ui_readout_->show();
}

void SrsControl::addFec() {
  // once connection established, read initial values from SRS
  int fec_id = 0;
  for (; fecs_.count(fec_id) != 0; ++fec_id) {
  }
  const auto srs_name = QString::asprintf("FEC%d", fec_id);
  auto* widget = new QWidget(this);
  auto* layout = new QVBoxLayout(widget);
  auto* splitter = new QSplitter(widget);
  layout->addWidget(splitter);
  const auto& srs_ip = ui_->srs_ip->text();
  try {
    fecs_[fec_id].reset(new FecControl(splitter, this, srs_name, srs_ip));
  } catch (const srs::LogMessage& msg) {
    QMessageBox box;
    box.critical(0, "Error", QString::fromStdString(msg.message()));
    box.setFixedSize(500, 200);
    return;
  }

  /*widget->setLayout(new QVBoxLayout);
  widget->layout()->setContentsMargins(0, 0, 0, 0);
  //widget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
  widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);*/
  //widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
  //widget->adjustSize();
  //widget->setAutoFillBackground(true);
  //ui_->tabs_selector->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
  tabs_fecs_->insertTab(fec_id + 1, widget, srs_name + " " + srs_ip);

  auto* fec_item = new QTreeWidgetItem;
  tree_fecs_->addTopLevelItem(fec_item);
  // status LED indicator
  auto* fec_led = new LedIndicator(tree_fecs_);
  fecs_[fec_id]->setLedIndicator(fec_led);
  fec_led->setState(LedIndicator::IDLE);
  tree_fecs_->setItemWidget(fec_item, 0, fec_led);
  // readout tickbox
  auto* fec_ro = new QCheckBox(tree_fecs_);
  fec_ro->setChecked(false);
  fec_ro->setEnabled(false);  // disable user interaction for now
  connect(fec_ro, SIGNAL(toggled(bool)), fecs_[fec_id].get(), SLOT(switchReadout(bool)));
  fecs_[fec_id]->setROEnabledBox(fec_ro);
  tree_fecs_->setItemWidget(fec_item, 1, fec_ro);
  fec_item->setText(2, srs_ip);

  setStatusBarText(
      QString::asprintf("Added a new FEC%d at %s; all registers updated.", fec_id, srs_ip.toStdString().c_str()));
  SRS_INFO("SrsControl:enablePanels") << "New SRS FEC connected with IP: " << ui_->srs_ip->text().toStdString();

  fecs_[fec_id]->refreshAllRegisters();
  ui_readout_->addFec(srs_ip, fecs_[fec_id]->srs());
}

void SrsControl::removeFec(int tab_id) {
  if (tab_id == 0)
    return;  // skip the general tab
  if (tabs_fecs_->widget(tab_id))
    delete tabs_fecs_->widget(tab_id);
  const int fec_id = tab_id - 1;
  if (fecs_.count(fec_id) > 0) {
    ui_readout_->removeFec(fecs_.at(fec_id)->fecIp());
    QThread::msleep(500);
    fecs_[fec_id].release();
    fecs_.erase(fec_id);
  }
  if (tree_fecs_->topLevelItem(fec_id))
    delete tree_fecs_->topLevelItem(fec_id);
}

void SrsControl::setStatusBarText(const QString& str) {
  if (str.contains("[FEC")) {
    auto tokens = str.split("[FEC")[1].split("] ");
    auto fec_id = tokens[0].toInt();
    auto tree_fec = tree_fecs_->topLevelItem(fec_id);
    if (tree_fec) {
      tree_fec->setText(3, QTime::currentTime().toString());
      tree_fec->setText(4, tokens[1]);
      tree_fecs_->repaint();
    }
  }
  if (statusBar())
    statusBar()->showMessage(str, 2000);
  SRS_INFO("SrsControl:setStatusBarText") << str.toStdString();
}
