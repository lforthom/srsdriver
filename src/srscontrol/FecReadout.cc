/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPushButton>
#include <future>

#include "srscontrol/FecControl.h"
#include "srscontrol/FecReadout.h"
#include "srscontrol/WaveformCollector.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_FecReadout.h"

QT_CHARTS_USE_NAMESPACE

using namespace std::string_literals;

FecReadout::FecReadout(QWidget* parent) : QDialog(parent), ui_(new Ui::FecReadout), wave_coll_(new WaveformCollector) {
  ui_->setupUi(this);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  setAutoFillBackground(true);
  connect(ui_->but_single, SIGNAL(clicked()), this, SLOT(singleReadout()));
  connect(ui_->but_start, SIGNAL(clicked()), this, SLOT(startReadout()));
  connect(ui_->but_stop, SIGNAL(clicked()), this, SLOT(stopReadout()));
  connect(ui_->unpack_raw, SIGNAL(stateChanged(int)), this, SLOT(toggleUnpacking()));
  connect(ui_->readout_freq, SIGNAL(valueChanged(double)), this, SLOT(updateFrequency()));
  connect(ui_->fec_selector, SIGNAL(currentTextChanged(const QString&)), this, SLOT(switchFec()));
  connect(ui_->channels_selector, SIGNAL(itemSelectionChanged()), this, SLOT(selectChannels()));
  connect(&tmr_, SIGNAL(timeout()), this, SLOT(update()));
  toggleUnpacking();
  //stopReadout();
  ui_->root_canvas->addWidget(wave_coll_->chartView());
}

FecReadout::~FecReadout() {}

void FecReadout::addFec(const QString& fec_addr, srs::SlowControl* sc) {
  if (map_fec_.count(fec_addr) == 0) {  // enable all buttons/UI
    ui_->but_single->setEnabled(true);
    ui_->but_start->setEnabled(true);
    ui_->unpack_raw->setEnabled(true);
    ui_->readout_freq->setEnabled(true);
    ui_->fec_selector->setEnabled(true);
  }
  map_fec_[fec_addr] = sc;
  ui_->fec_selector->addItem(fec_addr);
}

void FecReadout::removeFec(const QString& fec_addr) {
  if (map_fec_.count(fec_addr) == 0)
    return;
  if (map_fec_.at(fec_addr) == curr_fec_) {
    curr_fec_ = nullptr;
  }
  map_fec_.erase(fec_addr);
  if (auto id = ui_->fec_selector->findText(fec_addr); id >= 0)
    ui_->fec_selector->removeItem(id);
  if (map_fec_.empty()) {
    ui_->but_single->setEnabled(false);
    ui_->but_start->setEnabled(false);
    ui_->unpack_raw->setEnabled(false);
    ui_->readout_freq->setEnabled(false);
    ui_->fec_selector->setEnabled(false);
  }
}

void FecReadout::switchFec() {
  if (map_fec_.count(ui_->fec_selector->currentText()) > 0)
    curr_fec_ = map_fec_.at(ui_->fec_selector->currentText());
}

void FecReadout::prepareReadout() {
  if (!curr_fec_) {
    emit statusEvent(tr("SRS object was not properly initialised!"));
    return;
  }
  if (curr_fec_->numFec() > 0) {
    emit statusEvent(tr("A FEC was already added to the readout. Aborting."));
    return;
  }
  try {
    curr_fec_->addFec(6006);
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while adding FEC: ") + QString::fromStdString(log.message()));
  }
}

void FecReadout::toggleUnpacking() { wave_coll_->setUnpacking(ui_->unpack_raw->isChecked()); }

void FecReadout::setReadoutButtons(bool readout) {
  ui_->but_start->setEnabled(!readout);
  ui_->but_stop->setEnabled(readout);
  ui_->but_single->setEnabled(!readout);
}

void FecReadout::singleReadout() {
  if (!curr_fec_) {
    emit statusEvent(tr("SRS object was not properly initialised!"));
    return;
  }
  try {
    stopReadout();
    prepareReadout();
    curr_fec_->setReadoutEnable(true);
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while preparing single readout: ") + QString::fromStdString(log.message()));
  }
  SRS_DEBUG("FecReadout:singleReadout") << "Readout prepared for single frame acquisition. Trying now.";
  if (!update()) {
    emit statusEvent(tr("Failed to acquire one single frame!"));
    return;
  }
  try {
    curr_fec_->setReadoutEnable(false);
    stopReadout();
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while stopping single readout: ") + QString::fromStdString(log.message()));
  }
  emit statusEvent(tr("Single frame acquired."));
}

void FecReadout::startReadout() {
  if (!curr_fec_) {
    emit statusEvent(tr("SRS object was not properly initialised!"));
    return;
  }
  ui_->readout_freq->setEnabled(true);
  updateFrequency();
  try {
    prepareReadout();
    curr_fec_->setReadoutEnable(true);
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while starting continuous readout: ") + QString::fromStdString(log.message()));
  }
  emit statusEvent(tr("Readout started."));
  setReadoutButtons(true);
}

void FecReadout::stopReadout() {
  if (!curr_fec_) {
    emit statusEvent(tr("SRS object was not properly initialised!"));
    return;
  }
  ui_->readout_freq->setEnabled(false);
  tmr_.stop();
  try {
    curr_fec_->setReadoutEnable(false);
    curr_fec_->clearFecs();
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while stopping continuous readout: ") + QString::fromStdString(log.message()));
  }
  emit statusEvent(tr("Readout stopped."));
  setReadoutButtons(false);
}

bool FecReadout::update() {
  if (!curr_fec_) {
    SRS_WARNING("FecReadout:update") << "SRS object was not properly initialised!";
    return false;
  }
  const auto max_samples = curr_fec_->readApvAppRegister().ebCaptureWindow();
  if (curr_fec_->numFec() == 0) {
    SRS_DEBUG("FecReadout:update") << "No FECs added to the readout. Aborting.";
    return false;
  }
  SRS_DEBUG("FecReadout:update") << "Starting the retrieval of frames.";
  try {
    // set timeout on readout procedure to avoid keeping the thread hanging
    std::atomic_bool readout = {true};
    auto proc = std::async(&srs::SlowControl::read, curr_fec_, 0, std::ref(readout));
    if (proc.wait_for(std::chrono::milliseconds(500)) == std::future_status::timeout) {
      readout = false;
      return false;
    }
    wave_coll_->feed(proc.get(), max_samples);  // retrieve the frames once readout has succeeded
    return true;
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while retrieving frames: ") + QString::fromStdString(log.message()));
  }
  return false;
}

void FecReadout::updateFrequency() { tmr_.start(1.e3 / ui_->readout_freq->value()); }

void FecReadout::selectChannels() {
  tmr_.stop();
  std::vector<bool> channels_enable(16, false);                     // first disable all channels
  for (const auto* item : ui_->channels_selector->selectedItems())  // then enable selected channels
    channels_enable.at(ui_->channels_selector->row(item)) = true;
  wave_coll_->switchChannels(channels_enable);
  updateFrequency();
}
