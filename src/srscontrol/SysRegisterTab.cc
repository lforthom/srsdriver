/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMessageBox>

#include "srscontrol/FecControl.h"
#include "srscontrol/QElementsTable.h"
#include "srscontrol/RateMonitor.h"
#include "srscontrol/SysRegisterTab.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsdriver/SystemRegister.h"
#include "srsutils/Logging.h"
#include "ui_SysRegisterTab.h"

using namespace std::string_literals;

SysRegisterTab::SysRegisterTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::SysRegisterTab), main_(main) {
  ui_->setupUi(this);
  connect(ui_->sys_button_refresh, SIGNAL(clicked()), this, SLOT(refresh()));
  ui_->sys_button_push->setEnabled(false);
  connect(ui_->sys_button_push, SIGNAL(clicked()), this, SLOT(push()));
  connect(ui_->sys_button_reboot, SIGNAL(clicked()), this, SLOT(rebootFec()));
  connect(ui_->sys_button_warminit, SIGNAL(clicked()), this, SLOT(warmInit()));
  //
  table_dtc_info_.reset(new QElementsTable({"Padding byte", "Trailer byte"}, {""}, ui_->table_dtc_info));
  ui_->table_dtc_info->setModel(table_dtc_info_.get());

  connect(ui_->but_ratemon_panel, SIGNAL(clicked()), this, SLOT(launchRateMonitor()));
  ui_->but_ratemon_panel->setEnabled(true);
}

SysRegisterTab::~SysRegisterTab() {}

void SysRegisterTab::refresh() {
  if (main_->srs())
    try {
      sys_ = main_->srs()->readSystemRegister();
    } catch (const srs::LogMessage& err) {
      QMessageBox box;
      box.critical(0, "Error", ("Failed to update the system register!\nError: " + err.message()).data());
      box.setFixedSize(500, 200);
      return;
    }
  ui_->label_fw_version->setText(QString::asprintf("0x%04x", sys_.fwVersion()));
  ui_->label_hw_version->setText(QString::asprintf("0x%04x", sys_.hwVersion()));
  ui_->fec_ip->setText(QString::fromUtf8(sys_.fecIP().c_str()));
  ui_->daq_ip->setText(QString::fromUtf8(sys_.daqIP().c_str()));
  //
  const auto& clockst = sys_.clockStatus();
  ui_->clock_sel->setCurrentIndex((int)clockst.clockSelection());
  ui_->freq->setValue(clockst.clockFrequency());
  ui_->check_dtc0lock->setChecked(clockst.dtc0ClkLocked());
  ui_->check_dtc1lock->setChecked(clockst.dtc1ClkLocked());
  //
  const auto& clocksel = sys_.clockSelection();
  ui_->check_dtcsp->setChecked(clocksel.dtcSwapPorts());
  ui_->check_ethclk->setChecked(clocksel.ethernetClockSel());
  ui_->check_dtcti->setChecked(clocksel.dtcTriggerInhibit());
  ui_->check_dtcci->setChecked(clocksel.dtcClockInhibit());
  ui_->check_dtcsl->setChecked(clocksel.dtcSwapLanes());
  ui_->check_dtctip->setChecked(clocksel.dtcTriggerPolInvert());
  //
  const auto& dtcc = sys_.dtcConfig();
  ui_->pad_type->setCurrentIndex(dtcc.paddingType());
  ui_->trail_wrd_cnt->setValue(dtcc.trailerWordCount());
  table_dtc_info_->setData(0, 0, dtcc.paddingByte());
  table_dtc_info_->setData(1, 0, dtcc.trailerByte());
  ui_->table_dtc_info->updateGeometry();
  ui_->check_fc->setChecked(dtcc.flowControl());
  ui_->check_doe->setChecked(dtcc.dataOverEth());
  ui_->check_dcti->setChecked(dtcc.dataChannelTrgId());
  ui_->check_dfti->setChecked(dtcc.dataFrameTrgId());
  // only enable the "send" button if register first fetched
  ui_->sys_button_push->setEnabled(true);
  emit statusEvent("System registers pulled from SRS");
}

void SysRegisterTab::push() {
  if (!main_->srs()) {
    QMessageBox box;
    box.critical(0, "Error", "Failed to update the system register!");
    box.setFixedSize(500, 200);
  }
  ui_->sys_button_refresh->setEnabled(false);
  ui_->sys_button_push->setEnabled(false);
  try {
    sys_.setFecIP(ui_->fec_ip->text().toStdString());
    sys_.setDaqIP(ui_->daq_ip->text().toStdString());
    auto clk_sel = sys_.clockSelection();
    clk_sel.setDtcSwapPorts(ui_->check_dtcsp->isChecked());
    clk_sel.setEthernetClockSel(ui_->check_ethclk->isChecked());
    clk_sel.setDtcTriggerInhibit(ui_->check_dtcti->isChecked());
    clk_sel.setDtcClockInhibit(ui_->check_dtcci->isChecked());
    clk_sel.setDtcSwapLanes(ui_->check_dtcsl->isChecked());
    clk_sel.setDtcTriggerPolInvert(ui_->check_dtctip->isChecked());
    sys_.setClockSelection(clk_sel);
    auto dtc_cfg = sys_.dtcConfig();
    dtc_cfg.setPaddingType((srs::SystemRegister::DtcConfig::PaddingType)ui_->pad_type->currentIndex());
    dtc_cfg.setTrailerWordCount(ui_->trail_wrd_cnt->value());
    dtc_cfg.setPaddingByte(table_dtc_info_->data(0, 0));
    dtc_cfg.setTrailerByte(table_dtc_info_->data(1, 0));
    dtc_cfg.setFlowControl(ui_->check_fc->isChecked());
    dtc_cfg.setDataOverEth(ui_->check_doe->isChecked());
    dtc_cfg.setDataChannelTrgId(ui_->check_dcti->isChecked());
    dtc_cfg.setDataFrameTrgId(ui_->check_dfti->isChecked());
    sys_.setDtcConfig(dtc_cfg);
    if (!main_->srs()->messenger().writeConfig(srs::P_SYS, 0, sys_))
      throw SRS_ERROR("SysRegisterTab:push") << "Communication error.";
  } catch (const srs::LogMessage& err) {
    QMessageBox box;
    box.critical(0, "Error", ("Failed to update the FEC/DAQ IP!\nError: "s + err.message()).data());
    box.setFixedSize(500, 200);
  }
  // write the system configuration register
  emit statusEvent("System registers pushed onto the SRS");
  ui_->sys_button_refresh->setEnabled(true);
  ui_->sys_button_push->setEnabled(true);
}

void SysRegisterTab::launchRateMonitor() {
  auto ratemon = new RateMonitor(nullptr, main_);
  connect(ratemon, SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));
  ratemon->setAttribute(Qt::WA_DeleteOnClose);
  ratemon->show();
}

void SysRegisterTab::warmInit() {
  if (main_->srs())
    main_->srs()->warmInit();
  emit statusEvent("FEC warm reinitialisation command sent");
}

void SysRegisterTab::rebootFec() {
  if (main_->srs())
    main_->srs()->rebootFec();
  emit statusEvent("FEC reboot command sent");
}
