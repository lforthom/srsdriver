/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMessageBox>
#include <QProcess>
#include <QThread>

#include "srscontrol/AdcCardRegisterTab.h"
#include "srscontrol/ApvAppRegisterTab.h"
#include "srscontrol/BulkConfigTab.h"
#include "srscontrol/FecControl.h"
#include "srscontrol/FecReadout.h"
#include "srscontrol/LedIndicator.h"
#include "srscontrol/QPinger.h"
#include "srscontrol/SrsControl.h"
#include "srscontrol/SysRegisterTab.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_FecControl.h"

//#define SRS_PING_DISABLED
//#define SRS_COM_DISABLED

FecControl::FecControl(QWidget* parent, SrsControl* srs, QString fec_name, QString fec_ip)
    : QWidget(parent), parent_(srs), name_(std::move(fec_name)), ip_(std::move(fec_ip)), ui_(new Ui::FecControl) {
  if (ip_.isEmpty())
    throw SRS_ERROR("FecControl") << "Invalid FEC IP: " << ip_.toStdString();

  ui_->setupUi(this);

  //--- system register
  tab_system_.reset(new SysRegisterTab(ui_->tab_general, this));
  //tab_system_->setLayout(layout_);
  connect(tab_system_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

  //--- APV application register
  tab_apvapp_.reset(new ApvAppRegisterTab(ui_->tab_apvapp, this));
  //tab_apvapp_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
  //tab_apvapp_->setLayout(layout_);
  connect(tab_apvapp_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

  //--- APV register
  tab_apv_.reset(new ApvRegisterTab(ui_->tab_apv, this));
  connect(tab_apv_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

  //--- PLL register
  tab_pll_.reset(new PllRegisterTab(ui_->tab_pll, this));
  connect(tab_pll_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

  //--- ADC card register
  tab_adccard_.reset(new AdcCardRegisterTab(ui_->tab_adccard, this));
  connect(tab_adccard_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

  //--- bulk configuration
  tab_bulkconfig_.reset(new BulkConfigTab(ui_->tab_bulk, this));
  connect(tab_bulkconfig_.get(), SIGNAL(statusEvent(QString)), this, SLOT(setStatusBarText(QString)));

#ifndef SRS_COM_DISABLED
  try {
    srs_.reset(new srs::SlowControl(ip_.toStdString()));
  } catch (const srs::LogMessage& msg) {
    QMessageBox box;
    box.critical(0, "Error", QString::fromStdString(msg.message()));
    box.setFixedSize(500, 200);
    return;
  }
  togglePinger(true);
#endif

  ui_->tabs_selector->setEnabled(true);
  for (int i = 0; i < (int)Tabs::NUM_TABS; ++i)
    ui_->tabs_selector->setTabEnabled(i, true);
}

FecControl::~FecControl() { srs_->clearFecs(); }

void FecControl::refreshAllRegisters() {
  tab_system_->refresh();
  QThread::msleep(250);
  tab_apv_->refresh();
  QThread::msleep(250);
  tab_pll_->refresh();
  QThread::msleep(250);
  // needs to be called after PLL register definition
  tab_apvapp_->refresh();
  QThread::msleep(250);
  tab_adccard_->refresh();
}

void FecControl::togglePinger(bool enable) {
  if (parent_led_)
    parent_led_->flashLedIndicator();
  if (enable && !ping_)
    ping_.reset(new QPinger(
        ip_,
        [&]() {
          //setStatusBarText(tr("Ping successful"));
          if (parent_led_)
            parent_led_->setState(LedIndicator::OK);
        },
        [&]() {
          setStatusBarText(tr("Failed to PING the IP address ") + ip_);
          if (parent_led_)
            parent_led_->setState(LedIndicator::ERROR);
        }));
  else if (!enable && ping_) {
    delete ping_.release();
    if (parent_led_)
      parent_led_->setState(LedIndicator::IDLE);
  }
}

void FecControl::setStatusBarText(const QString& str) { parent_->setStatusBarText("[" + name_ + "] " + str); }

void FecControl::switchReadout(bool enabled) { tab_apvapp_->switchReadout(enabled); }

std::vector<QCheckBox*>& FecControl::apvChannelsSelectors() { return tab_apv_->apvChannelsSelectors(); }

std::vector<QCheckBox*>& FecControl::pllChannelsSelectors() { return tab_pll_->pllChannelsSelectors(); }
