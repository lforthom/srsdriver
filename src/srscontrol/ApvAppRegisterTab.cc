/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QMessageBox>
#include <QSpinBox>
#include <QTableView>
#include <iostream>

#include "srscontrol/ApvAppRegisterTab.h"
#include "srscontrol/FecControl.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_ApvAppRegisterTab.h"

using namespace std::string_literals;

ApvAppRegisterTab::ApvAppRegisterTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::ApvAppRegisterTab), main_(main) {
  ui_->setupUi(this);
  connect(ui_->apvapp_button_refresh, SIGNAL(clicked()), this, SLOT(refresh()));
  ui_->apvapp_button_push->setEnabled(false);
  connect(ui_->apvapp_button_push, SIGNAL(clicked()), this, SLOT(push()));
  connect(ui_->but_readout_enabled, SIGNAL(toggled(bool)), this, SLOT(switchReadout(bool)));
  connect(ui_->gen_trg_trgburst, &QSlider::valueChanged, this, [=]() {
    ui_->gen_trg_trgburst_display->setText(QString::number(ui_->gen_trg_trgburst->value()));
  });

  //--- human-friendly UI
  for (size_t i = 0; i < evbld_enable_ch_master_.size(); ++i) {
    evbld_enable_ch_master_[i] = this->findChild<QCheckBox*>(QString::asprintf("evbld_enable_ch%zu_master", i));
    connect(evbld_enable_ch_master_[i], SIGNAL(toggled(bool)), this, SLOT(toggleButtons()));
    evbld_enable_ch_slave_[i] = this->findChild<QCheckBox*>(QString::asprintf("evbld_enable_ch%zu_slave", i));
  }
  for (size_t i = 0; i < gen_apz_ch_sync_.size(); ++i)
    gen_apz_ch_sync_[i] = this->findChild<QCheckBox*>(QString::asprintf("apz_sync_ch%zu", i));
  for (size_t i = 0; i < gen_apz_ch_calib_.size(); ++i)
    gen_apz_ch_calib_[i] = this->findChild<QCheckBox*>(QString::asprintf("apz_calib_ch%zu", i));

  //--- table
  std::vector<QString> labels_apvapp;
  for (const auto& lab : srs::ApvAppRegister::fieldNames) {
    lut_apvapp_[lab.first] = labels_apvapp.size();
    labels_apvapp.emplace_back(QString::fromStdString(lab.second));
  }
  table_apvapp_info_.reset(new QElementsTable(labels_apvapp, {""}, ui_->table_apvapp));
  ui_->table_apvapp->setModel(table_apvapp_info_.get());
  ui_->table_apvapp->setEnabled(false);
}

ApvAppRegisterTab::~ApvAppRegisterTab() {}

void ApvAppRegisterTab::switchReadout(bool enabled) {
  main_->srs()->setReadoutEnable(enabled);
  emit statusEvent("Readout " + QString(enabled ? "enabled" : "disabled"));
}

void ApvAppRegisterTab::toggleButtons() {
  for (size_t i = 0; i < evbld_enable_ch_master_.size(); ++i) {
    evbld_enable_ch_slave_[i]->setEnabled(evbld_enable_ch_master_[i]->isChecked());
    if (!evbld_enable_ch_master_[i]->isChecked())
      evbld_enable_ch_slave_[i]->setChecked(false);
  }
}

void ApvAppRegisterTab::refresh() {
  ui_->apvapp_button_refresh->setEnabled(false);
  ui_->apvapp_button_push->setEnabled(false);
  // read the APV application register
  if (main_->srs())
    try {
      apvapp_ = main_->srs()->readApvAppRegister();
    } catch (const srs::LogMessage& err) {
      QMessageBox box;
      box.critical(0, "Error", ("Failed to update the APV application register!\nError: " + err.message()).data());
      box.setFixedSize(500, 200);
      return;
    }

  // "generic" human-friendly part
  // Trigger control
  const auto& trg_mode = apvapp_.triggerMode();
  ui_->gen_trg_seq_reset->setChecked(trg_mode.apvReset());
  ui_->gen_trg_seq_pulse->setChecked(trg_mode.apvTestPulse());
  ui_->gen_trg_seq_trgmode->setCurrentIndex((int)trg_mode.externalTrigger());
  ui_->gen_trg_seq_trgpol->setCurrentIndex((int)trg_mode.nimTriggerPolarity());
  ui_->gen_trg_trgburst->setValue(3 * (apvapp_.triggerBurst() + 1));
  ui_->gen_trg_seq_period->setValue(apvapp_.triggerSeqPeriod());
  //ui_->gen_trg_seq_period_value_->setText("...");
  ui_->gen_trg_delay->setValue(apvapp_.triggerDelay());
  ui_->gen_pulse_delay->setValue(apvapp_.testPulseDelay());
  ui_->gen_ro_delay->setValue(apvapp_.roSync());
  // APZ control
  ui_->gen_apz_nsamples->setValue(apvapp_.apzNumSamples());
  ui_->gen_apz_zs_thres_int->setValue(apvapp_.apzZeroSuppThreshold().integerThreshold());
  ui_->gen_apz_zs_thres_frac->setValue(apvapp_.apzZeroSuppThreshold().fractionalThreshold());
  for (size_t i = 0; i < gen_apz_ch_sync_.size(); ++i)
    gen_apz_ch_sync_[i]->setChecked((apvapp_.apzSyncDet() >> i) & 0x1);
  const auto& apz_params = apvapp_.apzZeroSuppParameters();
  ui_->gen_apz_peak_find->setChecked(apz_params.peakFindMode());
  ui_->gen_apz_ped_corr_disabled->setChecked(apz_params.disablePedestalCorrection());  //FIXME invert!
  ui_->gen_apz_force_signal->setChecked(apz_params.forceSignal());
  ui_->gen_apz_thr_mode->setCurrentIndex((int)apz_params.thresholdMode());
  // APZ status
  const auto& apz_status = apvapp_.apzStatus();
  ui_->gen_apz_bypass->setChecked(apz_status.apzBypassN());
  ui_->gen_apz_watchdog_flag->setChecked(apz_status.watchdogFlag());
  ui_->gen_apz_phasecal_busy->setChecked(apz_status.phaseCalibBusy());
  ui_->gen_apz_enabled->setChecked(apz_status.apzEnabled());
  ui_->gen_apz_cmd_done->setChecked(apz_status.commandDone());
  ui_->gen_apz_calib_crt->setValue(apz_status.calibAllCurrent());
  ui_->gen_apz_calib_all_done->setChecked(apz_status.calibAllDone());
  ui_->gen_apz_pedcal_busy->setChecked(apz_status.pedestalCalibBusy());
  ui_->gen_apz_phase_aligned->setChecked(apz_status.phaseAligned());
  for (size_t i = 0; i < gen_apz_ch_calib_.size(); ++i)
    gen_apz_ch_calib_[i]->setChecked((apz_status.apzChannelStatus() >> i) & 0x1);

  const auto& ch_enable(apvapp_.ebChannelEnableMask());
  for (size_t i = 0; i < ch_enable.size(); ++i) {
    if (main_->apvChannelsSelectors().empty() || i >= main_->apvChannelsSelectors().size()) {
      SRS_ERROR("ApvAppRegisterTab:refresh")
          << ">>> channel " << i << " has invalid state: " << apvapp_.ebChannelEnableMask().mode(i) << ".";
      continue;
    }
    auto *apv_sel = main_->apvChannelsSelectors().at(i), *pll_sel = main_->pllChannelsSelectors().at(i);
    switch (ch_enable.mode(i)) {
      case srs::ChannelEnable::Mode::DISABLED:
        evbld_enable_ch_master_[i]->setCheckState(Qt::CheckState::Unchecked);
        evbld_enable_ch_slave_[i]->setCheckState(Qt::CheckState::Unchecked);
        apv_sel->setChecked(false);
        pll_sel->setChecked(false);
        break;
      case srs::ChannelEnable::Mode::MASTER:
        evbld_enable_ch_master_[i]->setCheckState(Qt::CheckState::Checked);
        evbld_enable_ch_slave_[i]->setCheckState(Qt::CheckState::Unchecked);
        apv_sel->setChecked(true);
        pll_sel->setChecked(true);
        break;
      case srs::ChannelEnable::Mode::ALL_APVS:
        evbld_enable_ch_master_[i]->setCheckState(Qt::CheckState::Checked);
        evbld_enable_ch_slave_[i]->setCheckState(Qt::CheckState::Checked);
        apv_sel->setChecked(true);
        pll_sel->setChecked(true);
        break;
      default:
        SRS_ERROR("ApvAppRegisterTab:refresh")
            << ">>> channel " << i << " has invalid state: " << apvapp_.ebChannelEnableMask().mode(i) << ".";
        break;
    }
  }
  ui_->evbld_data_length->setValue(apvapp_.ebCaptureWindow());
  ui_->evbld_mode->setCurrentIndex((int)apvapp_.ebMode());
  ui_->evbld_evt_info_type->setValue(apvapp_.ebDataFormat());
  const auto& eid = apvapp_.ebEventInfoData();
  ui_->evbld_evt_info_header_field->setCurrentIndex((int)eid.headerInfo());
  ui_->evbld_evt_info_runlabel->setValue(eid.runLabel());

  // "advanced" table part
  ui_->table_apvapp->setEnabled(true);
  for (size_t i = 0; i < apvapp_.size(); ++i) {
    if (lut_apvapp_.count(i) > 0)
      table_apvapp_info_->setData(lut_apvapp_.at(i), 0, (int)apvapp_.value(i), false);
  }
  table_apvapp_info_->layoutChanged();

  // only enable the "send" button if register first fetched
  emit statusEvent("APV application register pulled from SRS");
  ui_->apvapp_button_refresh->setEnabled(true);
  ui_->apvapp_button_push->setEnabled(true);
  ui_->but_readout_enabled->setEnabled(true);
  if (main_->srs()) {
    const auto ro_enabled = main_->srs()->readoutEnabled();
    ui_->but_readout_enabled->setChecked(ro_enabled);
    main_->roEnabledBox()->setEnabled(true);
    main_->roEnabledBox()->setChecked(ro_enabled);
  }
}

void ApvAppRegisterTab::push() {
  if (!main_->srs()) {
    QMessageBox box;
    box.critical(0, "Error", "Failed to update the APV application register!");
    box.setFixedSize(500, 200);
  }
  // Trigger control
  try {
    auto trg = apvapp_.triggerMode();
    trg.setApvReset(ui_->gen_trg_seq_reset->isChecked());
    trg.setApvTestPulse(ui_->gen_trg_seq_pulse->isChecked());
    trg.setExternalTrigger((bool)ui_->gen_trg_seq_trgmode->currentIndex());
    trg.setNimTriggerPolarity((bool)ui_->gen_trg_seq_trgpol->currentIndex());
    apvapp_.setTriggerMode(trg);
    apvapp_.setTriggerBurst((int)(ui_->gen_trg_trgburst->value() / 3) - 1);
    apvapp_.setTriggerSeqPeriod(ui_->gen_trg_seq_period->value());
    apvapp_.setTriggerDelay(ui_->gen_trg_delay->value());
    apvapp_.setTestPulseDelay(ui_->gen_pulse_delay->value());
    apvapp_.setRoSync(ui_->gen_ro_delay->value());

    // APZ control
    apvapp_.setApzNumSamples(ui_->gen_apz_nsamples->value());
    auto zst = apvapp_.apzZeroSuppThreshold();
    zst.setIntegerThreshold(ui_->gen_apz_zs_thres_int->value());
    zst.setFractionalThreshold(ui_->gen_apz_zs_thres_frac->value());
    apvapp_.setApzZeroSuppThreshold(zst);
    //
    auto zsp = apvapp_.apzZeroSuppParameters();
    zsp.setPeakFindMode(ui_->gen_apz_peak_find->isChecked());
    zsp.setDisablePedestalCorrection(ui_->gen_apz_ped_corr_disabled->isChecked());
    zsp.setForceSignal(ui_->gen_apz_force_signal->isChecked());
    zsp.setThresholdMode(ui_->gen_apz_thr_mode->currentIndex());
    apvapp_.setApzZeroSuppParameters(zsp);

    auto mask = apvapp_.ebChannelEnableMask();
    for (size_t i = 0; i < evbld_enable_ch_master_.size(); ++i) {
      if (main_->apvChannelsSelectors().empty() || i >= main_->apvChannelsSelectors().size()) {
        SRS_ERROR("ApvAppRegisterTab:refresh")
            << ">>> channel " << i << " has invalid state: " << apvapp_.ebChannelEnableMask().mode(i) << ".";
        continue;
      }
      auto *apv_sel = main_->apvChannelsSelectors().at(i), *pll_sel = main_->pllChannelsSelectors().at(i);
      if (evbld_enable_ch_master_[i]->isChecked()) {
        if (evbld_enable_ch_slave_[i]->isChecked()) {
          mask.setMode(i, srs::ChannelEnable::ALL_APVS);
          apv_sel->setChecked(true);
          pll_sel->setChecked(true);
        } else {
          mask.setMode(i, srs::ChannelEnable::MASTER);
          apv_sel->setChecked(true);
          pll_sel->setChecked(true);
        }
      } else {
        mask.setMode(i, srs::ChannelEnable::DISABLED);
        apv_sel->setChecked(false);
        pll_sel->setChecked(false);
      }
    }
    apvapp_.setEbChannelEnableMask(mask);
    apvapp_.setEbCaptureWindow(ui_->evbld_data_length->value());
    apvapp_.setEbMode((srs::ApvAppRegister::EventBuilderMode)ui_->evbld_mode->currentIndex());
    apvapp_.setEbDataFormat(ui_->evbld_evt_info_type->value());
    auto info = apvapp_.ebEventInfoData();
    info.setHeaderInfo(static_cast<srs::ApvAppRegister::EbEventInfoData::HeaderInfo>(
        ui_->evbld_evt_info_header_field->currentIndex()));
    info.setRunLabel(ui_->evbld_evt_info_runlabel->value());
    apvapp_.setEbEventInfoData(info);
    if (!main_->srs()->messenger().writeConfig(srs::P_APVAPP, 0, apvapp_))
      throw SRS_ERROR("ApvAppRegisterTab:push") << "Communication error.";
  } catch (const srs::LogMessage& err) {
    QMessageBox box;
    box.critical(0, "Error", ("Failed to update the APV application register!\nError: "s + err.message()).data());
    box.setFixedSize(500, 200);
  }
  // upload the configuration
  emit statusEvent("APV application register pushed onto the SRS");
}
