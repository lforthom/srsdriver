/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QPushButton>
#include <cmath>
#include <future>

#include "srscontrol/FecControl.h"
#include "srscontrol/RateMonitor.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsreadout/SrsFrame.h"
#include "srsutils/Logging.h"
#include "ui_RateMonitor.h"

RateMonitor::RateMonitor(QWidget* parent, FecControl* main)
    : QWidget(parent),
      ui_(new Ui::RateMonitor),
      main_(main),
      period_ms_(1000.),
      readout_ms_(0.75 * period_ms_),
      norm_(1000. / readout_ms_) {
  ui_->setupUi(this);
  connect(ui_->but_close, SIGNAL(clicked()), this, SLOT(close()));
  connect(ui_->check_enable, SIGNAL(stateChanged(int)), this, SLOT(toggleReadout(int)));
  connect(&tmr_, SIGNAL(timeout()), this, SLOT(update()));
}

RateMonitor::~RateMonitor() { toggleReadout(0); }

void RateMonitor::toggleReadout(int rd) {
  if (!main_->srs())
    return;
  try {
    if (rd > 0) {
      main_->srs()->setReadoutEnable(true);
      main_->srs()->addFec(6006);
      tmr_.start(ceilf(period_ms_));
    } else {
      tmr_.stop();
      main_->srs()->setReadoutEnable(false);
      main_->srs()->clearFecs();
    }
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while toggling the readout: ") + QString::fromStdString(log.message()));
  }
}

void RateMonitor::update() {
  scalers_.clear();
  try {
    auto frames_coll = main_->srs()->readoutFor(std::chrono::milliseconds((int)readout_ms_), 0);
    scalers_.num_triggers = frames_coll.size();
    for (const auto& frames : frames_coll)
      scalers_.num_frames += frames.size();
  } catch (const srs::LogMessage& log) {
    emit statusEvent(tr("Error while retrieving the frames for the rate monitor: ") +
                     QString::fromStdString(log.message()));
    return;
  }
  ui_->lcd_trgrate->display(scalers_.num_triggers * norm_);
  ui_->lcd_frmrate->display(scalers_.num_frames * norm_);
  ui_->lcd_frmpertrig->display(scalers_.num_frames * 1. / scalers_.num_triggers);
}

void RateMonitor::Scalers::clear() { num_triggers = num_frames = 0; }

void RateMonitor::closeEvent(QCloseEvent* event) {
  toggleReadout(0);  // abort the readout before closing the window
  QWidget::closeEvent(event);
}
