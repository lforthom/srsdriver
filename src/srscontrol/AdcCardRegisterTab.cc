/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCheckBox>
#include <QMessageBox>
#include <bitset>

#include "srscontrol/AdcCardRegisterTab.h"
#include "srscontrol/FecControl.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/Logging.h"
#include "ui_AdcCardRegisterTab.h"

using namespace std::string_literals;

AdcCardRegisterTab::AdcCardRegisterTab(QWidget* parent, FecControl* main)
    : QWidget(parent), ui_(new Ui::AdcCardRegisterTab), main_(main) {
  ui_->setupUi(this);
  connect(ui_->adc_button_refresh, SIGNAL(clicked()), this, SLOT(refresh()));
  ui_->adc_button_push->setEnabled(false);
  connect(ui_->adc_button_push, SIGNAL(clicked()), this, SLOT(push()));

  // channels selector widget
  for (size_t i = 0; i < NUM_HDMI_CH; ++i) {
    adc_reset_pin_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_resetpin", i));
    adc_pwdwn0_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_pwdwn0", i));
    adc_pwdwn1_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_pwdwn1", i));
    adc_eqctrl0_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_eqctrl0", i));
    adc_eqctrl1_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_eqctrl1", i));
    adc_trgbuf_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_trgbuf", i));
    adc_bclk_[i] = this->findChild<QCheckBox*>(QString::asprintf("adc_ch%zu_bclk", i));
  }
}

AdcCardRegisterTab::~AdcCardRegisterTab() {}

void AdcCardRegisterTab::refresh() {
  //--- "generic" human-friendly part

  // fetch APV register from slow control
  if (main_->srs())
    try {
      adc_ = main_->srs()->readAdcCardRegister();
    } catch (const srs::LogMessage& err) {
      QMessageBox box;
      box.critical(0, "Error", ("Failed to update the ADC card register!\nError: " + err.message()).data());
      box.setFixedSize(500, 200);
      return;
    }
  for (size_t i = 0; i < NUM_HDMI_CH; ++i) {
    adc_reset_pin_[i]->setChecked(adc_.hybridResetPin().enabled(i));
    adc_pwdwn0_[i]->setChecked(adc_.powerDownCh0().enabled(i));
    adc_pwdwn1_[i]->setChecked(adc_.powerDownCh1().enabled(i));
    adc_eqctrl0_[i]->setChecked(adc_.eqLevel0().enabled(i));
    adc_eqctrl1_[i]->setChecked(adc_.eqLevel1().enabled(i));
    adc_trgbuf_[i]->setChecked(adc_.triggerOut().enabled(i));
    adc_bclk_[i]->setChecked(adc_.bclkBuffer().enabled(i));
  }

  // only enable the "send" button if register first fetched
  ui_->adc_button_push->setEnabled(true);
  emit statusEvent("ADC card register pulled from SRS");
}

void AdcCardRegisterTab::push() {
  if (!main_->srs()) {
    QMessageBox box;
    box.critical(0, "Error", "Failed to update the ADC card register!");
    box.setFixedSize(500, 200);
  }
  ui_->adc_button_refresh->setEnabled(false);
  ui_->adc_button_push->setEnabled(false);
  // ADC card words
  try {
    for (size_t i = 0; i < NUM_HDMI_CH; ++i) {
      adc_.hybridResetPin().setEnabled(i, adc_reset_pin_[i]->isChecked());
      adc_.powerDownCh0().setEnabled(i, adc_pwdwn0_[i]->isChecked());
      adc_.powerDownCh1().setEnabled(i, adc_pwdwn1_[i]->isChecked());
      adc_.eqLevel0().setEnabled(i, adc_eqctrl0_[i]->isChecked());
      adc_.eqLevel1().setEnabled(i, adc_eqctrl1_[i]->isChecked());
      adc_.triggerOut().setEnabled(i, adc_trgbuf_[i]->isChecked());
      adc_.bclkBuffer().setEnabled(i, adc_bclk_[i]->isChecked());
    }
    if (!main_->srs()->messenger().writeConfig(srs::P_ADCCARD, 0, adc_))
      throw SRS_ERROR("AdcCardRegisterTab:push") << "Communication error.";
  } catch (const srs::LogMessage& err) {
    QMessageBox box;
    box.critical(0, "Error", ("Failed to update the APV register!\nError: "s + err.message()).data());
    box.setFixedSize(500, 200);
  }
  emit statusEvent("ADC card register pushed onto the SRS");
  ui_->adc_button_refresh->setEnabled(true);
  ui_->adc_button_push->setEnabled(true);
}
