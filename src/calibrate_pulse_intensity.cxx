/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TGraph.h>
#include <TH1.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TTree.h>

#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/ApvAdcCalibration.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"

int main(int argc, char* argv[]) {
  int num_events;
  std::string slow_control, output_file;
  std::vector<int> ical_values;
  bool debug, save_objs, save_tree;

  srs::utils::ArgumentsParser(argc, argv)
      .addOptionalArgument("slow-control,f", "FEC address", &slow_control, "10.0.0.2")
      .addOptionalArgument("num-events,n", "number of events to collect per ICAL value", &num_events, 200)
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .addOptionalArgument(
          "ical-vals,i",
          "list of ICAL values to probe",
          &ical_values,
          std::vector<int>{10, 20, 40, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 210, 220, 240, 255})
      .addOptionalArgument("output,o", "output file name (.root)", &output_file, "calibration.pulse.root")
      .addOptionalArgument("save-objs,s", "save the intermediate objects", &save_objs, false)
      .addOptionalArgument("save-tree,t", "save the fit results tree", &save_tree, true)
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);

  srs::SlowControl sc(slow_control);
  sc.addFec(6006);

  //===== Collection/preprocessing part

  srs::utils::ApvAdcCalibration calib(sc);

  std::map<int, TGraph> m_gr_ave_maxampl;

  for (const auto& ical : ical_values) {
    const auto max_ampl = calib.run(ical, num_events);
    for (const auto& ch_ave : max_ampl)
      m_gr_ave_maxampl[ch_ave.first].SetPoint(m_gr_ave_maxampl[ch_ave.first].GetN(), ical, ch_ave.second);
  }

  //===== Analysis part

  //----- prepare the output file for results

  auto file = TFile::Open(output_file.c_str(), "recreate");

  //----- ICAL scan result

  // build the fit function for the double-linear interpolation
  TF1 fit_func("fit_func",
               "(x < [0])*([1]*x+[2])+(x >= [0])*([3]*x+([1]-[3])*[0]+[2])",
               (float)(*ical_values.begin()),  // first ICAL value tested
               200.                            // remove saturation regime
  );
  fit_func.SetParName(0, "Kink position");
  fit_func.SetParameter(0, 80.);
  fit_func.SetParLimits(0, 50., 110.);
  fit_func.SetParName(1, "Lin.term (below kink)");
  fit_func.SetParName(2, "Cst.term (below kink)");
  fit_func.SetParName(3, "Lin.term (above kink)");
  // build a tree as results container (one entry per channel)
  TTree tree_res("results", "Collection of channel-indexed fit results");
  int ch_id;
  double chi2norm;
  double kink_pos, par_lin_low, par_cst_low, par_lin_high;
  double kink_pos_unc, par_lin_low_unc, par_cst_low_unc, par_lin_high_unc;
  tree_res.Branch("ch_id", &ch_id, "ch_id/I");
  tree_res.Branch("chi2norm", &chi2norm, "chi2norm/D");
  tree_res.Branch("kink_pos", &kink_pos, "kink_pos/D");
  tree_res.Branch("kink_pos_unc", &kink_pos_unc, "kink_pos_unc/D");
  tree_res.Branch("par_lin_low", &par_lin_low, "par_lin_low/D");
  tree_res.Branch("par_lin_low_unc", &par_lin_low_unc, "par_lin_low_unc/D");
  tree_res.Branch("par_cst_low", &par_cst_low, "par_cst_low/D");
  tree_res.Branch("par_cst_low_unc", &par_cst_low_unc, "par_cst_low_unc/D");
  tree_res.Branch("par_lin_high", &par_lin_high, "par_lin_high/D");
  tree_res.Branch("par_lin_high_unc", &par_lin_high_unc, "par_lin_high_unc/D");
  {  // a bit of plotting/fitting
    TCanvas c;
    TMultiGraph mg;
    size_t i = 0;
    for (auto& ch_gr : m_gr_ave_maxampl) {
      auto& gr = ch_gr.second;
      gr.SetTitle(Form("Channel %d", ch_gr.first));
      auto fit = dynamic_cast<TF1*>(fit_func.Clone());
      if (save_objs)
        file->WriteObject(&gr, Form("calib_maxamp_vs_pulseintens_ch%d", ch_gr.first));
      auto fit_res = gr.Fit(fit, "rems");
      if (!(int)fit_res)
        SRS_WARNING("main") << "Fit did not converge for channel " << ch_gr.first << ".";
      if (save_objs)
        file->WriteObject(fit, Form("calib_maxamp_vs_pulseintens_fit_ch%d", ch_gr.first));
      chi2norm = fit_res->Chi2() / fit_res->Ndf();
      ch_id = ch_gr.first;
      kink_pos = fit->GetParameter(0);
      kink_pos_unc = fit->GetParError(0);
      par_lin_low = fit->GetParameter(1);
      par_lin_low_unc = fit->GetParError(1);
      par_cst_low = fit->GetParameter(2);
      par_cst_low_unc = fit->GetParError(2);
      par_lin_high = fit->GetParameter(3);
      par_lin_high_unc = fit->GetParError(3);
      tree_res.Fill();
      gr.SetMarkerStyle(24 + (i++));
      mg.Add(&gr);
    }
    mg.Draw("alp pmc plc");
    mg.GetXaxis()->SetTitle("ICAL (APV[0x18]) value");
    mg.GetYaxis()->SetTitle(Form("Maximum amplitude (%d frames)", num_events));
    auto leg = c.BuildLegend(0.3, 0.875, 0.95, 0.925);
    leg->SetNColumns(m_gr_ave_maxampl.size());
    c.SaveAs("maxampl_allch_vs_ical.pdf");
  }
  if (save_tree)
    file->WriteObject(&tree_res, "results");

  //----- all objects are now properly stored

  file->Close();

  return 0;
}
