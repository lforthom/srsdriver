/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_PllRegister_h
#define srsdriver_PllRegister_h

#include "srsutils/Config.h"
#include "srsutils/POI.h"

namespace srs {
  /// PLL control register (2 words only)
  struct PllRegister : public Config<2> {
    PllRegister();
    explicit PllRegister(const words_t&);

    /// List of configuration words held by this register
    enum Field { CSR1_FINEDELAY = 0x0, TRG_DELAY = 0x1 };
    /// Human-readable names of each configuration word
    static const fnames_t fieldNames;
    friend std::ostream& operator<<(std::ostream&, const Field&);

    struct Csr1FineDelay : public ConfigWord {
      using ConfigWord::ConfigWord;

      uint8_t phaseAdjust() const;                  ///< CLK fine-phase adjustment (value <= 11)
      Csr1FineDelay& setPhaseAdjust(uint8_t);       ///< Set the CLK fine-phase adjustment (value <= 11)
      bool phaseFlip() const;                       ///< CLK phase flip
      Csr1FineDelay& setPhaseFlip(bool);            ///< Set the CLK phase flip
      bool triggerDelayEnabled() const;             ///< Access to TRG_DELAY register enabled
      Csr1FineDelay& setTriggerDelayEnabled(bool);  ///< Enable access to TRG_DELAY register

      void print(std::ostream& os) const override;
    };

    Csr1FineDelay csr1FineDelay() const;
    Csr1FineDelay& csr1FineDelay();
    PllRegister& setCsr1FineDelay(const Csr1FineDelay&);
    uint16_t triggerDelay() const;           ///< Trigger delay (in clock cycles)
    PllRegister& setTriggerDelay(uint16_t);  ///< Set the trigger delay (in clock cycles)

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
