/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_AdcCardRegister_h
#define srsdriver_AdcCardRegister_h

#include "srsutils/Config.h"
#include "srsutils/POI.h"

namespace srs {
  struct AdcCardRegister : public Config<7> {
    AdcCardRegister();
    explicit AdcCardRegister(const words_t&);

    /// Specialised channels pattern of inhibit for the ADC card register
    struct ChannelPattern : public POI<1> {
      explicit ChannelPattern(uint32_t word = 0);
      ChannelPattern(const ChannelPattern&);
      ChannelPattern& operator=(const ChannelPattern&);
      void print(std::ostream& os) const override;
    };

    /// List of configuration words held by this register
    enum Field {
      HYBRID_RST_N = 0x0,
      PWRDOWN_CH0 = 0x1,
      PWRDOWN_CH1 = 0x2,
      EQ_LEVEL_0 = 0x3,
      EQ_LEVEL_1 = 0x4,
      TRGOUT_ENABLE = 0x5,
      BCLK_ENABLE = 0x6
    };
    static const fnames_t fieldNames;  ///< Human-readable names of each configuration word
    friend std::ostream& operator<<(std::ostream&, const Field&);

    ChannelPattern& hybridResetPin();                           ///< Reset pin for each HDMI channel
    ChannelPattern hybridResetPin() const;                      ///< Reset pin for each HDMI channel
    AdcCardRegister& setHybridResetPin(const ChannelPattern&);  ///< Set the reset pin for each HDMI channel

    ChannelPattern& powerDownCh0();       ///< Power-down control of analog circuitry in each HDMI channel's master path
    ChannelPattern powerDownCh0() const;  ///< Power-down control of analog circuitry in each HDMI channel's master path
    /// Set the power-down control of analog circuitry in each HDMI channel's master path
    AdcCardRegister& setPowerDownCh0(const ChannelPattern&);

    ChannelPattern& powerDownCh1();       ///< Power-down control of analog circuitry in each HDMI channel's slave path
    ChannelPattern powerDownCh1() const;  ///< Power-down control of analog circuitry in each HDMI channel's slave path
    /// Set the power-down control of analog circuitry in each HDMI channel's master path
    AdcCardRegister& setPowerDownCh1(const ChannelPattern&);

    ChannelPattern& eqLevel0();                           ///< Equalisation control (bit 0) for each HDMI channel
    ChannelPattern eqLevel0() const;                      ///< Equalisation control (bit 0) for each HDMI channel
    AdcCardRegister& setEqLevel0(const ChannelPattern&);  ///< Equalisation control (bit 0) for each HDMI channel

    ChannelPattern& eqLevel1();                           ///< Equalisation control (bit 1) for each HDMI channel
    ChannelPattern eqLevel1() const;                      ///< Equalisation control (bit 1) for each HDMI channel
    AdcCardRegister& setEqLevel1(const ChannelPattern&);  ///< Equalisation control (bit 1) for each HDMI channel

    ChannelPattern& triggerOut();                           ///< Enable TRGOUT buffer for each HDMI channel
    ChannelPattern triggerOut() const;                      ///< Enable TRGOUT buffer for each HDMI channel
    AdcCardRegister& setTriggerOut(const ChannelPattern&);  ///< Set the TRGOUT buffer for each HDMI channel

    ChannelPattern& bclkBuffer();                           ///< Enables BCLK buffer for each HDMI channel
    ChannelPattern bclkBuffer() const;                      ///< Enable BCLK buffer for each HDMI channel
    AdcCardRegister& setBclkBuffer(const ChannelPattern&);  ///< Set the BCLK buffer for each HDMI channel

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
