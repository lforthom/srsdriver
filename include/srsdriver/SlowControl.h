/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_SlowControl_h
#define srsdriver_SlowControl_h

#include <atomic>
#include <chrono>
#include <memory>

#include "srsdriver/AdcCardRegister.h"
#include "srsdriver/ApvRegister.h"
#include "srsdriver/PllRegister.h"
#include "srsdriver/Port.h"
#include "srsdriver/SystemRegister.h"
#include "srsreadout/SrsFrame.h"

namespace srs {
  class ApvAddress;
  class Messenger;
  class Receiver;
  class SlowControl {
  public:
    explicit SlowControl(const std::string&);

    SystemRegister readSystemRegister();
    ApvRegister readApvRegister(const ApvAddress&);
    PllRegister readPllRegister(const ApvAddress&);
    ApvAppRegister readApvAppRegister();
    AdcCardRegister readAdcCardRegister();

    void setDaqIP(const std::string& daq_ipaddr, port_t port);
    void addFec(port_t port);                              ///< Add a FEC receiver to this control
    inline void clearFecs() { fecs_.clear(); }             ///< Remove all FECs receivers from object
    inline size_t numFec() const { return fecs_.size(); }  ///< Number of FECs objects registered to this object

    void setReadoutEnable(bool);  ///< Enable/disable the frames readout
    bool readoutEnabled();        ///< Check if frames readout is enabled

    void setLatency(uint16_t);
    void configureAdcs();

    void rebootFec();  ///< Reboot the FPGA
    void warmInit();   ///< Initiate a warm initialisation of the card

    inline Messenger& messenger() { return *sock_; }  ///< A pointer to the socket messaging object

    SrsFrameCollection read(size_t fec_id, std::atomic_bool& running);  ///< Read the next frames from a given FEC id
    /// Launch a continuous readout of SRS frames
    void readout(std::vector<SrsFrameCollection>& out, size_t fec_id, std::atomic_bool& running, bool unpack = true);
    /// Launch a continuous readout for a given duration
    std::vector<SrsFrameCollection> readoutFor(std::chrono::milliseconds duration, size_t fec_id, bool unpack = true);

  private:
    srs::ApvAppRegister::EventBuilderMode eb_mode_;

    std::unique_ptr<Messenger> sock_;
    std::vector<std::unique_ptr<Receiver> > fecs_;
  };
}  // namespace srs

#endif
