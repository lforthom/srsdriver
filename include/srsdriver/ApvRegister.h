/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_ApvRegister_h
#define srsdriver_ApvRegister_h

#include "srsutils/Config.h"
#include "srsutils/POI.h"

namespace srs {
  struct ApvRegister : public Config<30> {
    ApvRegister();
    explicit ApvRegister(const words_t&);
    /// List of configuration words held by this register
    enum Field {
      ERROR = 0x00,
      MODE = 0x01,
      LATENCY = 0x02,
      MUXGAIN = 0x03,
      IPRE = 0x10,
      IPCASC = 0x11,
      IPSF = 0x12,
      ISHA = 0x13,
      ISSF = 0x14,
      IPSP = 0x15,
      IMUXIN = 0x16,
      ICAL = 0x18,
      VPSP = 0x19,
      VFS = 0x1a,
      VFP = 0x1b,
      CDRV = 0x1c,
      CSEL = 0x1d
    };
    static const fnames_t fieldNames;  ///< Human-readable names of each configuration word
    friend std::ostream& operator<<(std::ostream&, const Field&);

    uint16_t error() const;  ///< SEU or Sync error

    /// Operation mode register
    struct ApvMode : public ConfigWord {
      using ConfigWord::ConfigWord;

      bool analogueBias() const;       ///< Analogue bias
      ApvMode& setAnalogueBias(bool);  ///< Set the analogue bias

      /// Possible trigger operation modes (number of samples per trigger)
      enum TriggerMode {
        T_3SAMPLE = 0x0,  ///< 3 samples per trigger
        T_1SAMPLE = 0x1   ///< 1 sample per trigger
      };
      /// Human-readable description of the triggering mode
      friend std::ostream& operator<<(std::ostream&, const TriggerMode&);
      TriggerMode triggerMode() const;              ///< Trigger operation mode
      ApvMode& setTriggerMode(const TriggerMode&);  ///< Set the trigger operation mode

      bool calibrationInhibit() const;       ///< Calibration inhibit
      ApvMode& setCalibrationInhibit(bool);  ///< Set the calibration inhibit mode

      /// Types of readouts
      enum ReadoutMode { RO_DECONVOLUTION = 0x0, RO_PEAK = 0x1 };
      /// Human-readable description of the readout mode
      friend std::ostream& operator<<(std::ostream&, const ReadoutMode&);
      ReadoutMode readoutMode() const;              ///< Type of readout
      ApvMode& setReadoutMode(const ReadoutMode&);  ///< Set the readout type

      /// Handled readout frequencies
      enum ReadoutFrequency {
        F_20MHZ = 0x0,  ///< 20-MHz mode
        F_40MHZ = 0x1   ///< 40-MHz mode
      };
      /// Human-readable description of the readout frequency
      friend std::ostream& operator<<(std::ostream&, const ReadoutFrequency&);
      ReadoutFrequency readoutFrequency() const;              ///< Readout frequency used
      ApvMode& setReadoutFrequency(const ReadoutFrequency&);  ///< Set the readout frequency

      /// Possible polarity modes for the preamplifier
      enum PreampPolarity { POL_NONINV = 0x0, POL_INV = 0x1 };
      /// Human-readable description of the preamplifier polarity
      friend std::ostream& operator<<(std::ostream&, const PreampPolarity&);
      PreampPolarity preampPolarity() const;              ///< Preamplifier polarity mode
      ApvMode& setPreampPolarity(const PreampPolarity&);  ///< Set the preamplifier polarity

      void print(std::ostream& os) const override;
    };
    ApvMode mode() const;                  ///< Operation mode for the APV25
    ApvMode& mode();                       ///< Operation mode register for the APV
    ApvRegister& setMode(const ApvMode&);  ///< Set the APV operation mode register

    uint16_t latency() const;           ///< Trigger latency
    ApvRegister& setLatency(uint16_t);  ///< Set the trigger latency, in clock cycles
    uint16_t muxGain() const;           ///< Gain of the output buffer
    ApvRegister& setMuxGain(uint16_t);  ///< Set the gain of the output buffer
    uint16_t iPre() const;              ///< Preamplifier current
    ApvRegister& setIPre(uint16_t);     ///< Set the preamplifier current
    uint16_t iPcasc() const;            ///< Preamplifier cascade current
    ApvRegister& setIPcasc(uint16_t);   ///< Set the preamplifier cascade current
    uint16_t iPsf() const;              ///< Preamplifier source follower current
    ApvRegister& setIPsf(uint16_t);     ///< Set the preamplifier source follower current
    uint16_t iSha() const;              ///< Shaper current
    ApvRegister& setISha(uint16_t);     ///< Set the shaper current
    uint16_t iSsf() const;              ///< Shaper source follower current
    ApvRegister& setISsf(uint16_t);     ///< Set the shaper source follower current
    uint16_t iPsp() const;              ///< APSP current
    ApvRegister& setIPsp(uint16_t);     ///< Set the APSP current
    uint16_t iMuxin() const;            ///< Output buffer pedestal control
    ApvRegister& setIMuxin(uint16_t);   ///< Set the output buffer pedestal
    uint16_t iCal() const;              ///< Calibration pulse strength
    ApvRegister& setICal(uint16_t);     ///< Set the calibration pulse strength
    uint16_t vPsp() const;              ///< APSP voltage level
    ApvRegister& setVPsp(uint16_t);     ///< Set the APSP voltage level
    uint16_t vFs() const;               ///< Shaper feedback voltage
    ApvRegister& setVFs(uint16_t);      ///< Set the shaper feedback voltage
    uint16_t vFp() const;               ///< Preamplifier feedback voltage
    ApvRegister& setVFp(uint16_t);      ///< Set the preamplifier feedback voltage
    uint16_t cDrv() const;              ///< Calibration channel mask
    ApvRegister& setCDrv(uint16_t);     ///< Set the calibration channel mask
    uint16_t cSel() const;              ///< Calibraton fine phase (3.125 ns)
    ApvRegister& setCSel(uint16_t);     ///< Set the calibraton fine phase (3.125 ns)

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
