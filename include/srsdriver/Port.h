/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_Port_h
#define srsdriver_Port_h

namespace srs {
  /// Port identifier type
  typedef int port_t;
  /// List of accessible SRS ports leading to control registers
  enum Port : port_t {
    P_INVALID = 0x0,
    /// System registers
    P_SYS = 0x1777,  // = 0d6007
    /// APV application registers (trigger sequencer/event builder)
    P_APVAPP = 0x1797,  // = 0d6039
    /// Hybrid I2C bus access (APV/PLL registers)
    P_APV = 0x1877,  // = 0d6263
    /// ADC CCard I2C registers (power, equalisation, channel reset, clock enable)
    P_ADCCARD = 0x1977  // = 0d6519
  };
}  // namespace srs

#endif
