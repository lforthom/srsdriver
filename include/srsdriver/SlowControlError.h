/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_SlowControlError_h
#define srsdriver_SlowControlError_h

#include "srsutils/ConfigWord.h"

namespace srs {
  /// List of slow control communication errors
  struct SlowControlError : private ConfigWord {
    using ConfigWord::ConfigWord;
    bool frameDecoderErrors() const { return checksumError() || illFormedCommand() || unrecognisedCommand(); }
    bool checksumError() const { return (word_ >> 16) & 0x1; }
    bool illFormedCommand() const { return (word_ >> 18) & 0x1; }
    bool unrecognisedCommand() const { return (word_ >> 19) & 0x1; }
    bool frameReceiverErrors() const {
      return replyIdError() || illegalLength() || incompleteWord() || bufferFull() || illegalSourcePort() ||
             destinationPortUnavailable();
    }
    bool replyIdError() const { return (word_ >> 26) & 0x1; }
    bool illegalLength() const { return (word_ >> 27) & 0x1; }
    bool incompleteWord() const { return (word_ >> 28) & 0x1; }
    bool bufferFull() const { return (word_ >> 29) & 0x1; }
    bool illegalSourcePort() const { return (word_ >> 30) & 0x1; }
    bool destinationPortUnavailable() const { return (word_ >> 31) & 0x1; }

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
