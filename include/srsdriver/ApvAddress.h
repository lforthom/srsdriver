/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_ApvAddress_h
#define srsdriver_ApvAddress_h

#include "srsutils/POI.h"

namespace srs {
  /// Specialised channels pattern of inhibit for the APV register
  struct ApvAddress : public POI<1> {
    enum Mode { PLL = 0, MASTER = 1, SLAVE = 2, ALL_APVS = 3 };
    explicit ApvAddress(uint32_t word);
    explicit ApvAddress(uint8_t poi, Mode mode);
    static ApvAddress all(Mode mode);
    static ApvAddress channel(size_t, Mode mode);
    Mode mode() const;
    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
