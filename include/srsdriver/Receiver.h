/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_Receiver_h
#define srsdriver_Receiver_h

#include <netinet/in.h>

#include <string>

#include "srsdriver/Port.h"
#include "srsutils/Config.h"

namespace srs {
  /// UDP receiver object for data transfer from SRS
  class Receiver {
  public:
    /// Receiver constructor
    /// \param[in] port UDP port to bind
    /// \param[in] invert Invert all 32-bit words received?
    explicit Receiver(port_t port, bool invert);
    ~Receiver();

    /// Retrieve from socket a list of request answers
    /// \param[in] max_words maximum number of 32-bit words to retrieve
    words_t retrieve(size_t max_words) const;  ///< Retrieve a collection of 32-bit words
    std::string address() const;               ///< IP address of the SRS module
    port_t port() const;                       ///< UDP port bound

  private:
    static constexpr int INVALID_FD = -999;  ///< Invalid file descriptor for tests

    void resetTotal();
    bool empty() const;

    int fd_{INVALID_FD};
    mutable uint64_t total_{0ull};
    struct sockaddr_in cliaddr_ {};

    const bool invert_words_;  ///< Invert all 32-bit words received?
  };
}  // namespace srs

#endif
