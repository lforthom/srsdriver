/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_SystemRegister_h
#define srsdriver_SystemRegister_h

#include "srsutils/Config.h"

namespace srs {
  /// System summary register
  struct SystemRegister : public Config<16> {
    SystemRegister();                         ///< Trivial constructor
    explicit SystemRegister(const words_t&);  ///< Construct a system register from its words

    /// List of configuration words held by this register
    enum Field {
      VERSION = 0x0,
      FPGAMAC_VENDORID = 0x1,
      FPGAMAC_ID = 0x2,
      FPGA_IP = 0x3,
      DAQPORT = 0x4,
      SCPORT = 0x5,
      FRAMEDLY = 0x6,
      TOTFRAMES = 0x7,
      ETHMODE = 0x8,
      SCMODE = 0x9,
      DAQ_IP = 0xa,
      DTCC_CTRL = 0xb,
      MCLK_SEL = 0xc,
      MCLK_STATUS = 0xd,
      VERSION_HW = 0xf,
      SYS_RSTREG = 0xffffffff
    };
    enum Message {
      WARM_INIT = 0xffff0001,
      REBOOT_FEC = 0xffff8000,
    };
    static const fnames_t fieldNames;  ///< Human-readable names of each configuration word
    friend std::ostream& operator<<(std::ostream&, const Field&);

    uint16_t fwVersion() const;                    ///< Firmware version identifier
    std::string vendorId() const;                  ///< Local MAC address, vendor identifier part
    std::string macId() const;                     ///< Local MAC address, device identifier part
    std::string fecIP() const;                     ///< Local (FEC) IP address
    SystemRegister& setFecIP(const std::string&);  ///< Set the FEC IP address
    std::string daqIP() const;                     ///< DAQ destination IP
    SystemRegister& setDaqIP(const std::string&);  ///< Set the DAQ destination IP
    uint16_t daqPort() const;                      ///< UDP port for data transfer
    uint16_t scPort() const;                       ///< UDP port for slow-control

    /// All possible values of clock source
    enum ClockSource { UNKNOWN, LOCAL, DTC, ETH };
    friend std::ostream& operator<<(std::ostream&, const ClockSource&);
    ClockSource clockSource() const;  ///< Select the source of the main (40-MHz) clock

    /// DTC configuration word
    struct DtcConfig : public ConfigWord {
      using ConfigWord::ConfigWord;

      /// Send data via the SC channel (Data over DTC over ETH).
      /// \note Data frames will pass through the Ethernet infrastructure in the DCS LAN,
      ///  and not through the DTC data channel (and the Event Builder in the SRU, S-Link, etc.)
      bool dataOverEth() const;
      DtcConfig& setDataOverEth(bool);  ///< Enable/disable data transmission through the SC channel

      bool flowControl() const;         ///< Disable the flow control between DTC and ETH for the SC channel
      DtcConfig& setFlowControl(bool);  ///< Enable/disable the DTC/ETH flow control

      /// Set of possible padding boundary modes
      /// \note According to SC manual, only 32-bit padding is implemented
      enum PaddingType { NONE = 0x0, P_16BIT = 0x1, P_32BIT = 0x2, P_64BIT = 0x3 };
      PaddingType paddingType() const;         ///< Padding boundary
      DtcConfig& setPaddingType(PaddingType);  ///< Set the padding boundary type

      bool dataChannelTrgId() const;         ///< Send TRGID (64-bit) in the data channel
      DtcConfig& setDataChannelTrgId(bool);  ///< Enable/disable TRGID transmission in data channel

      /// Send TRGID (64 bit) at the beginning of each data frame (if DTC_TRGIDENABLE is 1)
      /// \note By default TRGID is sent only with the first frame.
      bool dataFrameTrgId() const;
      DtcConfig& setDataFrameTrgId(bool);  ///< Enable/disable TRGID transmission at the beginning of each data frame

      /// Number of trailer words (32 bit) at the end of a data event
      /// \note If set to 0, no trailer is generated.
      uint8_t trailerWordCount() const;
      DtcConfig& setTrailerWordCount(uint8_t);  ///< Specify number of trailer words to transmit at the end of an event

      uint8_t paddingByte() const;  ///< Padding byte added at the end of a frame to align to a specific word boundary
      DtcConfig& setPaddingByte(uint8_t);  ///< Set the end padding byte for data frames

      uint8_t trailerByte() const;         ///< Byte repeated in the trailer on the DTC data channel
      DtcConfig& setTrailerByte(uint8_t);  ///< Set the trailer byte

      void print(std::ostream& os) const override;
    };

    DtcConfig dtcConfig() const;                     ///< Control register for the DTCC link
    DtcConfig& dtcConfig();                          ///< Control register for the DTCC link
    SystemRegister& setDtcConfig(const DtcConfig&);  ///< Set the DTCC link register

    /// Clock selector configuration word
    struct ClockSel : public ConfigWord {
      using ConfigWord::ConfigWord;

      bool dtcClockInhibit() const;        ///< DTC clock inhibit
      ClockSel& setDtcClockInhibit(bool);  ///< Set the inhibit flag for the DTC clock

      bool dtcTriggerInhibit() const;        ///< DTC trigger inhibit
      ClockSel& setDtcTriggerInhibit(bool);  ///< Set the inhibit flag for the DTC trigger

      bool dtcSwapPorts() const;        ///< Swap DTC ports (CTF connection from J1 instead of J2)
      ClockSel& setDtcSwapPorts(bool);  ///< Enable/disable DTC ports swap

      bool dtcSwapLanes() const;        ///< Swap the clock and trigger inputs on the DTC connector
      ClockSel& setDtcSwapLanes(bool);  ///< Enable/disable the clock and trigger inputs swap on the DTC connector

      bool dtcTriggerPolInvert() const;        ///< Invert the polarity of the trigger
      ClockSel& setDtcTriggerPolInvert(bool);  ///< Set the polarity of the trigger (false: normal, or true: inverted)

      bool ethernetClockSel() const;        ///< Force the use of the ethernet clock as main application clock
      ClockSel& setEthernetClockSel(bool);  ///< Enable/disable the use of the ethernet clock as main application clock

      void print(std::ostream& os) const override;
    };

    ClockSel clockSelection() const;                     ///< Main clock selection register
    ClockSel& clockSelection();                          ///< Main clock selection register
    SystemRegister& setClockSelection(const ClockSel&);  ///< Select main clock

    /// Clock status word
    struct ClockStatus : public ConfigWord {
      using ConfigWord::ConfigWord;

      bool dtc0ClkLocked() const;  ///< Stable clock present at DTC
      bool dtc1ClkLocked() const;  ///< Stable clock recovered from the ethernet port

      /// List of possible clock selection decisions
      enum ClockSelection { LOCAL = 0x0, DTC = 0x1, ETH_RX = 0x2, INVALID = 0x3 };
      /// Human-readable clock selection
      friend std::ostream& operator<<(std::ostream&, const ClockSelection&);
      ClockSelection clockSelection() const;  ///< Clock selection decision

      /// Estimate of the DTC clock frequency wrt the local oscillator
      /// \note Local oscillator rate normalised to 10k (1 unit = 100 ppm)
      double clockFrequency() const;

      void print(std::ostream& os) const override;
    };

    ClockStatus clockStatus() const;  ///< Main clock status register (read only)
    uint16_t hwVersion() const;       ///< FW version register (read only)

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
