/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_ApvAppRegister_h
#define srsdriver_ApvAppRegister_h

#include "srsutils/Config.h"
#include "srsutils/POI.h"

namespace srs {
  /// Channels enable pattern
  struct ChannelEnable : public POI<2> {
    explicit ChannelEnable(uint32_t word);
    ChannelEnable();
    ~ChannelEnable() override;
    enum Mode { DISABLED = 0x0, MASTER = 0x1, SLAVE = 0x2, ALL_APVS = 0x3 };
    friend std::ostream& operator<<(std::ostream&, const Mode&);
    inline Mode mode(size_t ch) const { return static_cast<Mode>(enabled(ch)); }
    ChannelEnable& setMode(size_t ch, Mode mode);

    void print(std::ostream& os) const override;
  };

  /// APV application summary register
  struct ApvAppRegister : public Config<32> {
    ApvAppRegister();
    explicit ApvAppRegister(const words_t&);
    /// List of configuration words held by this register
    enum Field {
      BCLK_MODE = 0x00,
      BCLK_TRGBURST = 0x01,
      BCLK_FREQ = 0x02,
      BCLK_TRGDELAY = 0x03,
      BCLK_TPDELAY = 0x04,
      BCLK_ROSYNC = 0x05,
      ADC_STATUS = 0x07,
      EVBLD_CHENABLE = 0x08,
      EVBLD_DATALENGTH = 0x09,
      EVBLD_MODE = 0x0a,
      EVBLD_EVENTINFOTYPE = 0x0b,
      EVBLD_EVENTINFODATA = 0x0c,
      RO_ENABLE = 0x0f,
      APZ_SYNC_DET = 0x10,
      APZ_STATUS = 0x11,
      APZ_APVSELECT = 0x12,
      APZ_NSAMPLES = 0x13,
      APZ_ZEROSUPP_THR = 0x14,
      APZ_ZEROSUPP_PRMS = 0x15,
      APV_SYNC_LOWTHR = 0x1d,
      APV_SYNC_HIGHTHR = 0x1e,
      APZ_CMD = 0x1f
    };
    static const fnames_t fieldNames;  ///< Human-readable names of each configuration word
    friend std::ostream& operator<<(std::ostream&, const Field&);

    /// Trigger sequencer register for the APV
    struct TriggerMode : public ConfigWord {
      using ConfigWord::ConfigWord;
      bool apvReset() const;           ///< APV reset flag (never used in run mode, only in test-pulse mode)
      TriggerMode& setApvReset(bool);  ///< Set/unset APV reset flag

      bool apvTestPulse() const;           ///< Test pulse mode
      TriggerMode& setApvTestPulse(bool);  ///< Set/unset test pulse mode

      bool externalTrigger() const;           ///< Trigger source: 1=TRIGIN-controlled, 0=internal continuous loop
      TriggerMode& setExternalTrigger(bool);  ///< Set trigger source

      bool nimTriggerPolarity() const;           ///< NIM (true) or inverse-NIM (false) trigger polarity
      TriggerMode& setNimTriggerPolarity(bool);  ///< Set the NIM (true) or inverse-NIM (false) trigger polarity

      void print(std::ostream& os) const override;
    };

    TriggerMode triggerMode() const;                     ///< Trigger sequencer register for the APV
    TriggerMode& triggerMode();                          ///< Trigger sequencer register for the APV
    ApvAppRegister& setTriggerMode(const TriggerMode&);  ///< Update the trigger sequencer register for the APV

    uint8_t triggerBurst() const;              ///< Time slots the APV chip is reading from its memory for each trigger
    ApvAppRegister& setTriggerBurst(uint8_t);  ///< Set # of slots read by the APV chip from its memory for each trigger

    uint16_t triggerSeqPeriod() const;              ///< Period of the trigger sequencer
    ApvAppRegister& setTriggerSeqPeriod(uint16_t);  ///< Set the period of the trigger sequencer

    uint16_t triggerDelay() const;  ///< Delay between the external/internal trigger and the APV trigger
    /// Set the delay between the external/internal trigger and the APV trigger
    ApvAppRegister& setTriggerDelay(uint16_t);

    uint16_t testPulseDelay() const;  ///< Delay between the external/internal trigger and the APV test-pulse
    /// Set the delay between the external/internal trigger and the APV test-pulse
    ApvAppRegister& setTestPulseDelay(uint16_t);

    uint16_t roSync() const;  ///< Delay between the external/internal trigger and the start of data recording
    /// Set the delay between the external/internal trigger and the start of data recording
    ApvAppRegister& setRoSync(uint16_t);

    uint32_t adcStatus() const;  ///< Read-only status of the ADC deserialisation code

    /// Channel-enable mask for the data transmission
    /// \note Even bits are masters and odd bits are slaves.
    ///  If bit is set, corresponding channel is enabled.
    ChannelEnable ebChannelEnableMask() const;
    ChannelEnable& ebChannelEnableMask();  ///< Channel-enable mask for the data transmission
    /// Set the channel-enable mask for the data transmission
    ApvAppRegister& setEbChannelEnableMask(const ChannelEnable&);

    uint16_t ebCaptureWindow() const;              ///< Length of the data capture window
    ApvAppRegister& setEbCaptureWindow(uint16_t);  ///< Set the length of the data capture window
    /// Event builder modes list
    enum EventBuilderMode {
      FRAME_EVENT_CNT = 0x0,  ///< frame-of-event counter (8-bit)
      FRAME_RUN_CNT = 0x1,    ///< frame-of-run counter (32-bit)
      TIMESTAMP = 0x2         ///< timestamp(24-bit) and frame-of-event counters
    };
    friend std::ostream& operator<<(std::ostream&, const EventBuilderMode&);
    EventBuilderMode ebMode() const;                     ///< Event builder mode
    ApvAppRegister& setEbMode(const EventBuilderMode&);  ///< Set the event builder mode
    uint8_t ebDataFormat() const;                        ///< Control the data format
    ApvAppRegister& setEbDataFormat(uint8_t);            ///< Set the data format

    /// Register to control the content of the header info field of the SRS data format
    struct EbEventInfoData : public ConfigWord {
      using ConfigWord::ConfigWord;
      /// Header info field
      enum HeaderInfo { LABEL_AND_DATALENGTH = 0x0, TRIGGER_AND_DATALENGTH = 0x1, TRIGGER_ONLY = 0x2 };
      friend std::ostream& operator<<(std::ostream&, const HeaderInfo&);
      HeaderInfo headerInfo() const;                      ///< Header info field content
      EbEventInfoData& setHeaderInfo(const HeaderInfo&);  ///< Select the content of the header info field
      uint16_t runLabel() const;               ///< Run label copied in the MSBs of HEADER_INFO_FIELD of every event
      EbEventInfoData& setRunLabel(uint16_t);  ///< Set run label copied in the MSBs of HEADER_INFO_FIELD of every event

      void print(std::ostream& os) const override;
    };
    EbEventInfoData ebEventInfoData() const;  ///< Data for the optional info filled in the data format
    EbEventInfoData& ebEventInfoData();       ///< Data for the optional info filled in the data format
    ApvAppRegister& setEbEventInfoData(const EbEventInfoData&);  ///< Set optional info filled in the data format
    bool readoutEnabled() const;                                 ///< Accepting new triggers
    ApvAppRegister& setReadoutEnabled(bool);                     ///< Set acquisition to accept new triggers
    uint16_t apzSyncDet() const;  ///< Presence of the APV sync pulses on each channel (read-only)

    /// Status of the APZ processor
    struct ApzStatus : private ConfigWord {
      using ConfigWord::ConfigWord;
      bool phaseCalibBusy() const;     ///< Clock-phase calibration running
      bool pedestalCalibBusy() const;  ///< Pedestal calibration running
      bool phaseAligned() const;       ///< Clock-phase calibration routine completed successfully
      bool watchdogFlag() const;       ///< Last pedestal calibration terminated by a watchdog reset
      bool calibAllDone() const;       ///< "Calibrate all" command finished
      bool commandDone() const;        ///< Command finished
      bool apzBypassN() const;         ///< Bypassing APZ code. Reading out raw data from channel apzApvSelect
      bool apzEnabled() const;         ///< APZ code enabled. Reading out zero-suppressed data
      /// When "Calibrate all" command is active this field indicates the current channel being treated.
      uint8_t calibAllCurrent() const;
      /// Indicates channels that were successfully calibrated by either "Calibrate all"
      ///  or "Calibrate single" commands
      /// \note When channels are calibrated one by one the corresponding bit is updated each time
      ///  "Calibrate single" command is executed. All bits are cleared by a "APZ reset" command
      uint16_t apzChannelStatus() const;

      void print(std::ostream& os) const override;
    };
    ApzStatus apzStatus() const;   ///< Read-only status of the APZ processor
    uint8_t apzApvSelect() const;  ///< Select one APV channel for single channel commands
    ApvAppRegister& setApzApvSelect(uint8_t);

    /// How many time-samples to process for each trigger
    /// \note If set to 0, the register is set internally using the trigger burst value:
    ///   (n+1)*3 samples are processed
    uint8_t apzNumSamples() const;
    ApvAppRegister& setApzNumSamples(uint8_t);  ///< Set the number of time-samples to process for each trigger

    /// Threshold register for the zero-suppression operation
    struct ApzZeroSuppThreshold : public ConfigWord {
      using ConfigWord::ConfigWord;
      uint8_t fractionalThreshold() const;                    ///< Fractional threshold part (6-bit, msb)
      ApzZeroSuppThreshold& setFractionalThreshold(uint8_t);  ///< Set the fractional threshold part
      uint8_t integerThreshold() const;                       ///< Integer threshold part (6-bit, lsb)
      ApzZeroSuppThreshold& setIntegerThreshold(uint8_t);     ///< Set the integer threshold part

      void print(std::ostream& os) const override;
    };
    ApzZeroSuppThreshold apzZeroSuppThreshold() const;                     ///< Zero-suppression threshold register
    ApzZeroSuppThreshold& apzZeroSuppThreshold();                          ///< Zero-suppression threshold register
    ApvAppRegister& setApzZeroSuppThreshold(const ApzZeroSuppThreshold&);  ///< Set ZS threshold register value

    /// Configuration register for the zero-suppression unit
    struct ApzZeroSuppParameters : public ConfigWord {
      using ConfigWord::ConfigWord;
      /// Peak find mode
      /// \note Data is further reduced by acquiring only the peak sample and its relative sample position.
      bool peakFindMode() const;
      ApzZeroSuppParameters& setPeakFindMode(bool);  ///< Enable/disable peak find mode
      /// Pedestal correction
      /// \note Pedestal value is forced to 0 for all channels, therefore uncorrected data is acquired.
      bool disablePedestalCorrection() const;
      ApzZeroSuppParameters& setDisablePedestalCorrection(bool);  ///< Disable/enable pedestal correction
      /// Force all channels to be acquired in APZ format
      /// \note No channel is suppressed. All 128 APV channels are acquired in APZ format.
      bool forceSignal() const;
      ApzZeroSuppParameters& setForceSignal(bool);  ///< Force all channels to be acquired in APZ format
      bool thresholdMode() const;  ///< Automatic (false) or ApzZeroSuppThreshold-defined (true) APZ threshold mode
      /// Set APZ threshold mode to automatic (false) or as defined in ApzZeroSuppThreshold (true)
      ApzZeroSuppParameters& setThresholdMode(bool);

      void print(std::ostream& os) const override;
    };
    ApzZeroSuppParameters apzZeroSuppParameters() const;  ///< Zero-suppression unit configuration register
    ApzZeroSuppParameters& apzZeroSuppParameters();       ///< Zero-suppression unit configuration register
    ApvAppRegister& setApzZeroSuppParameters(const ApzZeroSuppParameters&);  ///< Set ZS unit configuration register

    /// Low threshold for the APV sync-pulse detection
    /// \note If set to 0 (default) the threshold is internally hard wired (1100)
    uint16_t apvSyncLowThreshold() const;
    ApvAppRegister& setApvSyncLowThreshold(uint16_t);  ///< Set the low threshold for the APV sync-pulse detection

    /// High threshold for the APV sync-pulse detection
    /// \note If set to 0 (default) the threshold is internally hard wired (3000)
    uint16_t apvSyncHighThreshold() const;
    ApvAppRegister& setApvSyncHighThreshold(uint16_t);  ///< Set the high threshold for the APV sync-pulse detection

    uint8_t apzCommand() const;              ///< Command register for the APZ processor
    ApvAppRegister& setApzCommand(uint8_t);  ///< Set the command register for the APZ processor

    void print(std::ostream& os) const override;
  };
}  // namespace srs

#endif
