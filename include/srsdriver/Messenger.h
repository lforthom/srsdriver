/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsdriver_Messenger_h
#define srsdriver_Messenger_h

#include <arpa/inet.h>
#include <netdb.h>

#include <array>
#include <cassert>
#include <string>

#include "srsdriver/Port.h"
#include "srsdriver/SlowControlCommand.h"
#include "srsutils/Config.h"

namespace srs {
  /// A pair of (address -> value) for FEC interaction
  typedef std::pair<word_t, word_t> wordspair_t;
  /// List of pairs (address -> value)
  typedef std::vector<wordspair_t> wordspairs_t;
  /// Network socket handler for slow control interactions
  /// \author Laurent Forthomme <laurent.forthomme@cern.ch>
  /// \date Jan 2021
  class Messenger {
  public:
    /// Message-passing object constructor
    /// \param[in] ipaddr IPv4 address at which the device can be reached
    explicit Messenger(std::string ipaddr);
    virtual ~Messenger();

    /// Slow control data format
    struct MessageData {
      inline SlowControlCommand scCommand() const { return (SlowControlCommand)(cmd_field1 >> 16); }
      word_t request_id;
      word_t sub_address;
      word_t cmd_field1;
      word_t cmd_field2;
      words_t data_raw;
      wordspairs_t data_pairs;
    };

    //--- slow_control configuration files parsing/writing

    /// Parse a slow_control command file
    static words_t parseCommands(const std::string& file, std::string& ip, port_t& port, MessageData* data = nullptr);
    /// Write a set of WRITE_PAIRS commands for a slow_control command file
    template <typename T>
    static inline std::vector<std::string> writeCommands(
        const std::string& ip, const port_t& port, int sub_addr, const T& data, bool print_banner = true) {
      wordspairs_t data_pairs;
      for (size_t i = 0; i < data.size(); i += 2)
        data_pairs.emplace_back(std::make_pair(data.value(i), data.size() > i + 1 ? data.value(i + 1) : 0));
      return writeCommands(ip, port, sub_addr, data_pairs, print_banner);
    }
    /// Write a set of WRITE_PAIRS commands for a slow_control command file
    static std::vector<std::string> writeCommands(
        const std::string& ip, const port_t& port, int sub_addr, const wordspairs_t& data, bool print_banner = true);

    //--- slow control request generators

    /// Generate a request to write pairs of (address, data)
    static words_t writePairsRequest(int sub_addr, const wordspairs_t& pairs);
    /// Generate a request to write a collection of parameter words
    static words_t writeBurstRequest(int sub_addr, int first_addr, const words_t& data);
    /// Generate a request to read a collection of parameter words
    static words_t readBurstRequest(size_t num_words, int sub_addr, int first_addr);
    /// Generate a request to read a collection of register at a set of addresses
    static words_t readListRequest(int sub_addr, const words_t& addr);

    //--- network interaction

    /// Write a full configuration word
    template <size_t N>
    inline bool writeConfig(port_t port, int sub_addr, const Config<N>& config) {
      auto old_conf = readBurst(port, config.size(), sub_addr, 0);
      assert(config.size() == old_conf.size());
      wordspairs_t req;
      for (size_t i = 0; i < old_conf.size(); ++i)
        if (config.value(i) != old_conf.at(i))
          req.emplace_back(std::make_pair(i, config.value(i)));
      if (req.empty())
        return true;
      return writePairs(port, sub_addr, req);
    }
    /// Send a message at a given port to the socket
    /// \param[in] port SRS input port
    /// \param[in] req a collection of configuration words
    bool send(port_t port, const words_t& req);

    //--- slow control network requests handling

    /// Write pairs of (address, data) onto a socket-handled device
    bool writePairs(port_t port, int sub_addr, const wordspairs_t& pairs);
    /// Write a collection of parameter words onto a socket-handled device
    bool writeBurst(port_t port, int sub_addr, int first_addr, const words_t& data);
    /// Read a collection of parameter words from a socket-handled device
    words_t readBurst(port_t port, size_t num_words, int sub_addr = 0, int first_addr = 0);
    /// Read a collection of register at a set of addresses from a socket-handled device
    words_t readList(port_t port, int sub_addr, const words_t& addr);
    /// Maximum number of readouts to perform before giving up
    inline size_t maxReadoutAttempts() const { return max_num_attempts_readout_; }

  private:
    /// List of fields in the UDP communication payload
    enum UdpPayloadField {
      REQUEST_ID = 0x0,
      SUB_ADDRESS = 0x1,
      CMD_FIELD1 = 0x2,
      CMD_FIELD2 = 0x3,
      NUM_PAYLOAD_FIELDS
    };

    /// UDP error payload
    enum struct UdpPayloadError { REQUEST_ID, ERROR_CODE };

    /// Connect to the network socket
    bool bind(port_t port);
    /// Retrieve from socket a list of request answers
    /// \param[in] max_words maximum number of 32-bit words to retrieve
    words_t retrieve(size_t max_words);
    /// Handle a send-and-retrieve communication with the socket
    words_t inquire(port_t port, const words_t& req);

    /// Extract the data and error words from a request answer
    /// \param[in] data a request answer from socket (data words interleaved with error words)
    words_t parse(const words_t& data) const;
    /// Build a new request with incremented counter
    static words_t newRequest(int sub_addr, SlowControlCommand cmd);

    const size_t max_num_attempts_readout_;  ///< Number of readout attempts to perform before giving up
    const size_t num_debug_list_;            ///< Number of words to print in debugging
    const uint32_t delay_between_readouts_;  ///< Delay to impose between two readout/one write and one read
    int fd_{-1};                             ///< Socket file descriptor
    struct sockaddr_in servaddr_ {};         ///< Input network stream information
    const std::string ip_addr_;              ///< SRS controller IP address
    struct addrinfo* ai_{nullptr};
    std::array<char, 128> host_{};
    /// Number of requests already processed
    /// \note currently unused by both FW and SW
    size_t req_cnt_{0ull};
  };
}  // namespace srs

#endif
