/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsreadout_SrsFrame_h
#define srsreadout_SrsFrame_h

#include "srsdriver/ApvAppRegister.h"
#include "srsutils/ConfigWord.h"

namespace srs {
  /// Unpacking index for a SRS data stream
  class SrsData : public words_t {
  public:
    /// Construct an unpacking index from its current position
    /// and the last position in the stream
    explicit SrsData(witer_t&, const witer_t);
    virtual void print(std::ostream&) const {}  ///< Human-readable version of this word (to be overriden)

    static constexpr word_t GLOBAL_TRAILER = 0xfafafafa;

  protected:
    static constexpr uint16_t apv_header_level_ = 1500;
    mutable witer_t ptr_;  ///< Pointer to the word currently read
    witer_t beg_;          ///< Pointer to the beginning of the words stream
    const witer_t end_;    ///< Pointer to the end of the words stream
  };

  /// A global frame as unpacked from the SRS output
  class SrsFrame : public words_t {
  public:
    explicit SrsFrame(const words_t& words, ApvAppRegister::EventBuilderMode mode);

    struct FrameCounter : public Word {
      explicit FrameCounter(word_t word, ApvAppRegister::EventBuilderMode mode);
      bool check() const;
      void print(std::ostream& os) const override;

      uint16_t timestamp() const;
      uint8_t frameCounter() const;
      uint32_t globalFrameCounter() const;

      const ApvAppRegister::EventBuilderMode mode;
    };
    /// 32-bit word primarily tagging each UDP frame corresponding to one event
    inline FrameCounter frameCounter() const { return FrameCounter(at(FRAME_CNT), mode_); }

    enum DataSource { ADC, APV_ZS, A_CCARD };
    friend std::ostream& operator<<(std::ostream&, const DataSource&);
    std::string dataSourceStr() const;  ///< Three-letter representation of the data producer
    DataSource dataSource() const;      ///< Data producer
    inline uint8_t daqChannel() const { return at(SRC_CHANNEL) & 0xff; }  ///< DAQ channel number
    //uint8_t apvIndex() const { return (headerInfo().fecId() << 4) | daqChannel(); }
    uint8_t apvIndex() const { return at(FRAME_CNT) & 0xff; }
    struct HeaderInfo : public Word {
      using Word::Word;
      inline uint16_t adcSize() const { return word_ & 0xffff; }        ///< Number of ADC words
      inline uint16_t fecId() const { return (word_ >> 16) & 0xffff; }  ///< FEC identifier
      void print(std::ostream& os) const override;
    };
    /// Field reserved for future use to allow the transport of event related
    /// data or information about the data structure in the Hybrid Layer
    HeaderInfo headerInfo() const { return (HeaderInfo)at(HDR_INFO); }

    struct NoMoreFramesException {};  ///< Exception to specify no more frames are to be read from the input stream
    /// Retrieve the next object in the frame
    /// \tparam T Object type to be unpacked
    template <typename T>
    T next() const {
      if (ptr_ == cend())
        throw NoMoreFramesException();
      return T(ptr_, cend());
    }
    SrsData* nextPtr() const;  ///< Retrieve the next object pointer in the frame

    void print(std::ostream& os) const;

  private:
    enum Field { FRAME_CNT = 0, SRC_CHANNEL = 1, HDR_INFO = 2, NUM_HDR };

    const ApvAppRegister::EventBuilderMode mode_;
    mutable witer_t ptr_;
  };
  typedef std::vector<SrsFrame> SrsFrameCollection;  ///< A collection of global SRS frames
}  // namespace srs

#endif
