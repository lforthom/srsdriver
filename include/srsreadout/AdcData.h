/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsreadout_AdcData_h
#define srsreadout_AdcData_h

#include "srsreadout/SrsFrame.h"

namespace srs {
  /// Unpacking index for the ADC readout mode
  class AdcData : public SrsData {
  public:
    /// Construct an unpacking index from its current position
    /// and the last position in the stream
    explicit AdcData(witer_t&, const witer_t);
    void print(std::ostream& os) const override;  ///< Read all ADC frames in the buffer

    /// An APV25 event
    struct ApvEvent {
      uint8_t address;                 ///< Column address used to store the signal when APV chip triggered
      bool error;                      ///< Error detected in APV25 internal logic
      std::array<uint16_t, 128> data;  ///< Analogue data
    };
    ApvEvent next() const;      ///< Retrieve the next APV25 event
    uint16_t nextWord() const;  ///< Read the next frame word in the buffer

    /// Convert MUXed channel id into physical channel number
    static uint16_t physChannel(uint16_t ch);

  private:
    mutable bool lsb_{true};  ///< LSB/MSB unpacking flag
  };
}  // namespace srs

#endif
