/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_Word_h
#define srsutils_Word_h

#include <bitset>
#include <cstdint>
#include <iosfwd>
#include <map>
#include <string>
#include <vector>

/// Standard namespace for this library
namespace srs {
  static constexpr size_t WORD_LENGTH = 32;
  typedef uint32_t word_t;                              ///< Basic register word unit
  typedef std::vector<word_t> words_t;                  ///< Collection of register words
  typedef std::vector<word_t>::const_iterator witer_t;  ///< An iterator to roll through a list of register words
  typedef std::map<int, std::string> fnames_t;          ///< Collection of field names for a register
  class Messenger;

  /// Helper object to store a 32-bit word
  class Word {
  public:
    Word() = default;       ///< Default constructor
    explicit Word(word_t);  ///< Constructor from a 32-bit unsigned integer
    Word(const Word& oth);  ///< Copy constructor
    virtual ~Word();

    Word& operator=(const Word&);  ///< Assignment operator

    typedef std::bitset<WORD_LENGTH> bitset_t;

    operator uint32_t&() { return word_; }       ///< 32-bit unsigned integer conversion operator
    operator uint32_t() const { return word_; }  ///< 32-bit unsigned integer (const-qualified) conversion operator
    virtual void print(std::ostream& os) const;  ///< Human-readable printout of this word; to be overridden

  protected:
    word_t word_{0};  ///< 32-bit unsigned integer word held by this container
  };
}  // namespace srs

#endif
