/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_ApvAdcCalibration_h
#define srsutils_ApvAdcCalibration_h

#include <unordered_map>

namespace srs {
  class SlowControl;
  namespace utils {
    /// Tool to perform the ADC calibration through variable height pulses injection
    class ApvAdcCalibration {
    public:
      explicit ApvAdcCalibration(SlowControl&);  ///< Build a calibration object using a SC communicator object
      typedef std::unordered_map<int, float> chanvalues_t;  ///< Mean amplitude per channel
      /// Retrieve the mean amplitude observed for a given ICAL register value
      /// \param[in] ical calibration pulse height value (see the corresponding ApvRegister object)
      /// \param[in] num_events number of observations to reach proper precision
      chanvalues_t run(uint16_t ical, size_t num_events);

    private:
      /// Perform the readout+parsing operation for the given configuration
      void collectFrames(chanvalues_t&, size_t fec_id);

      std::atomic_bool running_;
      SlowControl& sc_;
      size_t num_samples_baseline_;
      size_t num_printout_;
    };
  }  // namespace utils
}  // namespace srs

#endif
