/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_Config_h
#define srsutils_Config_h

#include <array>
#include <memory>

#include "srsutils/ConfigWord.h"

/// Standard namespace for this library
namespace srs {
  class Messenger;
  /// A collection of smart pointer objects to configuration words
  template <size_t N>
  using wordsptr_t = std::array<std::shared_ptr<ConfigWord>, N>;
  /// Helper object to store a collection of configuration words
  template <size_t N>
  class Config : public wordsptr_t<N> {
  public:
    friend class Messenger;  // allow messenger object to retrieve old configuration
    explicit inline Config() {
      for (size_t i = 0; i < N; ++i)
        (*this)[i].reset(new ConfigWord);
    }
    /// Build a configuration from a list of unsigned integers
    explicit inline Config(const words_t& words) {
      for (size_t i = 0; i < words.size(); ++i) {
        (*this)[i].reset(new ConfigWord(words.at(i)));
        old_config_[i] = ConfigWord(words.at(i));
      }
    }
    /// Retrieve one single configuration word value
    inline uint32_t value(size_t idx) const { return *(this->at(idx)); }
    /// Set a single configuration word value
    inline Config& setValue(size_t idx, uint32_t word) {
      *(*this)[idx] = ConfigWord(word);
      return *this;
    }
    /// Human-readable display of this configuration
    virtual void print(std::ostream&) const = 0;
    /// Retrieve raw words content
    inline words_t commands() const {
      words_t output;
      for (size_t i = 0; i < N; ++i)
        output.emplace_back(value(i));
      return output;
    }
    /// Word-by-word display of this configuration
    inline void printConfig(std::ostream& os) const {
      for (size_t i = 0; i < N; ++i)
        static_cast<Word>(value(i)).print(os);
    }

  private:
    std::array<ConfigWord, N> old_config_;
  };
}  // namespace srs

#endif
