/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_POI_h
#define srsutils_POI_h

#include <utility>

#include "srsutils/ConfigWord.h"

/// Standard namespace for this library
namespace srs {
  /// Pattern-of-inhibit word container
  /// \tparam N number of bits allocated per "channel"
  template <size_t N>
  class POI : public ConfigWord {
  public:
    /// A collection of LSB indexes for all "channels" in word
    typedef std::vector<uint16_t> pattern_t;
    /// Class constructor
    /// \param[in] word a 16 or 32-bit word holding the POI
    /// \param[in] pattern LSB positions for all "channels"
    explicit POI(uint32_t word, pattern_t pattern) : ConfigWord(0), pattern_(std::move(pattern)) {
      for (size_t i = 0; i < pattern_.size(); ++i)
        set(pattern_.at(i), ((uint32_t)word >> N * i) & ((1 << N) - 1), N);
    }
    /// Copy constructor
    POI(const POI& poi) : ConfigWord(poi.word_), pattern_(poi.pattern_) {}

    void print(std::ostream&) const override;

    /// Number of "channel"-like words contained
    size_t size() const { return pattern_.size(); }
    /// Enable/status word for a given "channel"
    uint16_t enabled(size_t ch_id) const {
      if (ch_id < pattern_.size())
        return (word_ >> pattern_[ch_id]) & ((1 << N) - 1);
      return 0;
    }
    /// Set the enable/status word for a "channel"
    POI& setEnabled(size_t ch_id, uint8_t en) {
      set(pattern_[ch_id], en, N);
      return *this;
    }

  protected:
    pattern_t pattern_;  ///< channel id -> bit in word
  };

  // a few template forward definitions to help compiler feel happy
  template class POI<1>;
  template class POI<2>;
}  // namespace srs

#endif
