/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_String_h
#define srsutils_String_h

#include <string>
#include <vector>

namespace srs {
  /// A namespace for all utility objects and functions
  namespace utils {
    /// Format a string using a printf style format descriptor.
    std::string format(const std::string& fmt, ...);
    /// TTY-type enumeration of colours
    enum class Colour {
      reset = -1,
      black = 30,
      red = 31,
      green = 32,
      yellow = 33,
      blue = 34,
      magenta = 35,
      cyan = 36,
      white = 37
    };
    /// TTY-type enumeration of text styles
    enum class Modifier { reset = -1, bold = 1, dimmed = 2, italic = 3, underline = 4, blink = 5, reverse = 7 };
    /// Colourise a string for TTY-type output streams
    std::string colourise(const std::string& str, const Colour& col, const Modifier& mod = Modifier::reset);
    /// Current human-readable date and time
    std::string now();
    /// Split a string according to a separation character
    std::vector<std::string> split(const std::string&, char);
    /// Merge a collection of strings in a single string
    std::string merge(const std::vector<std::string>&, const std::string&);
    /// Lowercase version of a string
    std::string tolower(const std::string&);
    /// Append a 's' whenever necessary
    std::string s(const std::string&, int, bool print_num = false);
  }  // namespace utils
}  // namespace srs

#endif
