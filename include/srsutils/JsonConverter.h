/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_JsonConverter_h
#define srsutils_JsonConverter_h

#include <nlohmann/json.hpp>

namespace srs {  // namespace srs
  class AdcCardRegister;
  class ApvRegister;
  class ApvAppRegister;
  class PllRegister;
  class SystemRegister;

  void to_json(nlohmann::json&, const AdcCardRegister&);
  void from_json(const nlohmann::json&, AdcCardRegister&);
  void to_json(nlohmann::json&, const ApvRegister&);
  void from_json(const nlohmann::json&, ApvRegister&);
  void to_json(nlohmann::json&, const ApvAppRegister&);
  void from_json(const nlohmann::json& js, ApvAppRegister&);
  void to_json(nlohmann::json&, const PllRegister&);
  void from_json(const nlohmann::json&, PllRegister&);
  void to_json(nlohmann::json&, const SystemRegister&);
  void from_json(const nlohmann::json& js, SystemRegister&);
}  // namespace srs

#endif
