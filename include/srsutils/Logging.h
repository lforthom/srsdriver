/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_Logging_h
#define srsutils_Logging_h

#include <iosfwd>
#include <regex>
#include <vector>

#ifdef _WIN32
#define __FUNC__ __FUNCSIG__
#else
#define __FUNC__ __PRETTY_FUNCTION__
#endif

#define SRS_LOG_MATCH(str, type) srs::Logger::get().passLoggingRule(str, srs::Logger::Level::type)

#define SRS_LOG(mod)                                     \
  (!SRS_LOG_MATCH(mod, information)) ? srs::NullStream() \
                                     : srs::LoggedMessage(__FUNC__, mod, srs::LogMessage::Type::verbatim)
#define SRS_INFO(mod)                                    \
  (!SRS_LOG_MATCH(mod, information)) ? srs::NullStream() \
                                     : srs::LoggedMessage(__FUNC__, mod, srs::LogMessage::Type::info)
#define SRS_DEBUG(mod) \
  (!SRS_LOG_MATCH(mod, debug)) ? srs::NullStream() : srs::LoggedMessage(__FUNC__, mod, srs::LogMessage::Type::debug)
#define SRS_WARNING(mod) \
  (!SRS_LOG_MATCH(mod, warning)) ? srs::NullStream() : srs::LoggedMessage(__FUNC__, mod, srs::LogMessage::Type::warning)
#define SRS_ERROR(mod) srs::LoggedMessage(__FUNC__, mod, srs::LogMessage::Type::error)

namespace srs {
  /// General purpose logging singleton
  /// \author Laurent Forthomme <laurent.forthomme@cern.ch>
  /// \date 15 Oct 2015
  class Logger {
  public:
    /// Retrieve the running instance of the logger
    static Logger& get();

    /// Logging threshold for the output stream
    enum class Level { nothing = 0, error, warning, information, debug };
    /// Redirect the logger to a given output stream
    friend std::ostream& operator<<(std::ostream& os, const Logger::Level& lvl);

    /// \brief Add a new rule to display exceptions/messages
    /// \param[in] rule Regex rule to handle
    void addLoggingRule(const std::string& rule);
    /// Collection of logging exceptions
    inline const std::vector<std::regex>& exceptionRules() const { return allowed_exc_; }
    /// Is the module set to be displayed/logged?
    /// \param[in] tmpl Module name to probe
    /// \param[in] lev Upper verbosity level
    bool passLoggingRule(const std::string& tmpl, const Level& lev) const;

    /// Insertion operator
    std::ostream& operator<<(const std::string&);
    /// Output stream
    inline std::ostream* outputPtr() { return output_; }
    /// Set the output stream
    inline Logger& setOutput(std::ostream* output, bool pretty = true) {
      output_ = output;
      pretty_ = pretty;
      return *this;
    }
    /// Logging threshold
    inline Level level() const { return level_; }
    /// Set the logging threshold
    Logger& setLevel(const Level& lev);
    /// Set the printing "pretty-ness"
    inline void setPretty(bool pretty) { pretty_ = pretty; }
    /// Use pretty-print routines
    inline bool pretty() const { return pretty_; }

  private:
    /// Initialize a logging object
    explicit Logger();

    std::vector<std::regex> allowed_exc_;
    /// Logging threshold for the output stream
    Level level_;
    /// Output stream to use for all logging operations
    std::ostream* output_;
    /// Use pretty-print routines
    bool pretty_;
  };

  /// A generic exception type
  /// \author Laurent Forthomme <laurent.forthomme@cern.ch>
  /// \date 27 Mar 2015
  struct LogMessage {
    /// Generic exception constructor
    inline LogMessage() {}
    virtual ~LogMessage() noexcept = default;
    /// Enumeration of exception severities
    enum class Type : int {
      undefined = -1,  ///< Irregular exception
      debug,           ///< Debugging information to be enabled
      verbatim,        ///< Raw information
      info,            ///< Prettified information
      warning,         ///< Casual non-stopping warning
      error            ///< General non-stopping error
    };
    /// Dump the full exception information in a given output stream
    /// \param[inout] os the output stream where the information is dumped
    virtual std::ostream& print(std::ostream* os = nullptr) const = 0;
    /// Logging message
    virtual std::string message() const = 0;
  };

  /// A simple exception handler
  /// \date 24 Mar 2015
  class LoggedMessage : public LogMessage {
  public:
    /// Generic constructor
    /// \param[in] module exception classifier
    /// \param[in] type exception type
    /// \param[in] id exception code (useful for logging)
    explicit LoggedMessage(const char* module = "", Type type = Type::undefined, short id = 0);
    /// Generic constructor
    /// \param[in] from method invoking the exception
    /// \param[in] module exception classifier
    /// \param[in] type exception type
    /// \param[in] id exception code (useful for logging)
    explicit LoggedMessage(const char* from, const char* module, Type type = Type::undefined, short id = 0);
    /// Copy constructor
    LoggedMessage(const LoggedMessage& rhs) noexcept;
    /// Default destructor (potentially killing the process)
    virtual ~LoggedMessage() noexcept;

    //----- Overloaded stream operators

    /// Generic templated message feeder operator
    template <typename T>
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, const T& var) {
      auto& nc_except = const_cast<LoggedMessage&>(exc);
      nc_except.message_ << var;
      return exc;
    }
    /// Generic templated pair-variables feeder operator
    template <typename T, typename U>
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, const std::pair<T, U>& pair_var) {
      return exc << "(" << pair_var.first << ", " << pair_var.second << ")";
    }
    /// Generic templated vector-variables feeder operator
    template <typename T>
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, const std::vector<T>& vec_var) {
      exc << "{";
      std::string sep;
      if (!vec_var.empty())
        for (const auto& var : vec_var)
          exc << sep << var, sep = ", ";
      return exc << "}";
    }
    /// Generic templated vector-variables feeder operator
    template <typename T, std::size_t N>
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, const std::array<T, N>& vec_var) {
      exc << "{";
      std::string sep;
      if (!vec_var.empty())
        for (const auto& var : vec_var)
          exc << sep << var, sep = ", ";
      return exc << "}";
    }
    /// Generic templated mapping-variables feeder operator
    template <typename T, typename U>
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, const std::map<T, U>& map_var) {
      exc << "{";
      std::string sep;
      if (!map_var.empty())
        for (const auto& var : map_var)
          exc << sep << "{" << var.first << " -> " << var.second << "}", sep = ", ";
      return exc << "}";
    }
    /// Pipe modifier operator
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, std::ios_base& (*f)(std::ios_base&)) {
      auto& nc_except = const_cast<LoggedMessage&>(exc);
      f(nc_except.message_);
      return exc;
    }
    /// Output stream throughput operator
    inline friend const LoggedMessage& operator<<(const LoggedMessage& exc, std::ostream& (*f)(std::ostream&)) {
      auto& nc_except = const_cast<LoggedMessage&>(exc);
      f(nc_except.message_);
      return exc;
    }
    /// Lambda function handler
    template <typename T>
    inline LoggedMessage& log(T&& lam) {
      lam(*this);
      return *this;
    }
    /// Flushing operator
    inline operator std::ostream&() { return message_.flush(); }
    /// Human-readable explanation of the message
    inline std::string message() const override { return message_.str(); }
    /// Origin of the message
    inline std::string from() const { return from_; }
    /// Message error code
    inline int errorNumber() const { return error_num_; }
    /// Message type
    inline Type type() const { return type_; }
    /// Human-readable dump of the exception message
    std::ostream& print(std::ostream* os = nullptr) const override;

  private:
    std::string from_;            ///< Origin of the exception
    std::string module_;          ///< Logging classification
    std::ostringstream message_;  ///< Message to throw
    Type type_;                   ///< Logging type
    short error_num_;             ///< Integer exception number
  };

  /// Placeholder for debugging messages if logging threshold is not reached
  /// \date Apr 2018
  struct NullStream : LogMessage {
    using LogMessage::LogMessage;
    /// Empty constructor
    inline NullStream() = default;
    /// Empty constructor
    inline NullStream(const LoggedMessage&) {}
    std::ostream& print(std::ostream* os = nullptr) const override { return *os; }
    /// Stream operator (null and void)
    template <class T>
    inline NullStream& operator<<(const T&) {
      return *this;
    }
    /// Lambda function handler (null and void)
    template <typename T>
    inline NullStream& log(T&&) {
      return *this;
    }
    std::string message() const override { return ""; }
  };
}  // namespace srs

#endif
