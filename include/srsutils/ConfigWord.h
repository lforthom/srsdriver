/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srsutils_ConfigWord_h
#define srsutils_ConfigWord_h

#include "srsutils/Word.h"

namespace srs {
  /// Helper object to store a configuration word
  class ConfigWord : public Word {
  public:
    ConfigWord() = default;            ///< Empty configuration word
    explicit ConfigWord(word_t word);  ///< Build a configuration word from an integer
    ConfigWord(const ConfigWord&);     ///< Copy constructor

    ConfigWord& operator=(const ConfigWord&);  ///< Assignment operator

    /// Set a (few) bit(s) given its (their) LSB and value
    /// \param[in] lsb least-significant bit for the bitset
    /// \param[in] seq sequence to set
    /// \param[in] size number of bits to override
    ConfigWord& set(uint16_t lsb, uint32_t seq, uint16_t size);
    /// Virtual destructor (for memory footprint only)
    ~ConfigWord() override;

  private:
    word_t orig_word_{0};
  };
}  // namespace srs

#endif
