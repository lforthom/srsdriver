/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_SysRegisterTab_h
#define srscontrol_SysRegisterTab_h

#include <QWidget>
#include <memory>

#include "srsdriver/SystemRegister.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class SysRegisterTab;
}
QT_END_NAMESPACE
class FecControl;
class QElementsTable;

class SysRegisterTab : public QWidget {
  Q_OBJECT
public:
  explicit SysRegisterTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~SysRegisterTab();

signals:
  void statusEvent(const QString&);

public slots:
  void refresh();

private slots:
  void push();
  void warmInit();
  void rebootFec();
  void launchRateMonitor();

private:
  std::unique_ptr<Ui::SysRegisterTab> ui_;
  FecControl* main_;
  srs::SystemRegister sys_;

  std::unique_ptr<QElementsTable> table_dtc_info_;
};

#endif
