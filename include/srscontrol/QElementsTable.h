/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_QElementsTable_h
#define srscontrol_QElementsTable_h

#include <QAbstractTableModel>

class QTableView;

class QElementsTable : public QAbstractTableModel {
  Q_OBJECT
public:
  QElementsTable(const std::vector<QString>& fields,
                 const std::vector<QString>& cols = {""},
                 QTableView* table = nullptr);

  static uint32_t parseData(const QString& raw_data);

  void clear(bool clear_fields = false);
  void append(const std::vector<QString>&);

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex& index = QModelIndex(), int role = Qt::DisplayRole) const override;
  uint32_t data(int field, int col = 0) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  bool setData(int field, int col, const QVariant& value, bool user = false);
  std::map<int, std::vector<QVariant> > modifiedData() const;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
  std::vector<QString> fields_;
  std::vector<QString> cols_;
  std::vector<std::vector<QString> > data_;
  std::map<int, bool> mod_rows_;
  QTableView* table_;
};

#endif
