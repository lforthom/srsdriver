/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_QPinger_h
#define srscontrol_QPinger_h

#include <QProcess>
#include <QTimer>
#include <QWidget>
#include <functional>
#include <memory>

using CallbackFunction = std::function<void()>;

class QPinger : public QObject {
  Q_OBJECT
public:
  explicit QPinger(QString addr,
                   CallbackFunction cbSuccess,
                   CallbackFunction cbError,
                   std::chrono::seconds period = std::chrono::seconds(5),
                   int max_num = 1);

public slots:
  void startPinging();

private:
  std::unique_ptr<QProcess> proc_;
  CallbackFunction cb_success_;
  CallbackFunction cb_error_;
  QTimer tmr_;
  QStringList args_;
};

#endif
