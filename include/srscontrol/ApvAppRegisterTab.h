/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_ApvAppRegisterTab_h
#define srscontrol_ApvAppRegisterTab_h

#include <QWidget>
#include <map>
#include <memory>
#include <vector>

#include "srscontrol/QElementsTable.h"
#include "srsdriver/ApvAppRegister.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class ApvAppRegisterTab;
}
QT_END_NAMESPACE
class FecControl;
class QCheckBox;

class ApvAppRegisterTab : public QWidget {
  Q_OBJECT
public:
  explicit ApvAppRegisterTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~ApvAppRegisterTab();

signals:
  void statusEvent(const QString&);

public slots:
  void refresh();
  void switchReadout(bool);

private slots:
  void push();
  void toggleButtons();

private:
  std::unique_ptr<Ui::ApvAppRegisterTab> ui_;
  FecControl* main_;  // NOT owning
  srs::ApvAppRegister apvapp_;
  //---
  std::array<QCheckBox*, 16> gen_apz_ch_sync_, gen_apz_ch_calib_;
  std::array<QCheckBox*, 8> evbld_enable_ch_master_, evbld_enable_ch_slave_;
  //---
  std::unique_ptr<QElementsTable> table_apvapp_info_;
  std::map<int, size_t> lut_apvapp_;
};

#endif
