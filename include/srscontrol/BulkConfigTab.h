/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_BulkConfigTab_h
#define srscontrol_BulkConfigTab_h

#include <QWidget>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
  class BulkConfigTab;
}
QT_END_NAMESPACE
class FecControl;

class BulkConfigTab : public QWidget {
  Q_OBJECT
public:
  explicit BulkConfigTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~BulkConfigTab();

signals:
  void statusEvent(const QString&);

public slots:
  void addFileClicked();
  void newCommandClicked();
  void clear();
  void push();
  void addConfig();
  void deleteLastConfig();

private:
  void newConfig(QString filename = "");

  enum { F_PORT = 0, F_IP, F_TYPE, F_SUBADDR, F_CMDINFO, F_WORDS } Fields;

  std::unique_ptr<Ui::BulkConfigTab> ui_;
  FecControl* main_;
};

#endif
