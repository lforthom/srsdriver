/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_WaveformCollector_h
#define srscontrol_WaveformCollector_h

#include <QDialog>
#include <QTimer>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <memory>

#include "srsreadout/SrsFrame.h"

QT_CHARTS_USE_NAMESPACE

class WaveformCollector : public QChart {
  Q_OBJECT

public:
  explicit WaveformCollector();

  /// Toggle all channels
  void switchChannels(const std::vector<bool>&);
  /// Feed and draw a waveform
  /// \param[in] frames frames composing the waveform
  /// \param[in] max_samples number of samples in each frame
  bool feed(const srs::SrsFrameCollection& frames, size_t max_samples);
  /// Toggle the unpacking of frames
  void setUnpacking(bool);

  QChartView* chartView() { return chart_view_; }

private:
  QChartView* chart_view_{nullptr};
  struct ChannelInfo {
    QString title;
    bool enabled{false};
  };
  std::map<int, ChannelInfo> m_channels_;
  bool unpack_raw_{true};
};

#endif
