/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_SrsControl_h
#define srscontrol_SrsControl_h

#include <QMainWindow>
#include <unordered_map>

#include "srscontrol/FecReadout.h"
#include "srscontrol/Logging.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class SrsControl;
}
QT_END_NAMESPACE
namespace srs {
  class SlowControl;
}
class FecControl;
class QCheckBox;
class QTabWidget;
class QTreeWidget;
class QTreeWidgetItem;
class LedIndicator;

class SrsControl : public QMainWindow {
  Q_OBJECT

public:
  SrsControl(QWidget* parent = nullptr);
  virtual ~SrsControl();

public slots:
  void setStatusBarText(const QString&);
  void launchLoggingPanel();
  void launchReadoutPanel();
  void addFec();
  void removeFec(int);
  void openAboutDialog();
  void itemDoubleClicked(QTreeWidgetItem*, int);

private:
  std::unique_ptr<Ui::SrsControl> ui_;
  QTabWidget* tabs_fecs_{nullptr};   //NOT owning
  QTreeWidget* tree_fecs_{nullptr};  //NOT owning

  typedef std::unordered_map<int, std::unique_ptr<FecControl> > FecControlColl;
  FecControlColl fecs_;

  std::unique_ptr<Logging> ui_log_;
  std::unique_ptr<FecReadout> ui_readout_;
};
#endif
