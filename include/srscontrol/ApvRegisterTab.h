/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_ApvRegisterTab_h
#define srscontrol_ApvRegisterTab_h

#include <QWidget>
#include <memory>

#include "srscontrol/QElementsTable.h"
#include "srsdriver/ApvAddress.h"
#include "srsdriver/ApvRegister.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class ApvRegisterTab;
}
QT_END_NAMESPACE
class FecControl;
class QCheckBox;

class ApvRegisterTab : public QWidget {
  Q_OBJECT
public:
  explicit ApvRegisterTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~ApvRegisterTab();

  typedef std::vector<QCheckBox*> ApvChannelsSel;
  ApvChannelsSel& apvChannelsSelectors() { return apv_ch_sel_; }

signals:
  void statusEvent(const QString&);

public slots:
  void refresh();

private slots:
  void push();
  void channelSelected(int);

private:
  static constexpr size_t NUM_CH = 8;

  srs::ApvAddress readApvChannels() const;

  std::unique_ptr<Ui::ApvRegisterTab> ui_;
  FecControl* main_;
  srs::ApvRegister apv_;
  // human read/write interaction, channels selection box
  ApvChannelsSel apv_ch_sel_;
  //---
  QTableView* table_apv_;
  std::unique_ptr<QElementsTable> table_apv_info_;
  std::map<int, size_t> lut_apv_;
};

#endif
