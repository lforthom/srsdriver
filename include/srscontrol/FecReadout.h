/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_FecReadout_h
#define srscontrol_FecReadout_h

#include <QDialog>
#include <QTimer>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
  class FecReadout;
}
QT_END_NAMESPACE
class FecControl;
namespace srs {
  class SlowControl;
}
class WaveformCollector;

class FecReadout : public QDialog {
  Q_OBJECT

public:
  explicit FecReadout(QWidget* parent = nullptr);
  virtual ~FecReadout();

signals:
  void statusEvent(const QString&);
  //void closed();

public slots:
  void addFec(const QString&, srs::SlowControl*);
  void removeFec(const QString&);
  void switchFec();

  void setReadoutButtons(bool);
  void singleReadout();
  void startReadout();
  void stopReadout();
  void selectChannels();

  void toggleUnpacking();
  void updateFrequency();
  bool update();

private:
  void prepareReadout();

  static constexpr size_t num_allowed_readouts_without_frame_ = 2;
  static const std::vector<int> channel_colours_;

  std::unique_ptr<Ui::FecReadout> ui_;
  std::unordered_map<QString, srs::SlowControl* /*NOT owning*/> map_fec_;
  srs::SlowControl* curr_fec_{nullptr};  //NOT owning

  QTimer tmr_;  ///< Readout timer
  std::unique_ptr<WaveformCollector> wave_coll_;
};

#endif
