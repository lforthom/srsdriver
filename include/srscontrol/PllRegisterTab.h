/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_PllRegisterTab_h
#define srscontrol_PllRegisterTab_h

#include <QWidget>
#include <memory>

#include "srscontrol/QElementsTable.h"
#include "srsdriver/PllRegister.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class PllRegisterTab;
}
QT_END_NAMESPACE
class FecControl;
class QCheckBox;

class PllRegisterTab : public QWidget {
  Q_OBJECT
public:
  explicit PllRegisterTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~PllRegisterTab();

  typedef std::vector<QCheckBox*> PllChannelsSel;
  PllChannelsSel& pllChannelsSelectors() { return pll_ch_sel_; }

signals:
  void statusEvent(const QString&);

public slots:
  void refresh();

private slots:
  void push();
  void channelSelected(int);

private:
  static constexpr size_t NUM_CH = 8;

  srs::ApvAddress readPllChannels() const;

  std::unique_ptr<Ui::PllRegisterTab> ui_;
  FecControl* main_;
  srs::PllRegister pll_;
  // human read/write interaction, channels selection box
  PllChannelsSel pll_ch_sel_;
  //---
  std::unique_ptr<QElementsTable> table_pll_info_;
  std::map<int, size_t> lut_pll_;
};

#endif
