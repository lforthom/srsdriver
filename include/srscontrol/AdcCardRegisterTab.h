/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_AdcCardRegisterTab_h
#define srscontrol_AdcCardRegisterTab_h

#include <QWidget>
#include <memory>

#include "srsdriver/AdcCardRegister.h"

QT_BEGIN_NAMESPACE
namespace Ui {
  class AdcCardRegisterTab;
}
QT_END_NAMESPACE
class FecControl;
class QCheckBox;

class AdcCardRegisterTab : public QWidget {
  Q_OBJECT
public:
  explicit AdcCardRegisterTab(QWidget* parent = nullptr, FecControl* main = nullptr);
  virtual ~AdcCardRegisterTab();

signals:
  void statusEvent(const QString&);

public slots:
  void refresh();

private slots:
  void push();

private:
  std::unique_ptr<Ui::AdcCardRegisterTab> ui_;
  FecControl* main_;
  srs::AdcCardRegister adc_;
  static constexpr size_t NUM_HDMI_CH = 8;
  // human read/write interaction, channels selection box
  std::array<QCheckBox*, NUM_HDMI_CH> adc_reset_pin_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_pwdwn0_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_pwdwn1_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_eqctrl0_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_eqctrl1_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_trgbuf_;
  std::array<QCheckBox*, NUM_HDMI_CH> adc_bclk_;
};

#endif
