/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_LedIndicator_h
#define srscontrol_LedIndicator_h

#include <QPushButton>

class LedIndicator : public QPushButton {
  Q_OBJECT
public:
  LedIndicator(QWidget* parent = nullptr);

  enum State { ERROR = -1, IDLE = 0, OK = 1 };
  void setState(State state);
  void toggle() { switchLedIndicator(); }

  void setOnColor(QColor);
  void setIdleColor(QColor);
  void setErrorColor(QColor);

  void setLedSize(int size);

public slots:
  void flashLedIndicator();
  void switchLedIndicator();

protected:
  void paintEvent(QPaintEvent*);

private:
  State lit_;
  QColor ledOnColor_;
  QColor ledIdleColor_;
  QColor ledErrorColor_;
  int ledSize_;
};

#endif
