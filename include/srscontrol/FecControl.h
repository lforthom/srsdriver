/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_FecControl_h
#define srscontrol_FecControl_h

#include <QCheckBox>
#include <QWidget>
#include <memory>

#include "srscontrol/ApvRegisterTab.h"
#include "srscontrol/PllRegisterTab.h"

namespace Ui {
  class FecControl;
}
namespace srs {
  class SlowControl;
}

class SrsControl;

class SysRegisterTab;
class ApvAppRegisterTab;
class AdcCardRegisterTab;
class BulkConfigTab;
class FecReadout;

class LedIndicator;
class QPinger;

class FecControl : public QWidget {
  Q_OBJECT

public:
  explicit FecControl(QWidget* parent = nullptr, SrsControl* srs = nullptr, QString fec_name = "", QString fec_ip = "");
  ~FecControl();

  void setLedIndicator(LedIndicator* led) { parent_led_ = led; }
  void setROEnabledBox(QCheckBox* box) { ro_enabled_box_ = box; }
  QCheckBox* roEnabledBox() { return ro_enabled_box_; }

  srs::SlowControl* srs() { return srs_.get(); }
  inline const QString& fecIp() const { return ip_; }

  ApvRegisterTab::ApvChannelsSel& apvChannelsSelectors();
  PllRegisterTab::PllChannelsSel& pllChannelsSelectors();
  void refreshAllRegisters();
  void togglePinger(bool);

public slots:
  void setStatusBarText(const QString&);
  void switchReadout(bool);

private:
  enum struct Tabs { GENERAL = 0, APVAPP, APV, PLL, ADCCARD, BULK, READOUT, NUM_TABS };

  SrsControl* parent_{nullptr};         // NOT owning
  LedIndicator* parent_led_{nullptr};   // NOT owning
  QCheckBox* ro_enabled_box_{nullptr};  // NOT owning

  const QString name_, ip_;

  std::unique_ptr<Ui::FecControl> ui_;               // owning
  std::unique_ptr<SysRegisterTab> tab_system_;       // owning
  std::unique_ptr<ApvAppRegisterTab> tab_apvapp_;    // owning
  std::unique_ptr<ApvRegisterTab> tab_apv_;          // owning
  std::unique_ptr<PllRegisterTab> tab_pll_;          // owning
  std::unique_ptr<AdcCardRegisterTab> tab_adccard_;  // owning
  std::unique_ptr<BulkConfigTab> tab_bulkconfig_;    // owning
  std::unique_ptr<QPinger> ping_;                    // owning

  std::unique_ptr<srs::SlowControl> srs_;  // owning
};

#endif
