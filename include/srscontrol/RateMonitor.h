/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_RateMonitor_H
#define srscontrol_RateMonitor_H

#include <QTimer>
#include <QWidget>
#include <memory>

namespace Ui {
  class RateMonitor;
}
class FecControl;

class RateMonitor : public QWidget {
  Q_OBJECT

public:
  explicit RateMonitor(QWidget* parent = nullptr, FecControl* main = nullptr);
  ~RateMonitor();

signals:
  void statusEvent(const QString&);

private slots:
  void toggleReadout(int);
  void update();
  void closeEvent(QCloseEvent*) override;

private:
  std::unique_ptr<Ui::RateMonitor> ui_;
  FecControl* main_;
  QTimer tmr_;
  const float period_ms_;
  const float readout_ms_;
  const float norm_;
  struct Scalers {
    void clear();
    uint64_t num_triggers;
    uint64_t num_frames;
  } scalers_;
};

#endif
