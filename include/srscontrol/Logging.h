/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef srscontrol_Logging_h
#define srscontrol_Logging_h

#include <QDialog>
#include <fstream>
#include <iosfwd>
#include <memory>
#include <sstream>

QT_BEGIN_NAMESPACE
namespace Ui {
  class Logging;
}
QT_END_NAMESPACE
class QTableView;
class QElementsTable;

class Logging : public QDialog {
  Q_OBJECT

public:
  explicit Logging(QWidget* parent = nullptr);
  ~Logging();

  void log(const QString& message);

private slots:
  void changeLoggingLevel();
  void clearLog();

private:
  std::unique_ptr<Ui::Logging> ui_;
  QTableView* table_log_;
  std::unique_ptr<QElementsTable> table_log_info_;

  /// Output stream derivation to EUDAQ_INFO
  class LogBuffer : public std::ostream {
  public:
    LogBuffer(Logging* log) : std::ostream(&buff_), buff_(log) {}

  private:
    class Logger : public std::stringbuf {
    public:
      Logger(Logging* log) : log_(log) {}
      int sync() override;

    private:
      Logging* log_;
    } buff_;
  } log_stream_;
  std::ofstream out_log_;
};

#endif
