/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srsdriver/Messenger.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"

int main(int argc, char* argv[]) {
  std::string slow_control;
  bool debug;
  srs::utils::ArgumentsParser(argc, argv)
      .addOptionalArgument("slow-control,f", "FEC address", &slow_control, "10.0.0.2")
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);

  srs::Messenger mess(slow_control);

  return 0;
}
