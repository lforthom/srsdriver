/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>

#include "srsdriver/ApvAddress.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/JsonConverter.h"
#include "srsutils/Logging.h"

using namespace std;
using json = nlohmann::json;

int main(int argc, char* argv[]) {
  bool debug, raw;
  string slow_control, output, json_output;

  srs::utils::ArgumentsParser(argc, argv)
      .addOptionalArgument("slow-control,f", "FEC address", &slow_control, "10.0.0.2")
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .addOptionalArgument("raw,r", "raw register dump", &raw, false)
      .addOptionalArgument("output,o", "output file (''=terminal)", &output, "")
      .addOptionalArgument("json-output,j", "use JSON output?", &json_output, "")
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);

  json js;

  ostringstream os;
  srs::SlowControl srs(slow_control);
  const auto system = srs.readSystemRegister();
  if (raw) {
    os << "System register:" << endl;
    system.printConfig(os);
  }
  system.print(os);
  if (!json_output.empty())
    js["system"] = system;

  for (size_t i = 0; i < 8; ++i) {
    os << "====== Channel " << i << "======\n"
       << "APV register:" << endl;
    const auto apv = srs.readApvRegister(srs::ApvAddress::channel(i, srs::ApvAddress::Mode::ALL_APVS));
    apv.printConfig(os);
    if (raw) {
      os << "APV register:" << endl;
      apv.printConfig(os);
    }
    if (!json_output.empty())
      js["channels"][i]["apv"] = apv;
    os << "PLL register:" << endl;
    const auto pll = srs.readPllRegister(srs::ApvAddress::channel(i, srs::ApvAddress::Mode::PLL));
    pll.printConfig(os);
    if (raw) {
      os << "PLL register:" << endl;
      apv.printConfig(os);
    }
    if (!json_output.empty())
      js["channels"][i]["pll"] = pll;
  }
  const auto apv_app = srs.readApvAppRegister();
  if (raw) {
    os << "APVapp register:" << endl;
    apv_app.printConfig(os);
  }
  apv_app.print(os);
  if (!json_output.empty())
    js["apvApp"] = apv_app;
  const auto adc_card = srs.readAdcCardRegister();
  if (raw) {
    os << "ADC card register:" << endl;
    adc_card.printConfig(os);
  }
  adc_card.print(os);
  if (!json_output.empty())
    js["adcCard"] = adc_card;

  if (output.empty())
    cout << os.str();
  else {
    ofstream ofile(output);
    ofile << os.str();
  }

  if (!json_output.empty()) {
    ofstream ofile(json_output);
    ofile << js;
  }

  return 0;
}
