/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2024  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsdriver/ApvAppRegister.h"
#include "srsdriver/Messenger.h"
#include "srsdriver/Receiver.h"
#include "srsdriver/SlowControl.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"

using namespace std;

int main(int argc, char* argv[]) {
  bool debug, unpack;
  int num_secs;
  string slow_control;
  vector<int> channels;

  srs::utils::ArgumentsParser(argc, argv)
      .addOptionalArgument("slow-control,f", "FEC address", &slow_control, "10.0.0.2")
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .addOptionalArgument("unpack,u", "unpack frames", &unpack, false)
      .addOptionalArgument("num-secs,n", "number of seconds of readout", &num_secs, 2)
      .addOptionalArgument("channels,c", "list of channels", &channels, vector<int>{})
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);
  //srs::Logger::get().setPretty(false);

  srs::SlowControl sc(slow_control);
  if (!channels.empty()) {
    srs::ApvAppRegister apvapp = sc.readApvAppRegister();
    for (size_t i = 0; i < 8; ++i) {
      const auto has_master = find(channels.begin(), channels.end(), 2 * i) != channels.end(),
                 has_slave = find(channels.begin(), channels.end(), 2 * i + 1) != channels.end();
      apvapp.ebChannelEnableMask().setEnabled(
          i,
          has_master && has_slave ? srs::ChannelEnable::ALL_APVS
                                  : (has_master ? srs::ChannelEnable::MASTER : srs::ChannelEnable::DISABLED));
    }
    apvapp.ebChannelEnableMask().print(cout);
    sc.messenger().writeConfig(srs::P_APVAPP, 0, apvapp);
  }

  sc.addFec(6006);

  if (!sc.readoutEnabled()) {
    sc.setReadoutEnable(true);
    SRS_INFO("main") << "Enabled readout.";
  }

  SRS_INFO("main") << "<<< readout enabled >>>";
  //
  const auto frames = sc.readoutFor(chrono::seconds(num_secs), 0, unpack);
  for (const auto& frame : frames)
    frame[0].print(cout);
  //atomic_bool readout{true};
  //auto frames = sc.read(0, readout);
  //frames[0].print(cout);

  sc.setReadoutEnable(false);

  return 0;
}
