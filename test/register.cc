/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "srsdriver/ApvAddress.h"
#include "srsdriver/ApvAppRegister.h"
#include "srsdriver/ApvRegister.h"

int main() {
  for (int i = 1; i < 3; ++i) {
    srs::ApvAddress(0x3, (srs::ApvAddress::Mode)i).print(std::cout);   // should be channels 0-1
    srs::ApvAddress(0x15, (srs::ApvAddress::Mode)i).print(std::cout);  // should be channels 0,2,4
    srs::ApvAddress(0x80, (srs::ApvAddress::Mode)i).print(std::cout);  // should be channel 7
  }
  {
    srs::ApvAppRegister apvapp;
    apvapp.ebChannelEnableMask().setMode(0, srs::ChannelEnable::ALL_APVS).setMode(2, srs::ChannelEnable::ALL_APVS);

    apvapp.ebEventInfoData().setRunLabel(0x1234);

    // print the whole register in hex-binary format
    apvapp.printConfig(std::cout);
  }

  return 0;
}
