/*
 *  srsdriver: a C++ driver for the SRS readout system
 *  Copyright (C) 2021-2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include "srsdriver/Messenger.h"
#include "srsutils/ArgumentsParser.h"
#include "srsutils/Logging.h"

int main(int argc, char* argv[]) {
  std::string config_file, output_config;
  bool debug;

  srs::utils::ArgumentsParser(argc, argv)
      .addArgument("config-file,c", "Input configuration file", &config_file)
      .addOptionalArgument("output-config,o", "Output configuration file", &output_config)
      .addOptionalArgument("debug,d", "debugging mode", &debug, false)
      .parse();

  if (debug)
    srs::Logger::get().setLevel(srs::Logger::Level::debug);

  std::string ip;
  srs::port_t port;
  srs::Messenger::MessageData data;
  auto config_parsed = srs::Messenger::parseCommands(config_file, ip, port, &data);
  for (const auto& pair : data.data_pairs)
    SRS_INFO("main") << pair.first << " -> " << pair.second;

  if (!output_config.empty()) {
    auto config_out = srs::Messenger::writeCommands(ip, port, data.sub_address, data.data_pairs);
    std::ofstream os(output_config);
    for (const auto& word : config_out)
      os << word << "\n";
    os.close();
  }

  return 0;
}
