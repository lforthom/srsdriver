#  srsdriver: a C++ driver for the SRS readout system
#  Copyright (C) 2021-2023  Laurent Forthomme
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pysrsdriver as srs
from sys import argv
import argparse

def main(args):
    parser = argparse.ArgumentParser(description='Slow control [python]')
    parser.add_argument('config', nargs='+', help='(List of) configuration commands file(s)')
    parser.add_argument('-x', '--dry-run', action='store_true', help='Only parse, do not send')
    parser.add_argument('-d', '--debug', action='store_true', help='Debugging mode')
    arg = parser.parse_args()

    if arg.debug:
        srs.Logger.get().level = srs.LoggerLevel.debug

    for cfg in arg.config:
        config, addr, port = srs.Messenger.parseCommands(cfg)
        if not arg.dry_run:
            msg = srs.Messenger(addr)
            if msg.send(port, config):
                print('Commands sent to port', port, 'at address', addr)
            else:
                print('Failed to send commands to port', port, 'at address', addr)

if __name__ == '__main__':
    main(argv)
